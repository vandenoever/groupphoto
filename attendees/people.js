const years = {
  "2003": {
    "people": {},
    "photo": "2003/groupphoto/NoveHradykde3.2beta.jpg"
  },
  "2004": {
    "people": {},
    "photo": "2004/groupphoto/7781f1.jpg"
  },
  "2005": {
    "people": {},
    "photo": "2005/groupphoto/akademy-2005-group-photo.jpg"
  },
  "2006": {
    "people": {
      "11": {
        "x": 620,
        "y": 608,
        "w": 84
      },
      "15": {
        "x": 740,
        "y": 620,
        "w": 84
      },
      "19": {
        "x": 830,
        "y": 598,
        "w": 72
      },
      "113": {
        "x": 2741.5,
        "y": 616.5,
        "w": 93
      },
      "130": {
        "x": 2226.5,
        "y": 639.5,
        "w": 33
      },
      "Konqi Konqueror, Dragon": {
        "x": 870,
        "y": 874,
        "w": 126
      },
      "Richard Dale, Ruby and C# bindings": {
        "x": 3077,
        "y": 935,
        "w": 174
      },
      "Katarina Erkkonen, apprentice": {
        "x": 3111,
        "y": 773,
        "w": 114
      },
      "Wade Olson, Marketing Working Group": {
        "x": 3053.5,
        "y": 702.5,
        "w": 105
      },
      "Tom Chance, Marketing Team": {
        "x": 2984,
        "y": 660,
        "w": 120
      },
      "Mário Vera": {
        "x": 3087.5,
        "y": 662.5,
        "w": 105
      },
      "Knut Yrvin, Skolelinux, Trolltech community": {
        "x": 2877.5,
        "y": 930.5,
        "w": 117
      },
      "Kevin Krammer": {
        "x": 2954,
        "y": 942,
        "w": 120
      },
      "Joon-Kyu Park, KLDraw": {
        "x": 2892,
        "y": 658,
        "w": 96
      },
      "Adriaan de Groot, e.V. Board": {
        "x": 2856.5,
        "y": 445.5,
        "w": 99
      },
      "Claire Lotion, KDE.nl, Marketing Team": {
        "x": 2945.5,
        "y": 736.5,
        "w": 81
      },
      "Andreas Kling": {
        "x": 2986,
        "y": 742,
        "w": 108
      },
      "Mauricio Piacentini": {
        "x": 2696,
        "y": 610,
        "w": 84
      },
      "Agustín Benito Bethencourt, Meduxa project": {
        "x": 2698.5,
        "y": 927.5,
        "w": 147
      },
      "Lars Knoll, Trolltech": {
        "x": 2760.5,
        "y": 715.5,
        "w": 99
      },
      "Carsten Pfeiffer": {
        "x": 2707.5,
        "y": 662.5,
        "w": 117
      },
      "Andreas Aardal Hanssen, Trolltech": {
        "x": 2785.5,
        "y": 612.5,
        "w": 105
      },
      "Allan Sandfeld Jensen": {
        "x": 2804,
        "y": 658,
        "w": 108
      },
      "George Staikos, KHTML": {
        "x": 2862,
        "y": 724,
        "w": 84
      },
      "John Cherry, OSDL": {
        "x": 2553,
        "y": 981,
        "w": 114
      },
      "Harald Fernengel, KDevelop": {
        "x": 2479.5,
        "y": 662.5,
        "w": 117
      },
      "Andras Mantia, Quanta": {
        "x": 2504,
        "y": 630,
        "w": 120
      },
      "Alexander Dymo, KDevelop": {
        "x": 2458.5,
        "y": 561.5,
        "w": 123
      },
      "Marius Bugge Monsen, Trolltech": {
        "x": 2580,
        "y": 628,
        "w": 120
      },
      "Christine Hundt": {
        "x": 2665,
        "y": 711,
        "w": 114
      },
      "Ellen Reitmayr, Usability": {
        "x": 2641.5,
        "y": 628.5,
        "w": 105
      },
      "Michal": {
        "x": 2536,
        "y": 588,
        "w": 96
      },
      "Dirk Müller, elite": {
        "x": 2598,
        "y": 580,
        "w": 96
      },
      "Sebastian Kügler, e.V. board, Marketing Working Group": {
        "x": 2443,
        "y": 1007,
        "w": 78
      },
      "Joseph Wenninger": {
        "x": 2384,
        "y": 672,
        "w": 108
      },
      "Jakob Petsovits, KDevelop": {
        "x": 2444.5,
        "y": 623.5,
        "w": 99
      },
      "Thomas Zander, KWord": {
        "x": 2472,
        "y": 720,
        "w": 108
      },
      "Cho Sung-Jae, Translator (South Korea)": {
        "x": 2568.5,
        "y": 443.5,
        "w": 111
      },
      "Adam Treat (manyoso), KDevelop": {
        "x": 2659,
        "y": 441,
        "w": 126
      },
      "Simon Hausmann, Trolltech": {
        "x": 2402.5,
        "y": 633.5,
        "w": 75
      },
      "Josef Spillner, KStuff": {
        "x": 2384,
        "y": 594,
        "w": 84
      },
      "Mirko Böhm, ex-e.V. Board": {
        "x": 2575.5,
        "y": 706.5,
        "w": 153
      },
      "Inge Wallin, KOffice": {
        "x": 2333,
        "y": 705,
        "w": 126
      },
      "Eric Laffoon, Quanta": {
        "x": 2335.5,
        "y": 614.5,
        "w": 81
      },
      "André Wöbbeking": {
        "x": 2303,
        "y": 557,
        "w": 102
      },
      "Jan Mühlig": {
        "x": 2280,
        "y": 644,
        "w": 96
      },
      "Nadeem Hasan": {
        "x": 2225.5,
        "y": 582.5,
        "w": 105
      },
      "Reinhold Kainhofer": {
        "x": 2167,
        "y": 613,
        "w": 78
      },
      "Stefan Winter": {
        "x": 2126.5,
        "y": 581.5,
        "w": 87
      },
      "Eva Brucherseifer, e.V. board": {
        "x": 2186.5,
        "y": 651.5,
        "w": 99
      },
      "Matthias Welwarsky, Eva's bloke": {
        "x": 2151,
        "y": 685,
        "w": 102
      },
      "Torsten Rahn, Artist": {
        "x": 2213,
        "y": 715,
        "w": 102
      },
      "Alan Horkan, KOffice": {
        "x": 2291.5,
        "y": 976.5,
        "w": 141
      },
      "Marcel Hilzinger, Linux New Media": {
        "x": 2158,
        "y": 1006,
        "w": 108
      },
      "Stefan Werden": {
        "x": 2088.5,
        "y": 699.5,
        "w": 111
      },
      "Valerie Hoh, Merchandise Coordinator": {
        "x": 2097,
        "y": 635,
        "w": 90
      },
      "Will Stephenson, KDE PIM": {
        "x": 2031.5,
        "y": 672.5,
        "w": 105
      },
      "Waldo Bastian, freedesktop.org": {
        "x": 2063.5,
        "y": 590.5,
        "w": 93
      },
      "Thiago Macieira, Qt DBus": {
        "x": 2023.5,
        "y": 450.5,
        "w": 117
      },
      "Volker Krause": {
        "x": 1967.5,
        "y": 564.5,
        "w": 105
      },
      "Christoph Cullmann": {
        "x": 1952,
        "y": 640,
        "w": 96
      },
      "Marcus Furlong, organiser": {
        "x": 2017,
        "y": 961,
        "w": 114
      },
      "Chris Howells, kscreensaver": {
        "x": 1966,
        "y": 732,
        "w": 84
      },
      "Christian Esken, KMix": {
        "x": 1894,
        "y": 648,
        "w": 96
      },
      "Benjamin Meyer, Trolltech": {
        "x": 1828,
        "y": 950,
        "w": 120
      },
      "Richard Moore, KJSEmbed": {
        "x": 1812,
        "y": 716,
        "w": 120
      },
      "Martin Aumüller, amarok": {
        "x": 1882,
        "y": 612,
        "w": 84
      },
      "Jes Hall, Promo": {
        "x": 1828,
        "y": 656,
        "w": 84
      },
      "Alexander Neundorf, CMake": {
        "x": 1857,
        "y": 443,
        "w": 102
      },
      "Tobias Hunger, Decibel": {
        "x": 1821,
        "y": 583,
        "w": 78
      },
      "Pino Toscano": {
        "x": 1766,
        "y": 618,
        "w": 84
      },
      "Michael Leibowitz, Intel/OpenOffice.org": {
        "x": 1670.5,
        "y": 970.5,
        "w": 129
      },
      "Pradeepto Bhattacharya, KDE India": {
        "x": 1712,
        "y": 792,
        "w": 72
      },
      "Kevin Ottens": {
        "x": 1740,
        "y": 694,
        "w": 96
      },
      "Stephan Binner, SuSE": {
        "x": 1746.5,
        "y": 656.5,
        "w": 69
      },
      "Gunnar Schmidt, accessibility": {
        "x": 1721,
        "y": 631,
        "w": 78
      },
      "Tobias Klein": {
        "x": 1700,
        "y": 604,
        "w": 60
      },
      "Tobias Koenig": {
        "x": 1676,
        "y": 650,
        "w": 60
      },
      "Olivier Goffart, Kopete": {
        "x": 1628,
        "y": 696,
        "w": 96
      },
      "Chris Schläger, TaskJuggler": {
        "x": 1576.5,
        "y": 708.5,
        "w": 105
      },
      "Holger Freyther": {
        "x": 1618,
        "y": 656,
        "w": 72
      },
      "Luboš Luňák, KWin": {
        "x": 1588,
        "y": 564,
        "w": 72
      },
      "Holger Schröder, KDE on Windows": {
        "x": 1546.5,
        "y": 596.5,
        "w": 93
      },
      "Aaron Seigo, undulating": {
        "x": 1502,
        "y": 706,
        "w": 96
      },
      "Martijn Klingens, kde.nl": {
        "x": 1502,
        "y": 652,
        "w": 84
      },
      "Sander Koning, KOffice": {
        "x": 1455.5,
        "y": 603.5,
        "w": 99
      },
      "Peter Simonnson, Konversation, KOffice": {
        "x": 1400,
        "y": 584,
        "w": 96
      },
      "Adrian Schröter": {
        "x": 1429.5,
        "y": 633.5,
        "w": 87
      },
      "Michaël Larouche, Kopete": {
        "x": 1398,
        "y": 688,
        "w": 96
      },
      "Cornelius Schumacher, e.V. board": {
        "x": 1429.5,
        "y": 741.5,
        "w": 123
      },
      "Hubert Figuiere, GPhoto": {
        "x": 1390.5,
        "y": 928.5,
        "w": 129
      },
      "Antonio Larrosa Jimenez, Akademy 2005": {
        "x": 1252,
        "y": 946,
        "w": 132
      },
      "Martin Konold, founder": {
        "x": 1286.5,
        "y": 770.5,
        "w": 105
      },
      "Johann Ollivier Lapeyre, Kopete and Oxygen": {
        "x": 1314.5,
        "y": 712.5,
        "w": 81
      },
      "Frank Karlitschek, kde-look": {
        "x": 1349.5,
        "y": 619.5,
        "w": 75
      },
      "Rainer Endres, screenshot recording": {
        "x": 1307,
        "y": 603,
        "w": 90
      },
      "Klaas Freitag (SUSE, Kraft)": {
        "x": 1234.5,
        "y": 622.5,
        "w": 117
      },
      "Leo Savernik": {
        "x": 1250.5,
        "y": 588.5,
        "w": 81
      },
      "Stephan Kulow, Release Dude": {
        "x": 1217,
        "y": 465,
        "w": 90
      },
      "Florian Graessle": {
        "x": 1157,
        "y": 599,
        "w": 78
      },
      "Isaac Clerencia, KOffice/Debian": {
        "x": 1194,
        "y": 778,
        "w": 84
      },
      "Pau Garcia i Quiles": {
        "x": 1129,
        "y": 935,
        "w": 114
      },
      "Albert Astals Cid, KPDF": {
        "x": 1088,
        "y": 704,
        "w": 108
      },
      "David Faure, elite": {
        "x": 1096.5,
        "y": 612.5,
        "w": 93
      },
      "Daniel Teske": {
        "x": 1067,
        "y": 631,
        "w": 66
      },
      "Sebastian Trueg": {
        "x": 1006.5,
        "y": 590.5,
        "w": 93
      },
      "Alfredo Beaumont Sainz, KFormula/KOffice": {
        "x": 1017.5,
        "y": 743.5,
        "w": 123
      },
      "Miquel Oliete Baliarda": {
        "x": 951,
        "y": 953,
        "w": 114
      },
      "Joan Francesc Serracant": {
        "x": 975.5,
        "y": 687.5,
        "w": 99
      },
      "Albert Cervera": {
        "x": 967,
        "y": 633,
        "w": 102
      },
      "Matthias Kretz, Phonon": {
        "x": 953.5,
        "y": 439.5,
        "w": 99
      },
      "Daniel Haas": {
        "x": 893.5,
        "y": 615.5,
        "w": 99
      },
      "John Tapsell, KSysGuard": {
        "x": 883,
        "y": 701,
        "w": 102
      },
      "Saki Tapsell, attached to John": {
        "x": 885.5,
        "y": 799.5,
        "w": 87
      },
      "Leo Vaz": {
        "x": 766.5,
        "y": 936.5,
        "w": 105
      },
      "Bartosz Fabianowski": {
        "x": 836.5,
        "y": 652.5,
        "w": 93
      },
      "Andreas Hartmetz, kdelibs": {
        "x": 777,
        "y": 629,
        "w": 78
      },
      "Kenny Duffus, Akademy 2007 organiser": {
        "x": 758,
        "y": 746,
        "w": 84
      },
      "Meni Livne": {
        "x": 735,
        "y": 699,
        "w": 78
      },
      "Fathi Boudra, Debian KDE Extras": {
        "x": 702,
        "y": 670,
        "w": 72
      },
      "George Wright, QtNX": {
        "x": 668.5,
        "y": 620.5,
        "w": 81
      },
      "Jos van den Oever, Strigi": {
        "x": 647.5,
        "y": 709.5,
        "w": 75
      },
      "Bertjan Broeksema": {
        "x": 600.5,
        "y": 662.5,
        "w": 81
      },
      "David Laban": {
        "x": 552,
        "y": 588,
        "w": 108
      },
      "Paco Garrido": {
        "x": 479.5,
        "y": 627.5,
        "w": 87
      },
      "Robert Knight, Konsole": {
        "x": 521.5,
        "y": 675.5,
        "w": 75
      },
      "Jonathan Riddell, Kubuntu": {
        "x": 510,
        "y": 760,
        "w": 96
      },
      "Anne Østergaard, Gnome Women": {
        "x": 379,
        "y": 757,
        "w": 90
      },
      "Pedro Jurado Maqueda, spanish promo": {
        "x": 358,
        "y": 702,
        "w": 84
      },
      "Robert Scott": {
        "x": 420,
        "y": 616,
        "w": 96
      },
      "Olaf Schmidt, Accessibility": {
        "x": 271,
        "y": 751,
        "w": 102
      },
      "Andreas Lloyd, anthropologist": {
        "x": 345,
        "y": 587,
        "w": 102
      },
      "Miguel Angel Pescador Santirso": {
        "x": 323,
        "y": 647,
        "w": 90
      },
      "David Vignoni, artist": {
        "x": 222.5,
        "y": 674.5,
        "w": 105
      },
      "Ken Wimer, Artist": {
        "x": 180,
        "y": 626,
        "w": 108
      }
    },
    "photo": "2006/groupphoto/akademy-2006-group-photo-big.jpg"
  },
  "2007": {
    "people": {
      "Cho Sung-Jae": {
        "x": 2780.5,
        "y": 74.5,
        "w": 81
      },
      "Benjamin Otte": {
        "x": 2119.5,
        "y": 63.5,
        "w": 93
      },
      "Fred Emmott (fred87)": {
        "x": 2126.5,
        "y": 138.5,
        "w": 93
      },
      "Holger Schröder": {
        "x": 2192,
        "y": 174,
        "w": 108
      },
      "Luciano Montanaro": {
        "x": 2181.5,
        "y": 117.5,
        "w": 93
      },
      "Troy Unrau": {
        "x": 2206.5,
        "y": 68.5,
        "w": 93
      },
      "Paul Adams": {
        "x": 2287.5,
        "y": 63.5,
        "w": 93
      },
      "Robert Knight": {
        "x": 2414.5,
        "y": 118.5,
        "w": 93
      },
      "Luigi Toscano": {
        "x": 2379.5,
        "y": 81.5,
        "w": 93
      },
      "Pedro Jurado Maqueda": {
        "x": 2537.5,
        "y": 96.5,
        "w": 93
      },
      "Rob Buis": {
        "x": 2575.5,
        "y": 133.5,
        "w": 93
      },
      "Dan Leinir Turthra Jensen (leinir)": {
        "x": 3076.5,
        "y": 630.5,
        "w": 93
      },
      "Joon-Kyu Park (segfault)": {
        "x": 2675.5,
        "y": 243.5,
        "w": 93
      },
      "12 XXX": {
        "x": 2728.5,
        "y": 320.5,
        "w": 93
      },
      "Maximilian Kossick": {
        "x": 2743.5,
        "y": 263.5,
        "w": 93
      },
      "David Vignoni": {
        "x": 2744.5,
        "y": 204.5,
        "w": 93
      },
      "Jos Poortvliet": {
        "x": 2789.5,
        "y": 245.5,
        "w": 93
      },
      "Martin Aumüller": {
        "x": 2874.5,
        "y": 224.5,
        "w": 93
      },
      "Klaas Freitag": {
        "x": 2883.5,
        "y": 273.5,
        "w": 93
      },
      "Bart Cerneels": {
        "x": 2836.5,
        "y": 292.5,
        "w": 93
      },
      "Guillaume Lazzara": {
        "x": 2417.5,
        "y": 225.5,
        "w": 93
      },
      "Carlo Navarra": {
        "x": 2470.5,
        "y": 198.5,
        "w": 93
      },
      "Riccardo Iaconelli (ruphy)": {
        "x": 2615.5,
        "y": 189.5,
        "w": 93
      },
      "Jure Repinc": {
        "x": 2538.5,
        "y": 234.5,
        "w": 93
      },
      "Bart Coppens": {
        "x": 2573.5,
        "y": 273.5,
        "w": 93
      },
      "Stian Haklev": {
        "x": 2662.5,
        "y": 296.5,
        "w": 93
      },
      "Sharan Rao": {
        "x": 2737.5,
        "y": 379.5,
        "w": 93
      },
      "Nicolas Roffet": {
        "x": 2988.5,
        "y": 364.5,
        "w": 93
      },
      "Johann Ollivier Lapeyre": {
        "x": 2955.5,
        "y": 315.5,
        "w": 93
      },
      "28 XXX": {
        "x": 2976.5,
        "y": 244.5,
        "w": 93
      },
      "Neja Repinc": {
        "x": 2921.5,
        "y": 181.5,
        "w": 93
      },
      "Neil Mckillop": {
        "x": 2830.5,
        "y": 174.5,
        "w": 93
      },
      "Tobias Hunger": {
        "x": 2683.5,
        "y": 121.5,
        "w": 93
      },
      "Mario Fux": {
        "x": 2660.5,
        "y": 56.5,
        "w": 93
      },
      "Martijn Klingens": {
        "x": 2872,
        "y": 68,
        "w": 126
      },
      "Ivan Čukić": {
        "x": 2997,
        "y": 107,
        "w": 126
      },
      "35 XXX": {
        "x": 3102,
        "y": 310,
        "w": 126
      },
      "Paolo Capriotti": {
        "x": 2585,
        "y": 327,
        "w": 126
      },
      "Nikolaj Hald Nielsen": {
        "x": 2856,
        "y": 360,
        "w": 126
      },
      "Ian Monroe": {
        "x": 2917,
        "y": 453,
        "w": 126
      },
      "Rex Dieter": {
        "x": 2560,
        "y": 668,
        "w": 126
      },
      "Dominik Schmidt (domme)": {
        "x": 2107,
        "y": 669,
        "w": 126
      },
      "Enrico Ros (eros)": {
        "x": 1618,
        "y": 600,
        "w": 126
      },
      "Luca Burelli (pillo)": {
        "x": 1485,
        "y": 631,
        "w": 126
      },
      "Jack Donaghy (jackster)": {
        "x": 1280,
        "y": 678,
        "w": 126
      },
      "Louai Al-Khanji": {
        "x": 287,
        "y": 601,
        "w": 126
      },
      "Laur Mõtus": {
        "x": 490,
        "y": 616,
        "w": 126
      },
      "Teemu Rytilahti": {
        "x": 669,
        "y": 595,
        "w": 126
      },
      "Alberto Scarpa (skaal)": {
        "x": 844,
        "y": 586,
        "w": 126
      },
      "Nicola Brisotto (bris8)": {
        "x": 979,
        "y": 621,
        "w": 126
      },
      "Konrad Kazimierz Dąbrowski": {
        "x": 2534,
        "y": 438,
        "w": 108
      },
      "Anne-Marie Mahfouf": {
        "x": 2506,
        "y": 271,
        "w": 78
      },
      "Hanna Skott": {
        "x": 2479.5,
        "y": 377.5,
        "w": 93
      },
      "Paul Millar": {
        "x": 2447.5,
        "y": 286.5,
        "w": 93
      },
      "Pau Garcia i Quiles": {
        "x": 2321.5,
        "y": 285.5,
        "w": 93
      },
      "Matt Williams": {
        "x": 2218.5,
        "y": 359.5,
        "w": 93
      },
      "Matt Hoosier": {
        "x": 1831,
        "y": 211,
        "w": 78
      },
      "Benoit Sigoure (tsuna)": {
        "x": 2333,
        "y": 209,
        "w": 78
      },
      "Martin Ellis": {
        "x": 2264,
        "y": 234,
        "w": 78
      },
      "Arno Rehn": {
        "x": 2203,
        "y": 288,
        "w": 78
      },
      "58 XXX": {
        "x": 1856,
        "y": 163,
        "w": 78
      },
      "David Laban": {
        "x": 1824,
        "y": 82,
        "w": 78
      },
      "Danny Allen (dannya)": {
        "x": 1883,
        "y": 85,
        "w": 78
      },
      "Richard Moore (richmoore)": {
        "x": 1930,
        "y": 83,
        "w": 78
      },
      "George McLachlan (gLAsgowMonkey)": {
        "x": 1920,
        "y": 138,
        "w": 78
      },
      "Ron Petrick": {
        "x": 1974,
        "y": 169,
        "w": 78
      },
      "Aleix Pol Gonzalez": {
        "x": 2009,
        "y": 122,
        "w": 90
      },
      "Simon Edwards": {
        "x": 2135,
        "y": 213,
        "w": 90
      },
      "Sebastian Müller": {
        "x": 2098.5,
        "y": 364.5,
        "w": 111
      },
      "Meni Livne": {
        "x": 2027.5,
        "y": 318.5,
        "w": 111
      },
      "Mary Ellen Foster": {
        "x": 1991.5,
        "y": 310.5,
        "w": 81
      },
      "Benjamin Reed (RangerRick)": {
        "x": 1739,
        "y": 398,
        "w": 114
      },
      "Alexis Ménard": {
        "x": 1125.5,
        "y": 170.5,
        "w": 87
      },
      "Laura Dragan": {
        "x": 1137.5,
        "y": 112.5,
        "w": 87
      },
      "Sander Koning": {
        "x": 1187.5,
        "y": 52.5,
        "w": 87
      },
      "73 XXX": {
        "x": 1253.5,
        "y": 61.5,
        "w": 87
      },
      "Hin-Tak Leung": {
        "x": 1405.5,
        "y": 179.5,
        "w": 87
      },
      "75 XXX": {
        "x": 1787.5,
        "y": 136.5,
        "w": 87
      },
      "Jarosław Staniek": {
        "x": 1745.5,
        "y": 106.5,
        "w": 75
      },
      "77 XXX": {
        "x": 1742.5,
        "y": 71.5,
        "w": 75
      },
      "Marijn Kruisselbrink": {
        "x": 1670.5,
        "y": 84.5,
        "w": 99
      },
      "Laurent Montel": {
        "x": 1606.5,
        "y": 75.5,
        "w": 87
      },
      "Andrew Manson": {
        "x": 1622,
        "y": 120,
        "w": 102
      },
      "Peter Murdoch (pete)": {
        "x": 1557.5,
        "y": 343.5,
        "w": 105
      },
      "Raúl Sánchez Siles": {
        "x": 1106.5,
        "y": 304.5,
        "w": 117
      },
      "Florian Piquemal": {
        "x": 1031,
        "y": 163,
        "w": 102
      },
      "84 XXX": {
        "x": 1046,
        "y": 104,
        "w": 96
      },
      "Emanuelle Tamponi": {
        "x": 1034,
        "y": 52,
        "w": 84
      },
      "Cyrille Berger": {
        "x": 910.5,
        "y": 27.5,
        "w": 117
      },
      "Franz Keferböck": {
        "x": 903.5,
        "y": 84.5,
        "w": 99
      },
      "Pino Toscano": {
        "x": 890,
        "y": 148,
        "w": 108
      },
      "Thibault Normand": {
        "x": 948.5,
        "y": 199.5,
        "w": 105
      },
      "Javier Uruen Val": {
        "x": 970,
        "y": 289,
        "w": 102
      },
      "David Bialer": {
        "x": 821.5,
        "y": 265.5,
        "w": 99
      },
      "Charlotte Nielsen": {
        "x": 791.5,
        "y": 248.5,
        "w": 93
      },
      "Ross Wilson": {
        "x": 514.5,
        "y": 68.5,
        "w": 99
      },
      "Marcus Hanwell": {
        "x": 794.5,
        "y": 185.5,
        "w": 93
      },
      "93 XXX": {
        "x": 793,
        "y": 144,
        "w": 84
      },
      "Joseph Kerian": {
        "x": 735,
        "y": 89,
        "w": 96
      },
      "Stefan Teleman": {
        "x": 675,
        "y": 57,
        "w": 102
      },
      "96 XXX": {
        "x": 672.5,
        "y": 130.5,
        "w": 87
      },
      "Mickaël Sibelle": {
        "x": 682.5,
        "y": 185.5,
        "w": 87
      },
      "Kyle Gordon": {
        "x": 709.5,
        "y": 233.5,
        "w": 99
      },
      "Matthew Rosewarne": {
        "x": 670.5,
        "y": 303.5,
        "w": 105
      },
      "Jens Herden": {
        "x": 559.5,
        "y": 373.5,
        "w": 105
      },
      "Khoem Sokhem (sokhem)": {
        "x": 485,
        "y": 355,
        "w": 102
      },
      "Olivier Serve (Tifauv)": {
        "x": 474.5,
        "y": 174.5,
        "w": 81
      },
      "Robert Scott": {
        "x": 547,
        "y": 139,
        "w": 108
      },
      "Bas Grolleman (bgrolleman)": {
        "x": 330.5,
        "y": 174.5,
        "w": 111
      },
      "Andras Mantia": {
        "x": 720.5,
        "y": 389.5,
        "w": 111
      },
      "Jonathan Riddell (Riddell)": {
        "x": 891,
        "y": 377,
        "w": 108
      },
      "André Wöbbeking": {
        "x": 1124,
        "y": 572,
        "w": 126
      },
      "Danny Kukawka (dannyK)": {
        "x": 2470,
        "y": 78,
        "w": 90
      },
      "Luboš Luňák (seli)": {
        "x": 2593,
        "y": 60,
        "w": 78
      },
      "Frerich Raabe (frerich)": {
        "x": 2325.5,
        "y": 104.5,
        "w": 87
      },
      "Simon Hausmann (tronical)": {
        "x": 2373,
        "y": 140,
        "w": 84
      },
      "Stephan Binner (beineri)": {
        "x": 2453,
        "y": 152,
        "w": 78
      },
      "Daniel Molkentin (danimo)": {
        "x": 2529,
        "y": 144,
        "w": 84
      },
      "Leo Savernik": {
        "x": 2253.5,
        "y": 125.5,
        "w": 75
      },
      "Albert Astals Cid (tsdgeos)": {
        "x": 1988.5,
        "y": 77.5,
        "w": 75
      },
      "Harri Porten (harri)": {
        "x": 2075,
        "y": 76,
        "w": 84
      },
      "David Faure (dfaure)": {
        "x": 2732,
        "y": 50,
        "w": 84
      },
      "Anne Østergaard": {
        "x": 2757.5,
        "y": 466.5,
        "w": 105
      },
      "Anne Wilson": {
        "x": 3134.5,
        "y": 463.5,
        "w": 99
      },
      "Kenny Duffus (seaLne)": {
        "x": 3243.5,
        "y": 460.5,
        "w": 105
      },
      "Gunnar Schmidt": {
        "x": 2337,
        "y": 671,
        "w": 138
      },
      "Pradeepto Bhattacharya": {
        "x": 2622.5,
        "y": 449.5,
        "w": 99
      },
      "Aaron Seigo (aseigo)": {
        "x": 2726.5,
        "y": 681.5,
        "w": 123
      },
      "Torsten Rahn (tackat)": {
        "x": 2931.5,
        "y": 556.5,
        "w": 117
      },
      "Wendy Van Craen": {
        "x": 3022.5,
        "y": 478.5,
        "w": 117
      },
      "Richard Dale": {
        "x": 2371.5,
        "y": 273.5,
        "w": 99
      },
      "Maurizio Monge": {
        "x": 2349.5,
        "y": 345.5,
        "w": 111
      },
      "Claire Lotion": {
        "x": 2423.5,
        "y": 474.5,
        "w": 105
      },
      "Joseph Wenninger (Jowenn)": {
        "x": 2194,
        "y": 413,
        "w": 108
      },
      "Andreas Hanssen": {
        "x": 1991,
        "y": 573,
        "w": 132
      },
      "Jesper Thomschütz": {
        "x": 1892,
        "y": 568,
        "w": 120
      },
      "Benjamin Meyer (icefox)": {
        "x": 1843,
        "y": 630,
        "w": 132
      },
      "Kevin Krammer": {
        "x": 2328,
        "y": 466,
        "w": 90
      },
      "Harald Fernengel": {
        "x": 2102,
        "y": 309,
        "w": 96
      },
      "Eva Brucherseifer (eva)": {
        "x": 1946.5,
        "y": 356.5,
        "w": 105
      },
      "Will Stephenson (Bille)": {
        "x": 2012,
        "y": 208,
        "w": 90
      },
      "Chris Schläger (cs)": {
        "x": 1940.5,
        "y": 214.5,
        "w": 99
      },
      "Kent Hansen": {
        "x": 1836.5,
        "y": 247.5,
        "w": 105
      },
      "Marius Bugge Monsen (mbm)": {
        "x": 1853.5,
        "y": 315.5,
        "w": 87
      },
      "Lars Knoll (lars)": {
        "x": 1729.5,
        "y": 301.5,
        "w": 105
      },
      "John Tapsell": {
        "x": 1682,
        "y": 232,
        "w": 96
      },
      "Rafael Fernández López": {
        "x": 1509,
        "y": 191,
        "w": 90
      },
      "John Layt": {
        "x": 1514,
        "y": 126,
        "w": 90
      },
      "Duncan Mac Vicar": {
        "x": 1376,
        "y": 105,
        "w": 96
      },
      "Alexander Dymo": {
        "x": 1563.5,
        "y": 98.5,
        "w": 87
      },
      "Adrian Schröter": {
        "x": 1493.5,
        "y": 45.5,
        "w": 93
      },
      "Josef Spillner": {
        "x": 1448,
        "y": 91,
        "w": 96
      },
      "Thiago Macieira": {
        "x": 1623.5,
        "y": 198.5,
        "w": 93
      },
      "Felipe Zimmerle": {
        "x": 1559.5,
        "y": 257.5,
        "w": 99
      },
      "Zack Rusin (zrusin)": {
        "x": 1600.5,
        "y": 289.5,
        "w": 111
      },
      "Giovanni Venturi (slacky)": {
        "x": 1609.5,
        "y": 423.5,
        "w": 99
      },
      "Mirko Böhm": {
        "x": 1506.5,
        "y": 268.5,
        "w": 93
      },
      "Helio Castro (helio)": {
        "x": 1431.5,
        "y": 269.5,
        "w": 87
      },
      "Till Adam (till)": {
        "x": 1384.5,
        "y": 268.5,
        "w": 87
      },
      "Jesper Pedersen": {
        "x": 1395,
        "y": 314,
        "w": 108
      },
      "Sebastian Kügler (sebas)": {
        "x": 1441.5,
        "y": 402.5,
        "w": 117
      },
      "Jos van den Oever": {
        "x": 1155,
        "y": 387,
        "w": 120
      },
      "Charles Samuels": {
        "x": 1052,
        "y": 368,
        "w": 126
      },
      "Cornelius Schumacher": {
        "x": 1289,
        "y": 359,
        "w": 114
      },
      "Isaac Clerencia": {
        "x": 1230.5,
        "y": 310.5,
        "w": 111
      },
      "Kevin Ottens": {
        "x": 1124,
        "y": 212,
        "w": 102
      },
      "Allan Sandfeld Jensen": {
        "x": 1225,
        "y": 225,
        "w": 96
      },
      "Ingo Klöcker": {
        "x": 1265.5,
        "y": 169.5,
        "w": 87
      },
      "Frank Karlitschek": {
        "x": 1191,
        "y": 110,
        "w": 96
      },
      "Boudewĳn Rempt": {
        "x": 1112,
        "y": 40,
        "w": 84
      },
      "Dominik Haumann": {
        "x": 1296.5,
        "y": 98.5,
        "w": 81
      },
      "Volker Krause": {
        "x": 1353.5,
        "y": 35.5,
        "w": 99
      },
      "Inge Wallin": {
        "x": 803,
        "y": 316,
        "w": 108
      },
      "Jakob Petsovits": {
        "x": 526.5,
        "y": 306.5,
        "w": 105
      },
      "Mauricio Piacentini": {
        "x": 456,
        "y": 100,
        "w": 102
      },
      "Frank Osterfeld": {
        "x": 815.5,
        "y": 39.5,
        "w": 99
      },
      "Adriaan de Groot": {
        "x": 570,
        "y": 205,
        "w": 90
      },
      "Dirk Müller (dirk)": {
        "x": 425.5,
        "y": 211.5,
        "w": 117
      },
      "Knut Yrvin": {
        "x": 346.5,
        "y": 403.5,
        "w": 117
      },
      "Olaf Schmidt": {
        "x": 191,
        "y": 392,
        "w": 132
      }
    },
    "photo": "2007/groupphoto/group-photo.jpg"
  },
  "2008": {
    "people": {
      "9": {
        "x": 585.5,
        "y": 315.5,
        "w": 39
      },
      "15": {
        "x": 729.5,
        "y": 314.5,
        "w": 51
      },
      "17": {
        "x": 804,
        "y": 309,
        "w": 30
      },
      "19": {
        "x": 849.5,
        "y": 298.5,
        "w": 21
      },
      "21": {
        "x": 886,
        "y": 296,
        "w": 30
      },
      "22": {
        "x": 907,
        "y": 297,
        "w": 36
      },
      "23": {
        "x": 919.5,
        "y": 274.5,
        "w": 39
      },
      "24": {
        "x": 932.5,
        "y": 301.5,
        "w": 33
      },
      "25": {
        "x": 944.5,
        "y": 272.5,
        "w": 45
      },
      "26": {
        "x": 957,
        "y": 307,
        "w": 42
      },
      "36": {
        "x": 1133,
        "y": 288,
        "w": 42
      },
      "37": {
        "x": 1140,
        "y": 264,
        "w": 24
      },
      "38": {
        "x": 1150,
        "y": 263,
        "w": 42
      },
      "40": {
        "x": 1195.5,
        "y": 275.5,
        "w": 39
      },
      "43": {
        "x": 1242.5,
        "y": 260.5,
        "w": 21
      },
      "44": {
        "x": 1249,
        "y": 262,
        "w": 36
      },
      "46": {
        "x": 1260.5,
        "y": 240.5,
        "w": 39
      },
      "48": {
        "x": 1312.5,
        "y": 267.5,
        "w": 21
      },
      "49": {
        "x": 1327.5,
        "y": 258.5,
        "w": 33
      },
      "56": {
        "x": 1420.5,
        "y": 270.5,
        "w": 27
      },
      "57": {
        "x": 1430.5,
        "y": 254.5,
        "w": 39
      },
      "60": {
        "x": 1504.5,
        "y": 220.5,
        "w": 33
      },
      "61": {
        "x": 1533.5,
        "y": 242.5,
        "w": 15
      },
      "62": {
        "x": 1531,
        "y": 224,
        "w": 30
      },
      "63": {
        "x": 1504,
        "y": 254,
        "w": 42
      },
      "64": {
        "x": 1536,
        "y": 256,
        "w": 48
      },
      "67": {
        "x": 1594,
        "y": 255,
        "w": 48
      },
      "69": {
        "x": 1618,
        "y": 227,
        "w": 24
      },
      "70": {
        "x": 1649.5,
        "y": 238.5,
        "w": 51
      },
      "76": {
        "x": 1765,
        "y": 233,
        "w": 48
      },
      "77": {
        "x": 1770,
        "y": 215,
        "w": 36
      },
      "78": {
        "x": 1793.5,
        "y": 213.5,
        "w": 33
      },
      "80": {
        "x": 1815,
        "y": 217,
        "w": 42
      },
      "83": {
        "x": 1883,
        "y": 219,
        "w": 36
      },
      "86": {
        "x": 1928,
        "y": 201,
        "w": 42
      },
      "90": {
        "x": 1979,
        "y": 239,
        "w": 60
      },
      "92": {
        "x": 2056.5,
        "y": 209.5,
        "w": 33
      },
      "93": {
        "x": 2032,
        "y": 241,
        "w": 60
      },
      "94": {
        "x": 2071.5,
        "y": 220.5,
        "w": 69
      },
      "96": {
        "x": 2132,
        "y": 214,
        "w": 66
      },
      "97": {
        "x": 2148.5,
        "y": 189.5,
        "w": 57
      },
      "98": {
        "x": 2179.5,
        "y": 193.5,
        "w": 45
      },
      "100": {
        "x": 2211,
        "y": 189,
        "w": 42
      },
      "101": {
        "x": 2233.5,
        "y": 189.5,
        "w": 51
      },
      "102": {
        "x": 2262.5,
        "y": 194.5,
        "w": 57
      },
      "107": {
        "x": 772.5,
        "y": 329.5,
        "w": 45
      },
      "113": {
        "x": 1239,
        "y": 326,
        "w": 42
      },
      "114": {
        "x": 1366,
        "y": 299,
        "w": 48
      },
      "116": {
        "x": 1488,
        "y": 280,
        "w": 54
      },
      "118": {
        "x": 163,
        "y": 442,
        "w": 72
      },
      "119": {
        "x": 249.5,
        "y": 488.5,
        "w": 57
      },
      "121": {
        "x": 288,
        "y": 489,
        "w": 54
      },
      "124": {
        "x": 431,
        "y": 422,
        "w": 54
      },
      "126": {
        "x": 476,
        "y": 449,
        "w": 60
      },
      "128": {
        "x": 572,
        "y": 430,
        "w": 36
      },
      "131": {
        "x": 671,
        "y": 372,
        "w": 60
      },
      "135": {
        "x": 801,
        "y": 405,
        "w": 54
      },
      "136": {
        "x": 849.5,
        "y": 384.5,
        "w": 45
      },
      "137": {
        "x": 889.5,
        "y": 361.5,
        "w": 39
      },
      "138": {
        "x": 883.5,
        "y": 393.5,
        "w": 45
      },
      "139": {
        "x": 925.5,
        "y": 361.5,
        "w": 27
      },
      "140": {
        "x": 928,
        "y": 384,
        "w": 48
      },
      "141": {
        "x": 952,
        "y": 354,
        "w": 48
      },
      "142": {
        "x": 981.5,
        "y": 368.5,
        "w": 45
      },
      "146": {
        "x": 1096.5,
        "y": 349.5,
        "w": 39
      },
      "148": {
        "x": 1164.5,
        "y": 363.5,
        "w": 33
      },
      "149": {
        "x": 1254,
        "y": 355,
        "w": 36
      },
      "150": {
        "x": 1278.5,
        "y": 357.5,
        "w": 39
      },
      "151": {
        "x": 1324,
        "y": 352,
        "w": 30
      },
      "153": {
        "x": 1382,
        "y": 348,
        "w": 36
      },
      "155": {
        "x": 1530.5,
        "y": 346.5,
        "w": 57
      },
      "158": {
        "x": 1835.5,
        "y": 331.5,
        "w": 51
      },
      "159": {
        "x": 1922.5,
        "y": 355.5,
        "w": 45
      },
      "160": {
        "x": 1954,
        "y": 365,
        "w": 48
      },
      "162": {
        "x": 538.5,
        "y": 483.5,
        "w": 51
      },
      "169": {
        "x": 802.5,
        "y": 455.5,
        "w": 63
      },
      "170": {
        "x": 837.5,
        "y": 440.5,
        "w": 93
      },
      "175": {
        "x": 968,
        "y": 448,
        "w": 60
      },
      "178": {
        "x": 1074.5,
        "y": 406.5,
        "w": 69
      },
      "180": {
        "x": 1172.5,
        "y": 372.5,
        "w": 51
      },
      "182": {
        "x": 1131,
        "y": 464,
        "w": 66
      },
      "188": {
        "x": 1272,
        "y": 456,
        "w": 72
      },
      "194": {
        "x": 1507,
        "y": 385,
        "w": 54
      },
      "195": {
        "x": 1468,
        "y": 419,
        "w": 72
      },
      "198": {
        "x": 1526,
        "y": 432,
        "w": 72
      },
      "201": {
        "x": 1631.5,
        "y": 417.5,
        "w": 75
      },
      "202": {
        "x": 1645.5,
        "y": 509.5,
        "w": 81
      },
      "203": {
        "x": 1696,
        "y": 434,
        "w": 66
      },
      "204": {
        "x": 1731,
        "y": 391,
        "w": 66
      },
      "205": {
        "x": 1767,
        "y": 369,
        "w": 66
      },
      "207": {
        "x": 1847,
        "y": 367,
        "w": 60
      },
      "209": {
        "x": 1738,
        "y": 443,
        "w": 78
      },
      "210": {
        "x": 1819,
        "y": 420,
        "w": 84
      },
      "212": {
        "x": 1933.5,
        "y": 417.5,
        "w": 81
      },
      "Till Adam (till), KDE PIM": {
        "x": 1109.5,
        "y": 269.5,
        "w": 33
      },
      "Konqi Konqueror (konqi), no idea, but seems to turn up to all the conferences": {
        "x": 1641,
        "y": 42,
        "w": 162
      },
      "Claudia Rauch (claudiar), e.v.": {
        "x": 2032.5,
        "y": 425.5,
        "w": 75
      },
      "Kenny Duffus (seaLne), Akademy": {
        "x": 1896,
        "y": 464,
        "w": 84
      },
      "Pradeepto Bhattacharya (pradeepto), KDE PIM, KDE India": {
        "x": 1888,
        "y": 372,
        "w": 60
      },
      "Carlos Licea": {
        "x": 1808.5,
        "y": 374.5,
        "w": 63
      },
      "Oswald Buddenhagen (ossi), KDM": {
        "x": 1595.5,
        "y": 442.5,
        "w": 81
      },
      "Marcel Hilzinger (?)": {
        "x": 1575,
        "y": 458,
        "w": 54
      },
      "Davide Bettio (WindowsUninstall), Plasma": {
        "x": 1448,
        "y": 484,
        "w": 90
      },
      "Kevin Krammer, XDG": {
        "x": 1431.5,
        "y": 447.5,
        "w": 75
      },
      "Leo Franchi (lfranchi), Amarok": {
        "x": 1415.5,
        "y": 366.5,
        "w": 63
      },
      "Jonathan Riddell (Riddell), Kubuntu": {
        "x": 1376.5,
        "y": 395.5,
        "w": 63
      },
      "Mike Arthur (mikearthur)": {
        "x": 1326.5,
        "y": 386.5,
        "w": 51
      },
      "Riccardo Iaconelli (ruphy), Oxygen": {
        "x": 1358.5,
        "y": 504.5,
        "w": 81
      },
      "Chani Armitage (Chani), Plasma": {
        "x": 1268,
        "y": 533,
        "w": 72
      },
      "Ingo Klöcker (KMail)": {
        "x": 1231.5,
        "y": 438.5,
        "w": 69
      },
      "Sven Krohlas (sven423), Amarok": {
        "x": 1275.5,
        "y": 383.5,
        "w": 57
      },
      "Seb Ruiz (sebr), xmms": {
        "x": 1227.5,
        "y": 375.5,
        "w": 63
      },
      "Sebastian Kügler (sebas), e.V.": {
        "x": 1172.5,
        "y": 517.5,
        "w": 75
      },
      "Johann Ollivier Lapeyre, Oxygen": {
        "x": 1065.5,
        "y": 486.5,
        "w": 81
      },
      "Thomas Baumgart / KMyMoney": {
        "x": 1170,
        "y": 407,
        "w": 60
      },
      "Niels van Mourik": {
        "x": 1117,
        "y": 373,
        "w": 66
      },
      "Simon Edwards (Sime), PyKDE": {
        "x": 1036,
        "y": 447,
        "w": 72
      },
      "Marijn Kruisselbrink (Mek)": {
        "x": 1006.5,
        "y": 407.5,
        "w": 63
      },
      "Jarosław Staniek (jstaniek), Kexi": {
        "x": 955,
        "y": 406,
        "w": 54
      },
      "Wendy Van Craen (CRAEW), Akademy": {
        "x": 876.5,
        "y": 500.5,
        "w": 63
      },
      "Bart Cerneels (Stecchino), Akademy": {
        "x": 904,
        "y": 466,
        "w": 66
      },
      "Lydia Pintscher (Nightrose), Amarok Promo": {
        "x": 887.5,
        "y": 424.5,
        "w": 51
      },
      "Bart Coppens (BCoppens), KOffice": {
        "x": 752,
        "y": 471,
        "w": 66
      },
      "Alexis Ménard (darktears), Plasma": {
        "x": 702.5,
        "y": 470.5,
        "w": 51
      },
      "Cornelius Schumacher (cornelius), e.V.": {
        "x": 686.5,
        "y": 437.5,
        "w": 51
      },
      "Kevin Ottens (ervin), Well Solid": {
        "x": 631.5,
        "y": 471.5,
        "w": 63
      },
      "Allan Sandfeld Jensen (carewolf), KHTML": {
        "x": 593.5,
        "y": 461.5,
        "w": 51
      },
      "Matthias Kretz (Vir), Phonon": {
        "x": 568,
        "y": 463,
        "w": 42
      },
      "Anurag Patel (anurag), KDE India": {
        "x": 2015.5,
        "y": 385.5,
        "w": 63
      },
      "Inge Wallin (ingwa), KOffice, Marble": {
        "x": 1733.5,
        "y": 340.5,
        "w": 63
      },
      "David Faure (dfaure), All Round Genius": {
        "x": 1632.5,
        "y": 357.5,
        "w": 75
      },
      "Jos van den Oever (vandenoever), Strigi": {
        "x": 1410,
        "y": 332,
        "w": 42
      },
      "Mark Kretschmann (markey), Amarok": {
        "x": 1343,
        "y": 338,
        "w": 48
      },
      "Marius Bugge Monsen (mbm), Qt": {
        "x": 1120.5,
        "y": 344.5,
        "w": 39
      },
      "Jernej Kos, Kostko, KFTPGrabber": {
        "x": 1070,
        "y": 356,
        "w": 42
      },
      "Girish Ramakrishnan, KOffice": {
        "x": 1052,
        "y": 362,
        "w": 36
      },
      "Gregor Kalisnik, Mastermind, KSlovar": {
        "x": 1006.5,
        "y": 365.5,
        "w": 51
      },
      "Holger Freyther, OpenMoko": {
        "x": 789,
        "y": 371,
        "w": 54
      },
      "Alexander Dymo (adymo), KDevelop": {
        "x": 756.5,
        "y": 384.5,
        "w": 51
      },
      "Harry Fernengel, KDevelop, Qt": {
        "x": 730,
        "y": 406,
        "w": 54
      },
      "Matthew Rosewarne": {
        "x": 640.5,
        "y": 425.5,
        "w": 51
      },
      "Chitlesh Goorha (chitlesh), Fedora": {
        "x": 605,
        "y": 405,
        "w": 42
      },
      "Lukas Tinkl (ltinkl), Red Hat": {
        "x": 516.5,
        "y": 429.5,
        "w": 51
      },
      "Mohammad Dhanii Anwari Mohammed-Taib, mdamt, Nokia": {
        "x": 424.5,
        "y": 473.5,
        "w": 63
      },
      "Olivier Goffart (Gof), Kopete": {
        "x": 392.5,
        "y": 465.5,
        "w": 51
      },
      "Jos Poortvliet (jospoortvliet), marketing": {
        "x": 220,
        "y": 533,
        "w": 60
      },
      "Prashanth Udupa, GCF": {
        "x": 323,
        "y": 427,
        "w": 72
      },
      "Myriam Schweingruber, Amarok, FSFE": {
        "x": 1444.5,
        "y": 320.5,
        "w": 57
      },
      "Helio Castro (heliocastro), Mandriva": {
        "x": 1418,
        "y": 295,
        "w": 42
      },
      "Jesper Thomschütz (jesperht), Plasma": {
        "x": 1180,
        "y": 325,
        "w": 48
      },
      "Andreas Hanssen (bibr), Qt, Plasma": {
        "x": 1041.5,
        "y": 320.5,
        "w": 45
      },
      "Andreas Hartmetz (maelcum), KDE Libs": {
        "x": 935.5,
        "y": 328.5,
        "w": 39
      },
      "Leonardo Sobral (lsobral), QEdje": {
        "x": 881,
        "y": 321,
        "w": 48
      },
      "Artur Duque de Souza (MoRpHeUz), QEdje": {
        "x": 841.5,
        "y": 343.5,
        "w": 39
      },
      "Meni Livne, Hebrew": {
        "x": 703,
        "y": 342,
        "w": 48
      },
      "Rex Dieter (rdieter), Fedora": {
        "x": 434.5,
        "y": 383.5,
        "w": 63
      },
      "Ian Wadhan, KDE Games": {
        "x": 364.5,
        "y": 385.5,
        "w": 51
      },
      "Sebastian Nyström, Head of Trolltech": {
        "x": 2311.5,
        "y": 221.5,
        "w": 51
      },
      "Àlex Fiestas": {
        "x": 2189.5,
        "y": 212.5,
        "w": 63
      },
      "Danny Allen (dannya), Digest": {
        "x": 2105.5,
        "y": 204.5,
        "w": 45
      },
      "Claudi Covaci (clau30), Marble": {
        "x": 2015,
        "y": 201,
        "w": 48
      },
      "Vincent Untz, Gnome, Novell": {
        "x": 1964.5,
        "y": 193.5,
        "w": 69
      },
      "Shinjo Park": {
        "x": 1947.5,
        "y": 228.5,
        "w": 51
      },
      "Oszkar Ambrus (aoszkar), Nepomuk": {
        "x": 1924,
        "y": 230,
        "w": 42
      },
      "Klaas Freitag, e.v.": {
        "x": 1900,
        "y": 205,
        "w": 36
      },
      "Wade Olson, Promo": {
        "x": 1896,
        "y": 233,
        "w": 54
      },
      "Wesley Stessens (profox)": {
        "x": 1871.5,
        "y": 250.5,
        "w": 45
      },
      "Torsten Rahn (tackat), Marble": {
        "x": 1838,
        "y": 215,
        "w": 60
      },
      "Gunnar Aastrand Grimnes (gromgull), Nepomuk": {
        "x": 1801,
        "y": 239,
        "w": 36
      },
      "Laura Josan (aprilush), Nepomuk": {
        "x": 1741.5,
        "y": 250.5,
        "w": 45
      },
      "Aaron Seigo, aseigo, El Presidento": {
        "x": 1701.5,
        "y": 254.5,
        "w": 57
      },
      "Eric Laffoon, KDE Webdev": {
        "x": 1729,
        "y": 233,
        "w": 36
      },
      "Stephan Binner (Beineri), openSUSE": {
        "x": 1715,
        "y": 233,
        "w": 24
      },
      "Antonio Larrosa Jimenez, antlarr": {
        "x": 1678,
        "y": 230,
        "w": 54
      },
      "Albert Astals Cid (tsdgeos), Poppler, Okular, i18n": {
        "x": 1623,
        "y": 240,
        "w": 36
      },
      "André Wöbbeking (WAndre), Cervisia": {
        "x": 1592.5,
        "y": 210.5,
        "w": 39
      },
      "Pau Garcia i Quiles (pgquiles), KDE Windows": {
        "x": 1567,
        "y": 239,
        "w": 42
      },
      "Rivo Laks, icon cache": {
        "x": 1475.5,
        "y": 230.5,
        "w": 39
      },
      "Teo Mrnjavac (teo_m), Amarok": {
        "x": 1453.5,
        "y": 257.5,
        "w": 45
      },
      "Daniel Molkentin (danimo), Kontact/wiki": {
        "x": 1413,
        "y": 248,
        "w": 30
      },
      "Dominik Schmidt (Domme)": {
        "x": 1389,
        "y": 238,
        "w": 30
      },
      "Aleix Pol Gonzalez": {
        "x": 1380.5,
        "y": 259.5,
        "w": 51
      },
      "Pino Toscano (pinotree), Okular": {
        "x": 1362,
        "y": 267,
        "w": 42
      },
      "Josef Spillner (josef), KDE Games": {
        "x": 1348.5,
        "y": 242.5,
        "w": 39
      },
      "Thiago Macieira (thiago), Qt": {
        "x": 1311.5,
        "y": 272.5,
        "w": 51
      },
      "Alexander Neundorf (alexxxxxx), CMake": {
        "x": 1271,
        "y": 257,
        "w": 48
      },
      "Thomas Zander (TZander), Qt, KOffice": {
        "x": 1247.5,
        "y": 282.5,
        "w": 45
      },
      "Thomas Moenicke, PHP Qt": {
        "x": 1221,
        "y": 271,
        "w": 42
      },
      "Volker Krause, KDE PIM, Akonadi": {
        "x": 1214.5,
        "y": 243.5,
        "w": 39
      },
      "Matthias Ettrich (ettrich), wrote a long e-mail a decade ago": {
        "x": 1165.5,
        "y": 258.5,
        "w": 57
      },
      "Lars Knoll, Qt, Webkit": {
        "x": 1114.5,
        "y": 289.5,
        "w": 39
      },
      "Tom Cooksey, Qt": {
        "x": 1099,
        "y": 287,
        "w": 30
      },
      "Luboš Luňák (Seli), kwin": {
        "x": 1085.5,
        "y": 254.5,
        "w": 39
      },
      "Antti Svenn, Nokia": {
        "x": 1064.5,
        "y": 257.5,
        "w": 39
      },
      "Antti Saukko, Nokia": {
        "x": 1052.5,
        "y": 277.5,
        "w": 51
      },
      "Philippe De Swert (Philippe), Nokia": {
        "x": 1009,
        "y": 292,
        "w": 24
      },
      "Ellen Reitmayr, (el), Usability": {
        "x": 1044,
        "y": 292,
        "w": 24
      },
      "Renato Chencarek (Chenca), QEdje - OpenBossa": {
        "x": 1014,
        "y": 270,
        "w": 42
      },
      "Laurent Montel (montel), Fixing Everything": {
        "x": 986.5,
        "y": 288.5,
        "w": 39
      },
      "Kate, Nokia": {
        "x": 858.5,
        "y": 301.5,
        "w": 39
      },
      "Rosalind (?), Nokia": {
        "x": 822,
        "y": 290,
        "w": 42
      },
      "Knut Yrvin, Trolltech": {
        "x": 779.5,
        "y": 295.5,
        "w": 39
      },
      "Eva Brucherseifer (eva), basysKom": {
        "x": 704,
        "y": 305,
        "w": 48
      },
      "Tobias Hunger (hunger), Decibel": {
        "x": 675,
        "y": 279,
        "w": 54
      },
      "George Wright, NX": {
        "x": 645.5,
        "y": 305.5,
        "w": 45
      },
      "Charles Samuels, noatun": {
        "x": 625,
        "y": 326,
        "w": 48
      },
      "Gunnar Schmidt (Schmi-Dt), accessibility": {
        "x": 606.5,
        "y": 330.5,
        "w": 39
      },
      "Fred Emmott, slamd64": {
        "x": 543,
        "y": 297,
        "w": 54
      },
      "Anne Wilson (annew)": {
        "x": 508.5,
        "y": 337.5,
        "w": 51
      },
      "Olaf Schmidt, accessibility": {
        "x": 500,
        "y": 314,
        "w": 42
      },
      "Than Ngo (than), Red Hat": {
        "x": 470.5,
        "y": 330.5,
        "w": 57
      },
      "Eugene Trounev": {
        "x": 433,
        "y": 335,
        "w": 54
      },
      "Barbara Wischhoefer, not just a user ": {
        "x": 418,
        "y": 329,
        "w": 42
      },
      "Bero Rosenkraenzer (bero), Ark Linux": {
        "x": 381.5,
        "y": 319.5,
        "w": 57
      }
    },
    "photo": "2008/groupphoto/akademy-2008-group-photo.jpg"
  },
  "2009": {
    "people": {
      "Knut Yrvin, Qt": {
        "x": 1842,
        "y": 1569,
        "w": 168
      },
      "Jos Poortvliet (jospoortvliet), marketing ": {
        "x": 3132,
        "y": 1275,
        "w": 114
      },
      "Torsten Rahn (tackat), Marble, Basyskom": {
        "x": 2973.5,
        "y": 1250.5,
        "w": 123
      },
      "Yolla Indria (yolla), Controls Ariya": {
        "x": 2796,
        "y": 1400,
        "w": 126
      },
      "Frederik Gladhorn (fregl), Distracting Nightrose": {
        "x": 2635,
        "y": 1333,
        "w": 132
      },
      "Pradeepto Bhattacharya (pradeepto), KDE India": {
        "x": 2524,
        "y": 1351,
        "w": 120
      },
      "Riccardo Iaconelli (ruphy), Oxygen": {
        "x": 2403,
        "y": 1337,
        "w": 126
      },
      "Seb Ruiz (sebr), Amarok": {
        "x": 2141.5,
        "y": 1253.5,
        "w": 141
      },
      "Kevin Krammer (krake), Akonadi": {
        "x": 1889.5,
        "y": 1377.5,
        "w": 135
      },
      "Artur Duque de Souza (morpheuz), openBossa, Plasma Netbook": {
        "x": 1672,
        "y": 1368,
        "w": 138
      },
      "Anne Wilson, community": {
        "x": 1445.5,
        "y": 1373.5,
        "w": 159
      },
      "Caio Marcelo de Oliveira Filho (cmarcelo), OpenBossa, INdT": {
        "x": 1194,
        "y": 1335,
        "w": 180
      },
      "Ana Cecília Martins (anniec), Plasma": {
        "x": 1021,
        "y": 1353,
        "w": 144
      },
      "Eduardo Fleury, OpenBossa, INdT": {
        "x": 783,
        "y": 1323,
        "w": 156
      },
      "Neja Repinc, translations": {
        "x": 586,
        "y": 1367,
        "w": 162
      },
      "Wendy Van Craen (CRAEW), promo &amp; events": {
        "x": 463.5,
        "y": 1431.5,
        "w": 135
      },
      "Sherry Lochhaas, makes Ian look good": {
        "x": 315,
        "y": 1384,
        "w": 126
      },
      "Ian Monroe (eean), Amarok": {
        "x": 183.5,
        "y": 1361.5,
        "w": 159
      },
      "137 xxx": {
        "x": 3154.5,
        "y": 1189.5,
        "w": 147
      },
      "Kenny Duffus (seaLne), Akademy organiser": {
        "x": 3051,
        "y": 1166,
        "w": 120
      },
      "Sebastian Trueg (trueg), K3b": {
        "x": 2968.5,
        "y": 1143.5,
        "w": 111
      },
      "Casper van Donderen, (cvandonderen), Kolf 2": {
        "x": 2855,
        "y": 1197,
        "w": 126
      },
      "Richard Moore, Javascript": {
        "x": 2756,
        "y": 1266,
        "w": 126
      },
      "Ingo Klöcker, KMail": {
        "x": 2687,
        "y": 1165,
        "w": 114
      },
      "Meni Livne, Hebrew": {
        "x": 2576.5,
        "y": 1231.5,
        "w": 129
      },
      "Holger Schröder, KDE Windows": {
        "x": 2488,
        "y": 1185,
        "w": 102
      },
      "Marco Martin (notmart), Plasma": {
        "x": 2408.5,
        "y": 1099.5,
        "w": 135
      },
      "Ariya Hidayat (ariya), Qt": {
        "x": 2292.5,
        "y": 1209.5,
        "w": 129
      },
      "Claudia Rauch (claudiar), organises us": {
        "x": 1998,
        "y": 1158,
        "w": 120
      },
      "Dan Leinir Turthra Jensen (leinir), Amarok": {
        "x": 1950.5,
        "y": 1249.5,
        "w": 117
      },
      "Pau Garcia i Quiles, KDE Windows": {
        "x": 1859.5,
        "y": 1173.5,
        "w": 123
      },
      "Carolina Segarra, stops Pau misbehaving": {
        "x": 1795.5,
        "y": 1190.5,
        "w": 105
      },
      "Leo Franchi (lfranchi), Amarok": {
        "x": 1695.5,
        "y": 1223.5,
        "w": 153
      },
      "Anselmolsm, OpenBossa, INdT, BugSquad": {
        "x": 1591.5,
        "y": 1185.5,
        "w": 111
      },
      "Alex Merry (randomguy3)": {
        "x": 1471,
        "y": 1246,
        "w": 132
      },
      "John Layt, printing": {
        "x": 1289,
        "y": 1220,
        "w": 126
      },
      "Jesus, OpenBossa, INdT": {
        "x": 993.5,
        "y": 1232.5,
        "w": 147
      },
      "Andre Moreira Magalh?es (andrunko), Collabora": {
        "x": 867,
        "y": 1237,
        "w": 126
      },
      "Jure Repinc, translations": {
        "x": 685.5,
        "y": 1222.5,
        "w": 141
      },
      "Gunnar Schmidt, Accessibility": {
        "x": 292.5,
        "y": 1284.5,
        "w": 129
      },
      "Ken Wimer (kwwii), Artwork, Canonical": {
        "x": 3070,
        "y": 1045,
        "w": 90
      },
      "Kevin Ottens (ervin), Solid, KDAB": {
        "x": 3033,
        "y": 1114,
        "w": 66
      },
      "Marius Bugge Monsen (mbm), Qt": {
        "x": 2846,
        "y": 1072,
        "w": 108
      },
      "Alexis Ménard (darktears), Qt, Plasma": {
        "x": 2739.5,
        "y": 1070.5,
        "w": 105
      },
      "Christoph Cullmann, Kate": {
        "x": 2599.5,
        "y": 1115.5,
        "w": 99
      },
      "Dominik Haumann, Kate": {
        "x": 2504.5,
        "y": 1096.5,
        "w": 81
      },
      "Martin Gräßlin (mgraesslin), KWin": {
        "x": 2396,
        "y": 1070,
        "w": 66
      },
      "Adrian Schröter, adrianS, SUSE": {
        "x": 2298,
        "y": 1105,
        "w": 96
      },
      "Frank Karlitschek (karli), openDesktop.org, e.V. Vice President": {
        "x": 2217,
        "y": 1117,
        "w": 108
      },
      "Mauro Iazzi, Lua Qt bindings": {
        "x": 1960.5,
        "y": 1067.5,
        "w": 93
      },
      "105 xxx": {
        "x": 1854.5,
        "y": 1077.5,
        "w": 111
      },
      "Jos van den Oever (vandenoever), Strigi, KO GmbH": {
        "x": 1673.5,
        "y": 1076.5,
        "w": 129
      },
      "103 xxx": {
        "x": 1468,
        "y": 1126,
        "w": 114
      },
      "Thomas McGuire (tmcguire), KMail, KDAB": {
        "x": 1346,
        "y": 1172,
        "w": 108
      },
      "Peter Zhou (peterzl), Amarok": {
        "x": 1115.5,
        "y": 1194.5,
        "w": 117
      },
      "Inge Wallin (ingwa), KOffice": {
        "x": 944,
        "y": 1131,
        "w": 132
      },
      "Abner Silva (abner), Collabora": {
        "x": 888,
        "y": 1153,
        "w": 108
      },
      "Jeff Mitchell (jefferai), Amarok": {
        "x": 829,
        "y": 1105,
        "w": 102
      },
      "Bart Cerneels (Stecchino), Amarok": {
        "x": 718.5,
        "y": 1143.5,
        "w": 99
      },
      "Celeste Lyn Paul (seele), Usability, Kubuntu, e.V.": {
        "x": 564,
        "y": 1194,
        "w": 90
      },
      "David Jarvie, KAlarm": {
        "x": 449,
        "y": 1209,
        "w": 102
      },
      "Chani Armitage (Chani), Plasma, Git thing": {
        "x": 396,
        "y": 1140,
        "w": 102
      },
      "Kevin Kin-foo (kevinkinfoo), KBugBuster": {
        "x": 2945,
        "y": 1027,
        "w": 90
      },
      "Aliona Kuznetsov, Step": {
        "x": 2922,
        "y": 1049,
        "w": 60
      },
      "Vladimir Kuznetsov (ksvladimir), Step": {
        "x": 2826,
        "y": 1006,
        "w": 90
      },
      "90 xxx": {
        "x": 2764.5,
        "y": 1011.5,
        "w": 81
      },
      "": {
        "x": 2705.5,
        "y": 1005.5,
        "w": 81
      },
      "Diederik van der Boor (vdboor), KMess": {
        "x": 2613,
        "y": 1046,
        "w": 96
      },
      "Niels Slot (nielsslot), KTurtle": {
        "x": 2510,
        "y": 1028,
        "w": 90
      },
      "Didier Fleury (mcFleury)": {
        "x": 2337.5,
        "y": 1029.5,
        "w": 75
      },
      "Robert Knight (robertknight), Konsole": {
        "x": 2011,
        "y": 1045,
        "w": 90
      },
      "Romain Pokrzywka (romain_kdab), KDAB": {
        "x": 1882.5,
        "y": 1002.5,
        "w": 93
      },
      "Erlend Hamberg, Kate": {
        "x": 1800.5,
        "y": 1001.5,
        "w": 99
      },
      "Bernhard Beschow (beschow), Kate": {
        "x": 1661,
        "y": 994,
        "w": 96
      },
      "81 xxx": {
        "x": 1556.5,
        "y": 1038.5,
        "w": 123
      },
      "Shinjo Park (peremen), KDE Korea": {
        "x": 1460.5,
        "y": 1044.5,
        "w": 105
      },
      "Sven Krohlas (sven423), Amarok": {
        "x": 1309.5,
        "y": 1102.5,
        "w": 99
      },
      "Martin Aumüller (aumull), Amarok": {
        "x": 1111,
        "y": 1113,
        "w": 96
      },
      "Alejandro Daniel Wainzinger (xevix), Amarok": {
        "x": 1045,
        "y": 1103,
        "w": 90
      },
      "Cornelius Schumacher (CSchuma), El Presidente": {
        "x": 929,
        "y": 1041,
        "w": 96
      },
      "Jose Millan Soto": {
        "x": 851.5,
        "y": 1034.5,
        "w": 87
      },
      "Laura Dragan, Nepomuk": {
        "x": 765.5,
        "y": 1074.5,
        "w": 87
      },
      "73 xxx": {
        "x": 644,
        "y": 1095,
        "w": 96
      },
      "Justin Kirby, keeps usability team happy": {
        "x": 574.5,
        "y": 1091.5,
        "w": 99
      },
      "Antti Saukko, Nokia": {
        "x": 2890,
        "y": 957,
        "w": 90
      },
      "George Goldberg (grundelburg), Telepathy &amp; KDE": {
        "x": 2763,
        "y": 950,
        "w": 84
      },
      "69 xxx": {
        "x": 2653,
        "y": 942,
        "w": 78
      },
      "Andreas Hartmetz (maelcum), KDELibs": {
        "x": 2600,
        "y": 977,
        "w": 84
      },
      "Kenny Coyle (automatical), Akademy work team": {
        "x": 2523,
        "y": 966,
        "w": 84
      },
      "Cedric Descamps, Akonadi on N810": {
        "x": 2408,
        "y": 990,
        "w": 84
      },
      "Jaroslav Řezník (jreznik), Fedora/RHEL KDE developer": {
        "x": 2329.5,
        "y": 961.5,
        "w": 81
      },
      "Gael Courcelle, Akonadi on N810": {
        "x": 2272,
        "y": 981,
        "w": 84
      },
      "Volker Krause (vkrause), KDAB": {
        "x": 2213.5,
        "y": 981.5,
        "w": 93
      },
      "Stephen Kelly (steveire), KJots, Akonadi, KDAB": {
        "x": 2016,
        "y": 986,
        "w": 72
      },
      "Jean-Nicolas Artaud, KOffice": {
        "x": 1988.5,
        "y": 957.5,
        "w": 63
      },
      "Will Stephenson (wstephenson), SUSE": {
        "x": 1936.5,
        "y": 962.5,
        "w": 63
      },
      "Alex Spehr (blahzahl), BugSquad": {
        "x": 1786.5,
        "y": 964.5,
        "w": 63
      },
      "Nikolaj Hald Nielsen (nhnfreespirit), Amarok": {
        "x": 1661,
        "y": 955,
        "w": 60
      },
      "Marijn Kruisselbrink (Mek), KOffice, Maemo": {
        "x": 1580.5,
        "y": 952.5,
        "w": 93
      },
      "Szymon Stefanek (pragma), KMail": {
        "x": 1481,
        "y": 970,
        "w": 84
      },
      "Alfredo Beaumont Sainz, KFormula": {
        "x": 1377.5,
        "y": 1002.5,
        "w": 93
      },
      "Albert Astals Cid (tsdgeos), Okular, Translations": {
        "x": 1302.5,
        "y": 1015.5,
        "w": 105
      },
      "Mikel Olasagasti": {
        "x": 1226.5,
        "y": 990.5,
        "w": 93
      },
      "Miquel Canes": {
        "x": 1179.5,
        "y": 978.5,
        "w": 75
      },
      "Alexander Dymo, KDevelop": {
        "x": 1146.5,
        "y": 1042.5,
        "w": 93
      },
      "Victor Blázquez": {
        "x": 1120,
        "y": 1012,
        "w": 66
      },
      "Aleix Pol Gonzalez, KDevelop, KDE Edu": {
        "x": 1059,
        "y": 1022,
        "w": 90
      },
      "Thiago Macieira (thiago), Qt, Git": {
        "x": 1064,
        "y": 965,
        "w": 72
      },
      "Rafael Fernández López (ereslibre), KDE Libs": {
        "x": 986,
        "y": 987,
        "w": 84
      },
      "Àlex Fiestas": {
        "x": 893.5,
        "y": 984.5,
        "w": 81
      },
      "45 xxx": {
        "x": 837,
        "y": 980,
        "w": 72
      },
      "Andrew Manson, Marble": {
        "x": 773.5,
        "y": 992.5,
        "w": 87
      },
      "Eva Brucherseifer (eva), Basyskom": {
        "x": 705,
        "y": 1003,
        "w": 90
      },
      "Pedro Jurado Maqueda, KDE Hispano": {
        "x": 608,
        "y": 1032,
        "w": 90
      },
      "Sebastian Kügler (sebas), e.V. board, Marketing, KDAB": {
        "x": 459,
        "y": 1047,
        "w": 102
      },
      "Tomaz Canabrava (tumaix), KDE Edu": {
        "x": 2860.5,
        "y": 877.5,
        "w": 99
      },
      "Jonathan Riddell (Riddell), Kubuntu, Canonical": {
        "x": 2833,
        "y": 924,
        "w": 54
      },
      "Justin Power": {
        "x": 2714,
        "y": 893,
        "w": 96
      },
      "David Barth (davidbarth), Canonical Desktop Experience": {
        "x": 2653,
        "y": 916,
        "w": 54
      },
      "Michael Jansen (mjansen), KDELibs, KHotKeys": {
        "x": 2542,
        "y": 829,
        "w": 102
      },
      "Nuno Pinheiro (pinheiro), Oxygen God": {
        "x": 2445.5,
        "y": 915.5,
        "w": 93
      },
      "Ramon Zarazua, KDevelop": {
        "x": 2411.5,
        "y": 900.5,
        "w": 69
      },
      "Frank Osterfeld (fosterfeld), KDAB": {
        "x": 2355,
        "y": 915,
        "w": 78
      },
      "Josef Spillner (spillner), KDE Games": {
        "x": 2311,
        "y": 907,
        "w": 60
      },
      "Alexandro Collerado, OpenOffice.org": {
        "x": 2236.5,
        "y": 901.5,
        "w": 87
      },
      "Mirko Böhm, KDAB": {
        "x": 2163,
        "y": 921,
        "w": 90
      },
      "Gökçen Eraslan, Pardus": {
        "x": 2149.5,
        "y": 935.5,
        "w": 45
      },
      "Allan Sandfeld Jensen (carewolf), KHTML": {
        "x": 2092,
        "y": 909,
        "w": 84
      },
      "Gökmen Göksel, Pardus": {
        "x": 2057.5,
        "y": 938.5,
        "w": 57
      },
      "Daniel Laidig (dani_l), Parley, KCharSelect": {
        "x": 1978.5,
        "y": 917.5,
        "w": 57
      },
      "Guillermo Antonio Amaral Bastidas, KDE Mexico": {
        "x": 1907,
        "y": 932,
        "w": 60
      },
      "Olivier Trichet (illogical, KDE PIM": {
        "x": 1825,
        "y": 881,
        "w": 90
      },
      "Arno Rehn, KDE Bindings": {
        "x": 1774,
        "y": 924,
        "w": 60
      },
      "Darío Andrés Rodríguez (Dario_Andres), Bugsquad Triager Extraordinaire, DrKonqi": {
        "x": 1695.5,
        "y": 911.5,
        "w": 87
      },
      "Daniel Molkentin (danimo), Qt, Wikis": {
        "x": 1594.5,
        "y": 900.5,
        "w": 63
      },
      "Alexandra Leisse (troubalex), KOffice, Qt": {
        "x": 1533,
        "y": 926,
        "w": 84
      },
      "André Wöbbeking (WAndre)": {
        "x": 1466,
        "y": 880,
        "w": 66
      },
      "Thomas Zander (TZander), KOffice, Qt": {
        "x": 1446.5,
        "y": 925.5,
        "w": 75
      },
      "Karl Vollmer (vollmer), ampache": {
        "x": 1413.5,
        "y": 952.5,
        "w": 63
      },
      "Till Adam (till), Kontact, KDAB": {
        "x": 1355.5,
        "y": 936.5,
        "w": 81
      },
      "Simon Edwards (Sime), PyKDE": {
        "x": 1292,
        "y": 902,
        "w": 90
      },
      "Luboš Luňák (Seli), KWin, SUSE": {
        "x": 1251.5,
        "y": 885.5,
        "w": 81
      },
      "Antonio Larrosa Jimenez (antlarr)": {
        "x": 1223.5,
        "y": 935.5,
        "w": 75
      },
      "Richard Dale (rdale), Ruby &amp; C# bindings": {
        "x": 1112.5,
        "y": 939.5,
        "w": 81
      },
      "Sebastian Sauer (dipesh), Kross": {
        "x": 1078.5,
        "y": 920.5,
        "w": 75
      },
      "Adriaan de Groot (ade), Vice Presedent, Whip wielder": {
        "x": 1020,
        "y": 934,
        "w": 78
      },
      "Olivier Goffart (gof), Kopete, Qt": {
        "x": 960.5,
        "y": 922.5,
        "w": 81
      },
      "Paul Adams (padams), KDE Research": {
        "x": 913.5,
        "y": 894.5,
        "w": 87
      },
      "Sandro Andrade (sandroandrade), KDevelop": {
        "x": 875.5,
        "y": 924.5,
        "w": 81
      },
      "Klaas Freitag, ex-board, Kraft": {
        "x": 793,
        "y": 916,
        "w": 96
      },
      "Danny Glassy, fonts": {
        "x": 737.5,
        "y": 950.5,
        "w": 75
      },
      "Mike Arthur (mikearthur), KDE PIM, KDAB": {
        "x": 699,
        "y": 935,
        "w": 84
      },
      "Agustín Benito Bethencourt (toscalix), GCDS organiser": {
        "x": 667,
        "y": 928,
        "w": 66
      },
      "David Faure (dfaure), Akademy Award Winner": {
        "x": 609,
        "y": 949,
        "w": 96
      },
      "Prakash Mohan (praksh), KStars": {
        "x": 525,
        "y": 935,
        "w": 114
      }
    },
    "photo": "2009/groupphoto/akademy-2009-group-photo.jpg"
  },
  "2010": {
    "people": {
      "Aaron Seigo (aseigo), chief morale officer": {
        "x": 1778.5,
        "y": 519.5,
        "w": 117
      },
      "Renato Chencarek (Chenca), INdT": {
        "x": 1681.5,
        "y": 507.5,
        "w": 117
      },
      "Eduardo Fleury, INdT": {
        "x": 1585,
        "y": 517,
        "w": 114
      },
      "Alexis Ménard (darktears), Nokia": {
        "x": 1547,
        "y": 544,
        "w": 72
      },
      "Leo Sobral Cunha, Nokia": {
        "x": 1441,
        "y": 532,
        "w": 114
      },
      "Alan Alpert, Nokia, Qt Quick": {
        "x": 1340,
        "y": 538,
        "w": 120
      },
      "7 xxx (young Knut Irvin?)": {
        "x": 1285.5,
        "y": 542.5,
        "w": 87
      },
      "8 xxx": {
        "x": 1179,
        "y": 527,
        "w": 108
      },
      "Àlex Fiestas (afiestas), Bluetooth": {
        "x": 1085,
        "y": 541,
        "w": 102
      },
      "Marius Bugge Monsen (mbm), Model View, Nokia": {
        "x": 1001,
        "y": 554,
        "w": 84
      },
      "Alexandra Leisse (troubalex), Qt, Nokia": {
        "x": 921.5,
        "y": 549.5,
        "w": 87
      },
      "Thomas Thym (ungethym)": {
        "x": 833.5,
        "y": 546.5,
        "w": 99
      },
      "Davide Bettio (WindowsUninstall), Plasma": {
        "x": 740.5,
        "y": 559.5,
        "w": 93
      },
      "Lydia Pintscher (Nightrose), Amarok, CWG": {
        "x": 663,
        "y": 576,
        "w": 84
      },
      "Artur Duque de Souza, Plasma": {
        "x": 584,
        "y": 575,
        "w": 84
      },
      "Rodrigo Vivi, Collabora": {
        "x": 473,
        "y": 553,
        "w": 90
      },
      "Felipe Zimmerlee, Collabora": {
        "x": 380.5,
        "y": 568.5,
        "w": 87
      },
      "Richard Larsson, KDE User": {
        "x": 347,
        "y": 519,
        "w": 78
      },
      "Henri Bergius (bergie), GeoClue, Midgard": {
        "x": 273.5,
        "y": 562.5,
        "w": 87
      },
      "Tomaz Canabrava (tomaz), Rocs": {
        "x": 169.5,
        "y": 559.5,
        "w": 105
      },
      "Jesper Thomschütz (jesperht), Qt, Nokia": {
        "x": 87.5,
        "y": 566.5,
        "w": 99
      },
      "Harri Rantala, University of Tampere": {
        "x": 1875,
        "y": 421,
        "w": 90
      },
      "Neja Repinc (SmrtSkoso), User": {
        "x": 1788.5,
        "y": 432.5,
        "w": 99
      },
      "Jure Repinc (JLP), Translation": {
        "x": 1741.5,
        "y": 428.5,
        "w": 87
      },
      "Leo Franchi (lfranchi), KDAB, Amarok": {
        "x": 1679.5,
        "y": 422.5,
        "w": 87
      },
      "Patrick Spendrin (SaroEngels), KDAB, KDE on Windows": {
        "x": 1611.5,
        "y": 427.5,
        "w": 75
      },
      "Frederik Gladhorn (fregl), Attica, Fluffy": {
        "x": 1538,
        "y": 429,
        "w": 84
      },
      "Inge Wallin (ingwa), KO GmbH, KOffice": {
        "x": 1467.5,
        "y": 435.5,
        "w": 75
      },
      "Allan Sandfeld Jensen (carewolf), KHTML": {
        "x": 1399.5,
        "y": 449.5,
        "w": 81
      },
      "Torsten Rahn (tackat), basysKom, Marble": {
        "x": 1323,
        "y": 435,
        "w": 84
      },
      "Michelangelo Marchesi (markezi), SjLUG Seinäjoki": {
        "x": 1224,
        "y": 437,
        "w": 84
      },
      "George Goldberg (grundelburg), Telepathy, Collabora": {
        "x": 1144,
        "y": 448,
        "w": 84
      },
      "Michael Leupold (lemma), Secret Service": {
        "x": 1055,
        "y": 440,
        "w": 96
      },
      "Jaroslav Řezník (jreznik), Fedora, RedHat": {
        "x": 966,
        "y": 447,
        "w": 84
      },
      "Pradeepto Bhattacharya (pradeepto), KDE India": {
        "x": 875,
        "y": 454,
        "w": 96
      },
      "Markus Goetz (mgoetz), Nokia": {
        "x": 790.5,
        "y": 458.5,
        "w": 69
      },
      "Olivier Goffart (gof), Nokia": {
        "x": 704,
        "y": 455,
        "w": 84
      },
      "Miia Ranta (Myrtti), Nomovok": {
        "x": 606,
        "y": 460,
        "w": 90
      },
      "Alexey Zakhlestin, Midgard": {
        "x": 527.5,
        "y": 462.5,
        "w": 75
      },
      "Justin Kirby (neomantra), Promo": {
        "x": 443.5,
        "y": 459.5,
        "w": 75
      },
      "John Layt (johnlayt), Printing": {
        "x": 353,
        "y": 465,
        "w": 72
      },
      "Michael Casadeval (NCommander), ARM, Canonical": {
        "x": 238.5,
        "y": 505.5,
        "w": 81
      },
      "Matthias Welwarsky (thinkfat), Archos, digiKam": {
        "x": 175,
        "y": 501,
        "w": 84
      },
      "Sergio Martins, KDAB, KDE PIM": {
        "x": 2098.5,
        "y": 329.5,
        "w": 87
      },
      "Kevin Ottens (ervin), KDAB, several pies": {
        "x": 2025,
        "y": 325,
        "w": 84
      },
      "Eduardo Robles Elvira (edulix) Konqueror": {
        "x": 1953.5,
        "y": 327.5,
        "w": 81
      },
      "Kevin Krammer (krake), KDAB, KDE PIM": {
        "x": 1884,
        "y": 346,
        "w": 78
      },
      "Ivan Čukić (ivan), Plasma, Lancelot": {
        "x": 1822.5,
        "y": 329.5,
        "w": 81
      },
      "David Jarvie, KAlarm": {
        "x": 1770.5,
        "y": 340.5,
        "w": 75
      },
      "Albert Astals Cid (tsdgeos), l10n, Poppler, Okular": {
        "x": 1704,
        "y": 348,
        "w": 84
      },
      "Dominik Haumann (dhaumann), Kate": {
        "x": 1649.5,
        "y": 354.5,
        "w": 69
      },
      "Christoph Cullmann (cullmann), Kate": {
        "x": 1581.5,
        "y": 354.5,
        "w": 75
      },
      "Joseph Wenninger (jowenn), Kate": {
        "x": 1505,
        "y": 360,
        "w": 72
      },
      "Arno Rehn (pumphaus), KDE Bindings": {
        "x": 1433.5,
        "y": 363.5,
        "w": 81
      },
      "55 xxx": {
        "x": 1360.5,
        "y": 367.5,
        "w": 75
      },
      "56 xxx": {
        "x": 1299.5,
        "y": 366.5,
        "w": 75
      },
      "Stefan Majewsky (majewsky), KDE Games": {
        "x": 1243.5,
        "y": 373.5,
        "w": 63
      },
      "Kacper Gazda": {
        "x": 1173,
        "y": 371,
        "w": 66
      },
      "Jarosław Staniek (jstaniek), Kexi": {
        "x": 1100.5,
        "y": 372.5,
        "w": 63
      },
      "Anna Gritsay": {
        "x": 1050.5,
        "y": 391.5,
        "w": 51
      },
      "Anton Gritsay": {
        "x": 991.5,
        "y": 394.5,
        "w": 57
      },
      "Joonas Sarajärvi (muep)": {
        "x": 989.5,
        "y": 361.5,
        "w": 45
      },
      "Saija Saarenpää (matrixx), Tieto": {
        "x": 928,
        "y": 400,
        "w": 54
      },
      "64 xxx": {
        "x": 927.5,
        "y": 367.5,
        "w": 45
      },
      "Anssi Eteläniemi": {
        "x": 856.5,
        "y": 376.5,
        "w": 81
      },
      "Jesse Janhonen": {
        "x": 791.5,
        "y": 380.5,
        "w": 63
      },
      "Holger Schröder (hsch), basysKom": {
        "x": 761,
        "y": 371,
        "w": 48
      },
      "Victor Blázquez, Maemo thingies": {
        "x": 716,
        "y": 389,
        "w": 66
      },
      "Pedro Jurado Maqueda, KDE Hispano": {
        "x": 652.5,
        "y": 380.5,
        "w": 69
      },
      "Jose Millan Soto": {
        "x": 567.5,
        "y": 383.5,
        "w": 69
      },
      "Mateu Batle, Collabora": {
        "x": 501.5,
        "y": 385.5,
        "w": 69
      },
      "Luciano Montanaro, KDE Games": {
        "x": 480,
        "y": 350,
        "w": 60
      },
      "Agustín Benito Bethencourt (Toscalix)": {
        "x": 420.5,
        "y": 386.5,
        "w": 69
      },
      "Oskari Kokko (NetBlade), Midgard": {
        "x": 390,
        "y": 358,
        "w": 54
      },
      "Dirk Hohndel, Intel": {
        "x": 355.5,
        "y": 421.5,
        "w": 57
      },
      "76 xxx": {
        "x": 328.5,
        "y": 385.5,
        "w": 63
      },
      "Sebastian Kügler (sebas), Open-SLX": {
        "x": 272.5,
        "y": 420.5,
        "w": 63
      },
      "Declan McGrath, KDE with Rails": {
        "x": 243.5,
        "y": 377.5,
        "w": 75
      },
      "Richard Dale (rdale), KDE Bindings": {
        "x": 2351,
        "y": 254,
        "w": 60
      },
      "Harri Porten (harri), Froglogic": {
        "x": 2268.5,
        "y": 253.5,
        "w": 63
      },
      "André Wöbbeking (WAndre)": {
        "x": 2179.5,
        "y": 248.5,
        "w": 75
      },
      "Ingo Klöcker (mahoutsukai), KDE PIM": {
        "x": 2111.5,
        "y": 255.5,
        "w": 69
      },
      "Leila Sundelin": {
        "x": 2052,
        "y": 268,
        "w": 54
      },
      "Kaj-Michael Lang (onion)": {
        "x": 1981.5,
        "y": 264.5,
        "w": 69
      },
      "Rudy Commenge (RudyWI)": {
        "x": 1913.5,
        "y": 283.5,
        "w": 75
      },
      "Ludovic Deveaux (ldeveaux)": {
        "x": 1852,
        "y": 280,
        "w": 66
      },
      "Benjamin Port (bport - ben2367)": {
        "x": 1791.5,
        "y": 289.5,
        "w": 69
      },
      "Jean-Nicolas Artaud (morice-net)": {
        "x": 1746,
        "y": 293,
        "w": 66
      },
      "89 xxx": {
        "x": 1673,
        "y": 293,
        "w": 66
      },
      "Martin Gräßlin (mgraesslin), KWin": {
        "x": 1620.5,
        "y": 298.5,
        "w": 57
      },
      "Otto Kopra, Nokia": {
        "x": 1545,
        "y": 297,
        "w": 72
      },
      "Alex Spehr (blauzahl), bugsquad chick, Froglogic": {
        "x": 1493,
        "y": 305,
        "w": 60
      },
      "Marijn Kruisselbrink (Mek), KSpread, KO GmbH": {
        "x": 1442,
        "y": 300,
        "w": 66
      },
      "Robin Appelman (icewind), ownCloud": {
        "x": 1386.5,
        "y": 310.5,
        "w": 63
      },
      "Lasse Liehu (lliehu), a few patches here and there": {
        "x": 1328,
        "y": 308,
        "w": 66
      },
      "David Laban (alsuren), Collabora, Telepathy": {
        "x": 1286,
        "y": 306,
        "w": 60
      },
      "Tobias Koenig (tokoe), KDAB, KDE PIM": {
        "x": 1231.5,
        "y": 315.5,
        "w": 63
      },
      "Tommi Rantala": {
        "x": 1180.5,
        "y": 306.5,
        "w": 63
      },
      "Thomas McGuire (tcmguire), KDAB, KMail Maintainer": {
        "x": 1114,
        "y": 319,
        "w": 60
      },
      "Volker Krause (vkrause),  KDAB, Akonadi/KDE PIM": {
        "x": 1055,
        "y": 311,
        "w": 66
      },
      "Marko Grönroos, Finnish localization": {
        "x": 998,
        "y": 316,
        "w": 54
      },
      "102 xxx": {
        "x": 906,
        "y": 321,
        "w": 72
      },
      "Diana Tiriplica (dainat), Kate": {
        "x": 832.5,
        "y": 322.5,
        "w": 69
      },
      "Victor Carbune (carbonix), KStar": {
        "x": 766.5,
        "y": 317.5,
        "w": 63
      },
      "Marco Martin (notmart), Plasma": {
        "x": 696,
        "y": 324,
        "w": 60
      },
      "Alessandro Diaferia (alediaferia), Plasma": {
        "x": 635.5,
        "y": 328.5,
        "w": 57
      },
      "David Vignoni (davigno), Oxygen": {
        "x": 578,
        "y": 326,
        "w": 48
      },
      "Troy Unrau (troy), KDE Adventurer at Large": {
        "x": 542.5,
        "y": 301.5,
        "w": 51
      },
      "Jerry Jalava, Midgard": {
        "x": 508,
        "y": 326,
        "w": 42
      },
      "Jesse Vartiainen": {
        "x": 424,
        "y": 323,
        "w": 60
      },
      "Riku Itäpuro (rostbach), helper in the bus": {
        "x": 342,
        "y": 321,
        "w": 54
      },
      "Timo Juhani Lindfors (lindi)": {
        "x": 2562.5,
        "y": 163.5,
        "w": 75
      },
      "Aleksi Hankalahti (ath)": {
        "x": 2508,
        "y": 180,
        "w": 66
      },
      "Luca Gugelmann": {
        "x": 2445,
        "y": 181,
        "w": 60
      },
      "Casper van Donderen, KDE Windows": {
        "x": 2389,
        "y": 189,
        "w": 60
      },
      "Claire Lotion": {
        "x": 2346,
        "y": 203,
        "w": 60
      },
      "117 xxx": {
        "x": 2280,
        "y": 192,
        "w": 66
      },
      "Tanu Kaskinen (tanuk), PulseAudio": {
        "x": 2229.5,
        "y": 199.5,
        "w": 63
      },
      "Stuart Jarvis (jakamoko), KDE Promo": {
        "x": 2169,
        "y": 200,
        "w": 60
      },
      "Hans Chen (Mogger), Forums, UserBase": {
        "x": 2101,
        "y": 202,
        "w": 72
      },
      "121 xxx": {
        "x": 2032.5,
        "y": 209.5,
        "w": 69
      },
      "122 xxx": {
        "x": 1963,
        "y": 212,
        "w": 66
      },
      "123 xxx": {
        "x": 1883.5,
        "y": 230.5,
        "w": 63
      },
      "Nuno Pinheiro (pinheiro), KDAB, Oxygen": {
        "x": 1827.5,
        "y": 231.5,
        "w": 57
      },
      "Sebastian Trueg (trueg), Nepomuk": {
        "x": 1763,
        "y": 234,
        "w": 54
      },
      "Vishesh Handa (vHanda), nepomuk": {
        "x": 1697.5,
        "y": 245.5,
        "w": 57
      },
      "Frank Karlitschek (Karli), e.V. board, openDesktop, ownCloud": {
        "x": 1635,
        "y": 248,
        "w": 60
      },
      "Jos van den Oever (vandenoever), KO GmbH, KOffice": {
        "x": 1567,
        "y": 244,
        "w": 60
      },
      "Jos Poortvliet (jospoortvliet), Dutch, Marketing": {
        "x": 1522.5,
        "y": 260.5,
        "w": 57
      },
      "Chani Armitage (Chani), Plasma": {
        "x": 1474.5,
        "y": 261.5,
        "w": 51
      },
      "Ryan Rix (rrix), Plasma, Fedora, KDEPIM": {
        "x": 1413.5,
        "y": 257.5,
        "w": 63
      },
      "132 xxx": {
        "x": 1363,
        "y": 264,
        "w": 54
      },
      "Nikolaj Hald Nielsen, Amarok": {
        "x": 1308.5,
        "y": 266.5,
        "w": 57
      },
      "134 xxx": {
        "x": 1254,
        "y": 262,
        "w": 60
      },
      "Sune Vuorela (pusling), Debian": {
        "x": 1186.5,
        "y": 252.5,
        "w": 69
      },
      "136 xxx": {
        "x": 1125.5,
        "y": 272.5,
        "w": 51
      },
      "137 xxx": {
        "x": 1077.5,
        "y": 280.5,
        "w": 51
      },
      "138 xxx": {
        "x": 1011.5,
        "y": 262.5,
        "w": 57
      },
      "Paavo Hartikainen (pahartik)": {
        "x": 959,
        "y": 277,
        "w": 54
      },
      "Daniele E. Domenichelli (drdanz), University of Genoa, KDE-Telepathy": {
        "x": 900.5,
        "y": 279.5,
        "w": 51
      },
      "141 xxx": {
        "x": 837.5,
        "y": 267.5,
        "w": 63
      },
      "Till Adam (till), KDAB, KDE PIM": {
        "x": 794,
        "y": 270,
        "w": 54
      },
      "Mirko Böhm (miroslav), KDAB": {
        "x": 735,
        "y": 273,
        "w": 48
      },
      "Uwe Geuder (u1106), Tampere volunteer": {
        "x": 673,
        "y": 265,
        "w": 54
      },
      "145 xxx": {
        "x": 626,
        "y": 279,
        "w": 54
      },
      "Tero Heino, volunteer": {
        "x": 605.5,
        "y": 253.5,
        "w": 45
      },
      "147 xxx": {
        "x": 560,
        "y": 272,
        "w": 42
      },
      "Sanna Salo, volunteer": {
        "x": 544.5,
        "y": 253.5,
        "w": 45
      },
      "David Faure (dfaure), KDAB, Guru": {
        "x": 509.5,
        "y": 274.5,
        "w": 51
      },
      "Thiago Macieira (thiago), Nokia": {
        "x": 477,
        "y": 299,
        "w": 54
      },
      "Aurélien Gâteau (agateau), Gwenview, Kubuntu": {
        "x": 443,
        "y": 256,
        "w": 60
      },
      "臥虎藏龍": {
        "x": 409,
        "y": 279,
        "w": 66
      },
      "153 xxx": {
        "x": 369,
        "y": 269,
        "w": 60
      },
      "Amanda Oliveira Castro (amandacsi), kde lovelace": {
        "x": 2733,
        "y": 85,
        "w": 108
      },
      "Camila Ayres (camilasan), kde lovelace": {
        "x": 2679.5,
        "y": 120.5,
        "w": 69
      },
      "Ilkka Lehtinen, COSS": {
        "x": 2605.5,
        "y": 105.5,
        "w": 81
      },
      "Claudia Rauch, KDE e.V.": {
        "x": 2550,
        "y": 127,
        "w": 66
      },
      "157 xxx": {
        "x": 2482.5,
        "y": 132.5,
        "w": 63
      },
      "Arjen Hiemstra (ahiemstra), Gluon": {
        "x": 2402,
        "y": 126,
        "w": 84
      },
      "Parul Vora, Wikimedia Foundation": {
        "x": 2351,
        "y": 146,
        "w": 72
      },
      "Guillaume Paumier (guillom / gpaumier), Wikimedia Foundation": {
        "x": 2299.5,
        "y": 153.5,
        "w": 51
      },
      "Rui Dias (TheInvisible), Professional Intruder": {
        "x": 2214,
        "y": 147,
        "w": 66
      },
      "Thorsten Telke, KDE e.V.": {
        "x": 2645,
        "y": 0,
        "w": 72
      },
      "Sanna Heiskanen, SaneMind, COSS": {
        "x": 2586,
        "y": 43,
        "w": 66
      },
      "Karen Thorburn, Karenski, COSS": {
        "x": 2531,
        "y": 31,
        "w": 66
      },
      "165 xxx": {
        "x": 2444.5,
        "y": 32.5,
        "w": 63
      },
      "Nikhil Marathe (nsm), Amarok, Kwin, Rekonq": {
        "x": 2388.5,
        "y": 58.5,
        "w": 51
      },
      "167 xxx": {
        "x": 2367.5,
        "y": 61.5,
        "w": 39
      },
      "Fathi Boudra (fabo), Nokia": {
        "x": 2306,
        "y": 64,
        "w": 72
      },
      "Sacha Schutz (DrIDK), Gluon": {
        "x": 2237.5,
        "y": 37.5,
        "w": 81
      },
      "Dan Leinir Turthra Jensen (leinir), Amarok, Gluon    ": {
        "x": 2188.5,
        "y": 81.5,
        "w": 69
      },
      "Milian Wolff (milianw), KDAB, KDevelop": {
        "x": 2159,
        "y": 71,
        "w": 60
      },
      "Shinjo Park (peremen), KDE Korea": {
        "x": 2105,
        "y": 81,
        "w": 60
      },
      "Leo Savernik, KHTML": {
        "x": 2156.5,
        "y": 158.5,
        "w": 57
      },
      "Christian Fetzer": {
        "x": 2109,
        "y": 172,
        "w": 54
      },
      "Michael Zanetti (mzanetti), KRemoteControl, SlideBack, kopete_otr": {
        "x": 2054,
        "y": 172,
        "w": 60
      },
      "Peter Grasch (bedahr), simon listens": {
        "x": 2004.5,
        "y": 173.5,
        "w": 57
      },
      "Friedrich W. H. Kossebau (frinring), Okteta, network:/ kio-slave, Openismus": {
        "x": 1947,
        "y": 174,
        "w": 66
      },
      "Adriaan de Groot ([ade]), e.V. Board Member": {
        "x": 1889,
        "y": 177,
        "w": 60
      },
      "Niklas Laxström (Nikerabbit), translatewiki.net": {
        "x": 1843.5,
        "y": 192.5,
        "w": 57
      },
      "Anne Wilson (annew), Userbase, Forums": {
        "x": 1784,
        "y": 199,
        "w": 60
      },
      "181 xxx": {
        "x": 1721,
        "y": 201,
        "w": 66
      },
      "182 xxx": {
        "x": 1664.5,
        "y": 209.5,
        "w": 57
      },
      "183 xxx": {
        "x": 1608,
        "y": 194,
        "w": 72
      },
      "Martin Sandsmark (sandsmark); Phonon, Filelight, ownCloud, attica, Norway": {
        "x": 1552.5,
        "y": 201.5,
        "w": 63
      },
      "Gunnar Schmidt": {
        "x": 1501,
        "y": 210,
        "w": 60
      },
      "186 xxx": {
        "x": 1444.5,
        "y": 211.5,
        "w": 63
      },
      "Alexander Neundorf, CMake": {
        "x": 1399,
        "y": 213,
        "w": 54
      },
      "Antje Neundorf, Alexander's wife": {
        "x": 1351.5,
        "y": 213.5,
        "w": 57
      },
      "Jeremy Whiting (jpwhiting), accessibility": {
        "x": 1281,
        "y": 218,
        "w": 60
      },
      "Brandi Whiting, jpwhiting's better half (wife)": {
        "x": 1243.5,
        "y": 226.5,
        "w": 57
      },
      "191 xxx": {
        "x": 1192,
        "y": 219,
        "w": 54
      },
      "Asko Soukka, VALO-CD.fi": {
        "x": 1137,
        "y": 232,
        "w": 54
      },
      "Stephan Binner (Beineri), basysKom, openSUSE": {
        "x": 1087.5,
        "y": 228.5,
        "w": 57
      },
      "Eva Brucherseifer (eva), basysKom": {
        "x": 1033.5,
        "y": 220.5,
        "w": 63
      },
      "Dominik Schmidt (domme)": {
        "x": 985.5,
        "y": 218.5,
        "w": 57
      },
      "Julian Bäume (johnyb)": {
        "x": 931,
        "y": 223,
        "w": 66
      },
      "Jonathan Riddell (Riddell), Canonical, Kubuntu": {
        "x": 898.5,
        "y": 240.5,
        "w": 45
      },
      "Suresh Chande, KOffice, Nokia": {
        "x": 868,
        "y": 230,
        "w": 54
      },
      "Boudewĳn Rempt (boud), Krita, KO GmbH": {
        "x": 822.5,
        "y": 230.5,
        "w": 51
      },
      "Jarkko Vilhunen (Eladith)": {
        "x": 771,
        "y": 226,
        "w": 48
      },
      "Esa-Matti Suuronen (Epeli)": {
        "x": 719.5,
        "y": 230.5,
        "w": 63
      },
      "Tuomas Räsänen (tuos)": {
        "x": 677.5,
        "y": 227.5,
        "w": 45
      },
      "Joel Lehtonen (Zouppen)": {
        "x": 630.5,
        "y": 228.5,
        "w": 39
      },
      "Rivo Laks (rivo)": {
        "x": 576,
        "y": 226,
        "w": 42
      },
      "Laur Mõtus (vprints) Povi Software, Estobuntu, Manpremo": {
        "x": 526,
        "y": 227,
        "w": 48
      },
      "Kåre Särs, Skanlite, libksane": {
        "x": 481,
        "y": 239,
        "w": 60
      },
      "Oni-san": {
        "x": 459,
        "y": 177,
        "w": 60
      },
      "Andras Mantia, KDAB, Quanta": {
        "x": 412.5,
        "y": 190.5,
        "w": 51
      },
      "Sebastien Renard, french translator": {
        "x": 2056.5,
        "y": 77.5,
        "w": 45
      },
      "Dirk Müller (dirk), Novell, openSUSE": {
        "x": 2032.5,
        "y": 88.5,
        "w": 45
      },
      "Ville-Pekka Vainio (vpv), Fedora": {
        "x": 1973,
        "y": 93,
        "w": 66
      },
      "212 xxx": {
        "x": 1956.5,
        "y": 96.5,
        "w": 57
      },
      "Sasu Karttunen (skfin)": {
        "x": 1910.5,
        "y": 109.5,
        "w": 51
      },
      "Daniel Laidig (dani_l), Parley, KCharSelect": {
        "x": 1888.5,
        "y": 111.5,
        "w": 45
      },
      "Siebrand Mazeland, translatewiki.net": {
        "x": 1850.5,
        "y": 101.5,
        "w": 45
      },
      "Anssi Hannula (Anssi), Mandriva, XBMC": {
        "x": 1822,
        "y": 117,
        "w": 42
      },
      "Nina Kuisma (ninnnu), Ubuntu Finland": {
        "x": 1787.5,
        "y": 132.5,
        "w": 51
      },
      "Jukka Neppius": {
        "x": 1791,
        "y": 103,
        "w": 54
      },
      "219 xxx": {
        "x": 1744,
        "y": 120,
        "w": 42
      },
      "Sami Kiviharju": {
        "x": 1720.5,
        "y": 132.5,
        "w": 33
      },
      "Ville Jyrkkä (jykae), Ubuntu Finland, marketing and community": {
        "x": 1674.5,
        "y": 131.5,
        "w": 45
      },
      "222 xxx": {
        "x": 1654.5,
        "y": 112.5,
        "w": 45
      },
      "Heikki Mäntysaari, Ubuntu Finland, l10n": {
        "x": 1611.5,
        "y": 112.5,
        "w": 39
      },
      "Alexander Dymo (adymo), KDevelop": {
        "x": 1565.5,
        "y": 135.5,
        "w": 39
      },
      "225 xxx": {
        "x": 1542.5,
        "y": 134.5,
        "w": 39
      },
      "Iridian Kiiskinen (Iridian), libqttracker": {
        "x": 1488,
        "y": 131,
        "w": 48
      },
      "Lynoure Braakman (Lynoure)": {
        "x": 1472,
        "y": 163,
        "w": 36
      },
      "Maike Sonnewald": {
        "x": 1448,
        "y": 151,
        "w": 42
      },
      "Diederik van der Boor (vdboor), KMess": {
        "x": 1415,
        "y": 134,
        "w": 42
      },
      "Jonas Müller (zanoi)": {
        "x": 1392.5,
        "y": 151.5,
        "w": 39
      },
      "Valerio Pilo (vpilo), KMess": {
        "x": 1366,
        "y": 156,
        "w": 42
      },
      "Sjors Gielen (sjors), KMess": {
        "x": 1315,
        "y": 139,
        "w": 48
      },
      "233 xxx": {
        "x": 1272.5,
        "y": 153.5,
        "w": 39
      },
      "234 xxx": {
        "x": 1247,
        "y": 146,
        "w": 42
      },
      "Aleix Pol Gonzalez (apol), KAlgebra, KDevelop": {
        "x": 1210.5,
        "y": 147.5,
        "w": 45
      },
      "Lukáš Tvrdý (LukasT), Krita": {
        "x": 1190.5,
        "y": 142.5,
        "w": 39
      },
      "Helio Castro (heliocastro), Collabora": {
        "x": 1169,
        "y": 164,
        "w": 36
      },
      "Mauricio Piacentini (piacentini), KDEGames, KDE-br": {
        "x": 1139.5,
        "y": 149.5,
        "w": 45
      },
      "Lamarque Souza (lvsouza), Plasma NM, KDE-br": {
        "x": 1112,
        "y": 158,
        "w": 42
      },
      "Sandro Andrade": {
        "x": 1090.5,
        "y": 148.5,
        "w": 39
      },
      "Luboš Luňák (Seli), Novell, openSUSE": {
        "x": 1062.5,
        "y": 144.5,
        "w": 39
      },
      "Laszlo Papp (djszapi), Chakra": {
        "x": 1031,
        "y": 157,
        "w": 42
      },
      "John Tapsell (JohnFlux), KSysGuard, basysKom": {
        "x": 1012,
        "y": 157,
        "w": 36
      },
      "Bruno Abinader (abinader), INdT": {
        "x": 976.5,
        "y": 155.5,
        "w": 39
      },
      "Robert Ahlskog (rahlskog)": {
        "x": 949,
        "y": 149,
        "w": 42
      },
      "246 xxx": {
        "x": 919.5,
        "y": 162.5,
        "w": 39
      },
      "247 xxx": {
        "x": 901.5,
        "y": 153.5,
        "w": 33
      },
      "Paul Adams (padams), Kolabsys": {
        "x": 862,
        "y": 150,
        "w": 42
      },
      "Bertjan Broeksema, KDAB, KDE PIM": {
        "x": 823.5,
        "y": 153.5,
        "w": 39
      },
      "Celeste Lyn Paul (seele), Usability": {
        "x": 803.5,
        "y": 179.5,
        "w": 39
      },
      "Stephen Kelly (steveire), KDAB, KDE PIM, Proxy Models": {
        "x": 771.5,
        "y": 157.5,
        "w": 45
      },
      "Pasi Pekkala (Tagi_)": {
        "x": 741.5,
        "y": 151.5,
        "w": 33
      },
      "Andreas Hartmetz (maelcum), KDAB, kdelibs": {
        "x": 718.5,
        "y": 157.5,
        "w": 39
      },
      "254 xxx": {
        "x": 688,
        "y": 149,
        "w": 42
      },
      "Bernhard Beschow (shentey), Kate, Marble": {
        "x": 656.5,
        "y": 147.5,
        "w": 45
      },
      "Ville Sundell (Solarius), Finhack, Vapaakoodi, Vapaasuomi, Thank a dev day": {
        "x": 604.5,
        "y": 152.5,
        "w": 57
      },
      "Rebecca Worledge, Codemate Ltd.": {
        "x": 564.5,
        "y": 150.5,
        "w": 45
      },
      "258 xxx": {
        "x": 540.5,
        "y": 145.5,
        "w": 45
      },
      "Juha Mustonen, Codemate Ltd.": {
        "x": 504,
        "y": 138,
        "w": 54
      },
      "Lauri Võsandi (lauri), Povi Software OÜ": {
        "x": 438,
        "y": 142,
        "w": 60
      },
      "261 xxx": {
        "x": 364,
        "y": 166,
        "w": 54
      },
      "Akarsh Simha (kstar)": {
        "x": 341.5,
        "y": 407.5,
        "w": 39
      }
    },
    "photo": "2010/groupphoto/akademy-2010-group-photo.jpg"
  },
  "2011": {
    "people": {
      "1 xxx": {
        "x": 2890,
        "y": 707,
        "w": 114
      },
      "2 xxx": {
        "x": 2858,
        "y": 776,
        "w": 126
      },
      "Larissa (Larifari), Tomahawk": {
        "x": 2793.5,
        "y": 948.5,
        "w": 123
      },
      "Dominik Schmidt (domme), Tomahawk": {
        "x": 2805,
        "y": 855,
        "w": 114
      },
      "Ursl, Tomahawk": {
        "x": 2721,
        "y": 962,
        "w": 96
      },
      "Christian Muehlhaeuser (muesli), Tomahawk": {
        "x": 2670,
        "y": 892,
        "w": 102
      },
      "Atul Jha,India": {
        "x": 2755,
        "y": 851,
        "w": 90
      },
      "Bradley M Kuhn, FSF Board (bkuhn)": {
        "x": 2634,
        "y": 819,
        "w": 96
      },
      "Michael Zanetti (mzanetti)": {
        "x": 2569.5,
        "y": 919.5,
        "w": 117
      },
      "Antonio Larrosa Jimenez (antlarr), Akademy 2005 organizer": {
        "x": 2473,
        "y": 920,
        "w": 96
      },
      "Mart Raudsepp (leio), Collabora/Gentoo": {
        "x": 2414.5,
        "y": 899.5,
        "w": 93
      },
      "María, antlarr better half": {
        "x": 2334.5,
        "y": 970.5,
        "w": 99
      },
      "Arun Raghavan (Ford_Prefect), Collabora/PulseAudio": {
        "x": 2318.5,
        "y": 915.5,
        "w": 87
      },
      "Johan Dahlin, gobject-introspection": {
        "x": 2206,
        "y": 921,
        "w": 102
      },
      "Colin Guthrie (coling), Pulseaudio": {
        "x": 2160,
        "y": 884,
        "w": 90
      },
      "Wim Taymans, Collabora/GStreamer": {
        "x": 2082,
        "y": 919,
        "w": 120
      },
      "Eitan Isaacson": {
        "x": 2056,
        "y": 850,
        "w": 102
      },
      "Jonas Emanuel Mueller (zanoi)": {
        "x": 1953.5,
        "y": 922.5,
        "w": 105
      },
      "João Paulo Rechi Vita (jprvita), GNOME GSOC student": {
        "x": 1938,
        "y": 823,
        "w": 78
      },
      "Patricia Santana Cruz": {
        "x": 1813.5,
        "y": 785.5,
        "w": 99
      },
      "Kristen Nielsen": {
        "x": 1837.5,
        "y": 911.5,
        "w": 99
      },
      "Izidor Matušov, GNOME GSoC student, Getting Things GNOME!": {
        "x": 1715.5,
        "y": 927.5,
        "w": 99
      },
      "23 xxx Mirsal": {
        "x": 1751,
        "y": 860,
        "w": 96
      },
      "Danilo Cesar Lemes de Paula, Collabora": {
        "x": 1703,
        "y": 802,
        "w": 90
      },
      "Øyvind Kolås (pippin), Intel OTC, Clutter": {
        "x": 1605.5,
        "y": 939.5,
        "w": 105
      },
      "Stuart Jarvis, KDE Promo": {
        "x": 1508.5,
        "y": 990.5,
        "w": 99
      },
      "Valério Valério": {
        "x": 1620,
        "y": 884,
        "w": 78
      },
      "28 xxx": {
        "x": 1574,
        "y": 815,
        "w": 90
      },
      "Lennart Poettering, Pulseaudio/systemd/Red Hat, secret in love with KDE": {
        "x": 1506.5,
        "y": 890.5,
        "w": 99
      },
      "30 xxx": {
        "x": 1500,
        "y": 794,
        "w": 84
      },
      "Tim-Philipp Müller, Collabora": {
        "x": 1452.5,
        "y": 827.5,
        "w": 111
      },
      "32 William Jon McCann": {
        "x": 1403,
        "y": 956,
        "w": 102
      },
      "33 xxx": {
        "x": 1305,
        "y": 997,
        "w": 84
      },
      "34 xxx": {
        "x": 1359,
        "y": 878,
        "w": 102
      },
      "35 xxx": {
        "x": 1376,
        "y": 819,
        "w": 78
      },
      "Giulia Dani, KDE Italian translations": {
        "x": 1207.5,
        "y": 797.5,
        "w": 93
      },
      "Jaroslav Řezník (jreznik), Fedora KDE, Red Hat": {
        "x": 1249,
        "y": 848,
        "w": 102
      },
      "Germán Póo-Caamaño, GNOME": {
        "x": 1258,
        "y": 921,
        "w": 108
      },
      "Diego Escalante": {
        "x": 1233.5,
        "y": 1002.5,
        "w": 105
      },
      "Riccardo Iaconelli (Ruphy), KDE": {
        "x": 1162,
        "y": 964,
        "w": 90
      },
      "Vladimír Beneš, Red Hat": {
        "x": 1130.5,
        "y": 872.5,
        "w": 111
      },
      "Marius Bugge Monsen (mmonsen), Cutehacks": {
        "x": 1130.5,
        "y": 787.5,
        "w": 99
      },
      "Oswald Buddenhagen (ossi), KDM, Qt": {
        "x": 1010.5,
        "y": 783.5,
        "w": 93
      },
      "Alberto Garcia, Igalia": {
        "x": 984.5,
        "y": 904.5,
        "w": 105
      },
      "Tadaja xxx": {
        "x": 914.5,
        "y": 794.5,
        "w": 117
      },
      "46 xxx": {
        "x": 874,
        "y": 904,
        "w": 126
      },
      "Carlos Garcia Campos (KaL), evince": {
        "x": 769.5,
        "y": 936.5,
        "w": 105
      },
      "48 Claudio Saavedra": {
        "x": 676,
        "y": 934,
        "w": 114
      },
      "Aracele Torres (araceletorres), KDE Brasil": {
        "x": 705.5,
        "y": 824.5,
        "w": 105
      },
      "Vitor Boschi (Klanticus), Plasma, KDE Brasil": {
        "x": 631.5,
        "y": 790.5,
        "w": 105
      },
      "Scott Reeves, openSUSE": {
        "x": 515.5,
        "y": 872.5,
        "w": 111
      },
      "Daker Pinheiro (dakerfp), Plasma": {
        "x": 530.5,
        "y": 777.5,
        "w": 99
      },
      "Lamarque Souza, Solid Network": {
        "x": 432.5,
        "y": 797.5,
        "w": 99
      },
      "54 xxx": {
        "x": 313.5,
        "y": 912.5,
        "w": 111
      },
      "Ufa (ufa)": {
        "x": 337,
        "y": 830,
        "w": 96
      },
      "Helio Castro (heliocastro), Collabora": {
        "x": 217.5,
        "y": 798.5,
        "w": 111
      },
      "Jon Nordby (jonnor)": {
        "x": 148,
        "y": 941,
        "w": 114
      },
      "Nuno Pinheiro, Oxygen": {
        "x": 31,
        "y": 938,
        "w": 126
      },
      "Filipe Saraiva (filipesaraiva), Cantor, KDE Brasil": {
        "x": 95.5,
        "y": 784.5,
        "w": 111
      },
      "Claudia Rauch (claudiar), KDE e.V.": {
        "x": -15.5,
        "y": 826.5,
        "w": 129
      },
      "Aakriti Gupta (aakriti), Calligra": {
        "x": 42.5,
        "y": 665.5,
        "w": 111
      },
      "62 xxx": {
        "x": 2707,
        "y": 798,
        "w": 78
      },
      "Jan Girlich (vollkorn)": {
        "x": 2529.5,
        "y": 771.5,
        "w": 99
      },
      "Lydia Pintscher (nightrose), KDE e.V.": {
        "x": 2407,
        "y": 787,
        "w": 84
      },
      "Mousumi Bhattacharya": {
        "x": 2282.5,
        "y": 799.5,
        "w": 63
      },
      "66 Raúl Gutiérrez Segalés, Collabora": {
        "x": 2500.5,
        "y": 719.5,
        "w": 81
      },
      "Madhu Vishy": {
        "x": 2570,
        "y": 722,
        "w": 72
      },
      "68 xxx": {
        "x": 2647.5,
        "y": 715.5,
        "w": 93
      },
      "Carl Symons (kallecarl)": {
        "x": 2766.5,
        "y": 720.5,
        "w": 93
      },
      "Stephen Kelly (steveire), KDE PIM": {
        "x": 2807,
        "y": 654,
        "w": 96
      },
      "Marcus Hanwell (cryos), Kitware, kdeedu": {
        "x": 2718.5,
        "y": 650.5,
        "w": 75
      },
      "Tobias Fischbach (LiMux)": {
        "x": 2675.5,
        "y": 612.5,
        "w": 87
      },
      "peppe": {
        "x": 2602.5,
        "y": 638.5,
        "w": 87
      },
      "Matthias Classen, Holder of Pants": {
        "x": 2484,
        "y": 626,
        "w": 78
      },
      "George Kiagiadakis (gkiagia), Debian/KDE/Collabora": {
        "x": 2417,
        "y": 658,
        "w": 72
      },
      "Pradeepto Bhattacharya (pradeepto), KDE India": {
        "x": 2317.5,
        "y": 725.5,
        "w": 87
      },
      "Shreya Pandit, Calligra": {
        "x": 2185.5,
        "y": 772.5,
        "w": 75
      },
      "Myriam Schweingruber (Mamarok), Amarok": {
        "x": 2213,
        "y": 672,
        "w": 90
      },
      "Olivier Goffart (ogoffart), Qt, woboq": {
        "x": 2052,
        "y": 723,
        "w": 90
      },
      "80 xxx": {
        "x": 1949,
        "y": 736,
        "w": 102
      },
      "Olav Vitters": {
        "x": 2025.5,
        "y": 658.5,
        "w": 81
      },
      "82 xxx": {
        "x": 2123,
        "y": 654,
        "w": 72
      },
      "Ville Jyrkkä": {
        "x": 2210.5,
        "y": 623.5,
        "w": 69
      },
      "84 xxx": {
        "x": 2325,
        "y": 620,
        "w": 90
      },
      "Alexander Larsson": {
        "x": 2448.5,
        "y": 568.5,
        "w": 81
      },
      "Thomas": {
        "x": 2570.5,
        "y": 601.5,
        "w": 81
      },
      "Florian Maier (LiMux)": {
        "x": 2646,
        "y": 580,
        "w": 72
      },
      "88 xxx": {
        "x": 2862.5,
        "y": 607.5,
        "w": 69
      },
      "89 xxx": {
        "x": 2766.5,
        "y": 581.5,
        "w": 75
      },
      "Jakub Steiner, GNOME artist": {
        "x": 2840.5,
        "y": 548.5,
        "w": 69
      },
      "Bastien Nocera, GNOME/Red hat": {
        "x": 2671,
        "y": 531,
        "w": 78
      },
      "Arthur Arlt (aarlt), KWin": {
        "x": 2811,
        "y": 497,
        "w": 60
      },
      "Dan Williams, NetworkManager": {
        "x": 2742,
        "y": 496,
        "w": 72
      },
      "Jana Pirat": {
        "x": 2790,
        "y": 458,
        "w": 66
      },
      "Kenny Coyle (automatical), Akademy": {
        "x": 2703,
        "y": 445,
        "w": 66
      },
      "96 Philip Withnall": {
        "x": 2593.5,
        "y": 499.5,
        "w": 75
      },
      "97 xxx": {
        "x": 2546.5,
        "y": 540.5,
        "w": 81
      },
      "Andreas Holzammer (aholza), KDE Windows": {
        "x": 2439.5,
        "y": 527.5,
        "w": 69
      },
      "99 xxx": {
        "x": 2379.5,
        "y": 543.5,
        "w": 81
      },
      "Mickaël Sibelle, French i18n": {
        "x": 2340.5,
        "y": 575.5,
        "w": 63
      },
      "Björn Balazs, Usability": {
        "x": 2246.5,
        "y": 578.5,
        "w": 81
      },
      "Inge Wallin, Calligra": {
        "x": 2315.5,
        "y": 524.5,
        "w": 75
      },
      "Daniel E. Moctezuma": {
        "x": 2386.5,
        "y": 497.5,
        "w": 69
      },
      "Sjors Gielen": {
        "x": 2501,
        "y": 491,
        "w": 60
      },
      "Julie Pichon": {
        "x": 2455,
        "y": 482,
        "w": 66
      },
      "Pascal Terjan": {
        "x": 2545.5,
        "y": 457.5,
        "w": 69
      },
      "107 xxx": {
        "x": 2546.5,
        "y": 393.5,
        "w": 75
      },
      "Guillaume Hormiere, Toulouse Mafia": {
        "x": 2481,
        "y": 398,
        "w": 90
      },
      "Jean-Nicolas Artaud, French, KDE - Calligra": {
        "x": 2417.5,
        "y": 441.5,
        "w": 69
      },
      "110 xxx": {
        "x": 2444,
        "y": 388,
        "w": 66
      },
      "111 xxx": {
        "x": 2409,
        "y": 407,
        "w": 54
      },
      "Anton Lazarev, Intel": {
        "x": 2372.5,
        "y": 432.5,
        "w": 75
      },
      "Stuart Dickson (stuartmd), Calligra, KO": {
        "x": 2366,
        "y": 394,
        "w": 60
      },
      "Álvaro M. Recio, University of Málaga, Spain": {
        "x": 2303.5,
        "y": 468.5,
        "w": 75
      },
      "Hanna Skott (hannaskott), Calligra": {
        "x": 2295,
        "y": 431,
        "w": 66
      },
      "Stephanie Lerotic": {
        "x": 2273,
        "y": 497,
        "w": 72
      },
      "117 xxx": {
        "x": 2222.5,
        "y": 513.5,
        "w": 81
      },
      "Teo Mrnjavac, Amarok": {
        "x": 2233.5,
        "y": 469.5,
        "w": 57
      },
      "Kimmo Hämäläinen": {
        "x": 2199,
        "y": 498,
        "w": 60
      },
      "Stefan Gehn, froglogic": {
        "x": 2183,
        "y": 462,
        "w": 60
      },
      "David Faure (dfaure), KDE demigod": {
        "x": 2149.5,
        "y": 516.5,
        "w": 69
      },
      "Rainer Endres (physos), KDE": {
        "x": 2141.5,
        "y": 567.5,
        "w": 69
      },
      "123 xxx": {
        "x": 2090.5,
        "y": 604.5,
        "w": 81
      },
      "124 xxx": {
        "x": 2047,
        "y": 594,
        "w": 72
      },
      "Tero Heino": {
        "x": 1964.5,
        "y": 604.5,
        "w": 69
      },
      "126 xxx": {
        "x": 1959,
        "y": 559,
        "w": 66
      },
      "Matthias Klumpp (ximion)": {
        "x": 2036.5,
        "y": 548.5,
        "w": 69
      },
      "Sébastien Renard (renards), KDE French translations": {
        "x": 2079,
        "y": 528,
        "w": 54
      },
      "Alex Spehr (blauzahl), Froglogic": {
        "x": 2099.5,
        "y": 487.5,
        "w": 69
      },
      "Gaelle Pion": {
        "x": 2116,
        "y": 448,
        "w": 66
      },
      "Luis Menina, GNOME": {
        "x": 2160.5,
        "y": 441.5,
        "w": 57
      },
      "Martin Gräßlin (mgraesslin), KWin": {
        "x": 2240.5,
        "y": 425.5,
        "w": 57
      },
      "Shantanu Tushar (Shaan7), Gluon": {
        "x": 2287,
        "y": 376,
        "w": 66
      },
      "Georg Grabler, The Chakra Project": {
        "x": 2233,
        "y": 364,
        "w": 54
      },
      "Arjen Hiemstra (ahiemstra), Gluon": {
        "x": 2178.5,
        "y": 375.5,
        "w": 81
      },
      "Florian Wilhelm, KDE e.V. intern": {
        "x": 2142.5,
        "y": 374.5,
        "w": 63
      },
      "Niels Slot, KTurtle": {
        "x": 2119.5,
        "y": 408.5,
        "w": 51
      },
      "Stef Walter, Collabora": {
        "x": 2083.5,
        "y": 427.5,
        "w": 57
      },
      "Marijn Kruisselbrink (Mek), Calligra": {
        "x": 2054,
        "y": 449,
        "w": 60
      },
      "140 xxx": {
        "x": 2023.5,
        "y": 483.5,
        "w": 75
      },
      "141 xxx": {
        "x": 1952,
        "y": 487,
        "w": 66
      },
      "Alexandre Franke, GNOME": {
        "x": 1980,
        "y": 439,
        "w": 66
      },
      "Dan Leinir Turthra Jensen (leinir), Gluon, Amarok": {
        "x": 2022.5,
        "y": 409.5,
        "w": 63
      },
      "Casper van Donderen (cvandonderen), KDE Windows": {
        "x": 2020,
        "y": 352,
        "w": 66
      },
      "Michael Fötsch": {
        "x": 1938,
        "y": 387,
        "w": 84
      },
      "Peter Penz": {
        "x": 1505,
        "y": 751,
        "w": 78
      },
      "147 xxx": {
        "x": 1571,
        "y": 743,
        "w": 72
      },
      "Björn Taubert, Intel": {
        "x": 1661.5,
        "y": 733.5,
        "w": 75
      },
      "Sjoerd Simons, Collabora": {
        "x": 1929.5,
        "y": 638.5,
        "w": 87
      },
      "Sulamita Garcia, Intel, Linuxchix, Ada Initiative": {
        "x": 1853,
        "y": 699,
        "w": 96
      },
      "Jos Poortvliet (jospoortvleit), flufflebunny": {
        "x": 1771,
        "y": 673,
        "w": 78
      },
      "Veronika": {
        "x": 1665,
        "y": 668,
        "w": 84
      },
      "153 xxx": {
        "x": 1536,
        "y": 692,
        "w": 78
      },
      "Benjamin Port (ben2367)": {
        "x": 1440.5,
        "y": 710.5,
        "w": 81
      },
      "Edward Hervey, Collabora": {
        "x": 1388.5,
        "y": 732.5,
        "w": 93
      },
      "Jiri Eischmann, Red Hat": {
        "x": 1297,
        "y": 747,
        "w": 90
      },
      "David Leisse, Alex' son": {
        "x": 1199,
        "y": 763,
        "w": 72
      },
      "Alexandra Leisse (troubalex), Qt, Nokia": {
        "x": 1179.5,
        "y": 703.5,
        "w": 81
      },
      "159 xxx": {
        "x": 1075,
        "y": 717,
        "w": 84
      },
      "Cornelius Schumacher, openSUSE, KDE e.V.": {
        "x": 1090,
        "y": 644,
        "w": 90
      },
      "Stefan Derkits, Amarok": {
        "x": 1393.5,
        "y": 660.5,
        "w": 87
      },
      "Raphael Kubo da Costa (rakuco), KDE Freebsd, KDEUtils": {
        "x": 1525,
        "y": 642,
        "w": 72
      },
      "Erlend Hamberg (ehamberg), Kate": {
        "x": 1609.5,
        "y": 607.5,
        "w": 81
      },
      "Johannes H. Jensen, xxx/GNOME": {
        "x": 1715.5,
        "y": 636.5,
        "w": 81
      },
      "Olivier Crête (Tester), Collabora": {
        "x": 1840,
        "y": 635,
        "w": 72
      },
      "Stefan Werden, basyskom": {
        "x": 1863,
        "y": 583,
        "w": 72
      },
      "Luis de Bethencourt, Collabora": {
        "x": 1929.5,
        "y": 537.5,
        "w": 63
      },
      "Robert Bragg, Intel": {
        "x": 1893.5,
        "y": 550.5,
        "w": 57
      },
      "169 xxx": {
        "x": 1802.5,
        "y": 548.5,
        "w": 69
      },
      "170 xxx": {
        "x": 1760.5,
        "y": 572.5,
        "w": 75
      },
      "Anita Reitere": {
        "x": 1718.5,
        "y": 578.5,
        "w": 75
      },
      "Carlos Martín Nieto": {
        "x": 1660,
        "y": 581,
        "w": 78
      },
      "Rosanna Yuen, GNOME Foundation": {
        "x": 1610,
        "y": 545,
        "w": 72
      },
      "174 xxx": {
        "x": 1557,
        "y": 568,
        "w": 84
      },
      "Thorsten Staerk, KTimeTracker": {
        "x": 1505.5,
        "y": 582.5,
        "w": 81
      },
      "Jonathon Jongsma, Collabora": {
        "x": 1435,
        "y": 593,
        "w": 72
      },
      "Marina Zhurakhinskaya": {
        "x": 1377.5,
        "y": 588.5,
        "w": 81
      },
      "Knut Yrvin, Qt/Nokia/Norwegian Talents Break Dancing Champion": {
        "x": 1338.5,
        "y": 637.5,
        "w": 75
      },
      "Behdad Esfahbod": {
        "x": 1269.5,
        "y": 607.5,
        "w": 81
      },
      "Klaas Freitag, openSUSE": {
        "x": 1216.5,
        "y": 626.5,
        "w": 93
      },
      "Frédéric Crozat (fcrozat), openSUSE": {
        "x": 1142.5,
        "y": 612.5,
        "w": 93
      },
      "Didier Roche (didrocks), Canonical": {
        "x": 1087,
        "y": 574,
        "w": 78
      },
      "Jonathan Blandford, Red Hat": {
        "x": 1481,
        "y": 562,
        "w": 60
      },
      "184 xxx": {
        "x": 1434.5,
        "y": 526.5,
        "w": 63
      },
      "Cédric Bail, Enlightenment": {
        "x": 1377.5,
        "y": 525.5,
        "w": 81
      },
      "Monika Lischke, Intel": {
        "x": 1285,
        "y": 545,
        "w": 84
      },
      "Sherry Lochhaas": {
        "x": 1257.5,
        "y": 567.5,
        "w": 63
      },
      "Ian Monroe (eean), Amarok": {
        "x": 1223.5,
        "y": 528.5,
        "w": 63
      },
      "Guillaume Desmottes (cassidy), Collabora": {
        "x": 1179,
        "y": 548,
        "w": 72
      },
      "190 xxx": {
        "x": 1124,
        "y": 502,
        "w": 66
      },
      "191 xxx": {
        "x": 1139.5,
        "y": 451.5,
        "w": 75
      },
      "Shaun McCance, GNOME": {
        "x": 1174.5,
        "y": 495.5,
        "w": 63
      },
      "193 xxx": {
        "x": 1230,
        "y": 478,
        "w": 66
      },
      "Valorie Zimmerman (valorie), Kubuntu/CWG": {
        "x": 1264,
        "y": 490,
        "w": 78
      },
      "Vincent Untz, openSUSE": {
        "x": 1334.5,
        "y": 496.5,
        "w": 69
      },
      "Federico Mena Quintero": {
        "x": 1410.5,
        "y": 477.5,
        "w": 57
      },
      "Cosimo Cecchi": {
        "x": 1478,
        "y": 498,
        "w": 78
      },
      "Ismail Donmez (cartman), openSUSE": {
        "x": 1580,
        "y": 506,
        "w": 66
      },
      "Will Stephenson, (wstephenson, Bille), openSUSE)": {
        "x": 1695.5,
        "y": 507.5,
        "w": 57
      },
      "200 xxx": {
        "x": 1801.5,
        "y": 500.5,
        "w": 63
      },
      "201 xxx": {
        "x": 1912.5,
        "y": 463.5,
        "w": 69
      },
      "Arthur Schiwon (Blizzz) ownCloud": {
        "x": 1873,
        "y": 490,
        "w": 72
      },
      "Agustín Benito Bethencourt (toscalix)": {
        "x": 1785.5,
        "y": 458.5,
        "w": 63
      },
      "Joseph Wenninger, Kate": {
        "x": 1735.5,
        "y": 485.5,
        "w": 69
      },
      "Dominik Haumann, Kate": {
        "x": 1701.5,
        "y": 455.5,
        "w": 57
      },
      "Ivan Čukić": {
        "x": 1659,
        "y": 465,
        "w": 66
      },
      "Thomas Thym (ungethym), KDE Promo": {
        "x": 1598,
        "y": 464,
        "w": 54
      },
      "Nick Shaforostoff, Lokalize": {
        "x": 1524.5,
        "y": 464.5,
        "w": 57
      },
      "Robert Ancell, LightDM, Ubuntu Desktop/Canonical": {
        "x": 1439,
        "y": 446,
        "w": 66
      },
      "Ryan Lortie (desrt), dconf, codethink": {
        "x": 1358,
        "y": 435,
        "w": 66
      },
      "211 xxx": {
        "x": 1258.5,
        "y": 422.5,
        "w": 69
      },
      "Youness Alaoui, Collabora": {
        "x": 1176,
        "y": 418,
        "w": 78
      },
      "213 xxx": {
        "x": 1083,
        "y": 439,
        "w": 60
      },
      "Albert Astals Cid (tsdgeos), Okular, KDE l10n": {
        "x": 1127,
        "y": 396,
        "w": 72
      },
      "Chusslove Illich, KDE l10n": {
        "x": 1229.5,
        "y": 383.5,
        "w": 63
      },
      "Thomas Fischer": {
        "x": 1330.5,
        "y": 373.5,
        "w": 75
      },
      "217 xxx": {
        "x": 1404.5,
        "y": 396.5,
        "w": 81
      },
      "218 xxx": {
        "x": 1484,
        "y": 420,
        "w": 66
      },
      "219 xxx": {
        "x": 1529.5,
        "y": 420.5,
        "w": 51
      },
      "220 xxx": {
        "x": 1560,
        "y": 429,
        "w": 66
      },
      "221 xxx": {
        "x": 1627.5,
        "y": 406.5,
        "w": 69
      },
      "Felix Lemke, KDE games": {
        "x": 1719,
        "y": 400,
        "w": 72
      },
      "Jeffrey Kelling, KDE games": {
        "x": 1808.5,
        "y": 418.5,
        "w": 81
      },
      "224 xxx": {
        "x": 1895.5,
        "y": 413.5,
        "w": 75
      },
      "Patrick Spendrin (SaroEngels), KDE Windows": {
        "x": 1915.5,
        "y": 333.5,
        "w": 75
      },
      "Alexander van Loon, Commit Digest": {
        "x": 1862.5,
        "y": 380.5,
        "w": 75
      },
      "Bertrand Lorentz, Banshee": {
        "x": 1833.5,
        "y": 378.5,
        "w": 57
      },
      "Stefan Majewsky, KDE games": {
        "x": 1773.5,
        "y": 392.5,
        "w": 75
      },
      "229 xxx": {
        "x": 1756.5,
        "y": 372.5,
        "w": 51
      },
      "Arno Rehn": {
        "x": 1662,
        "y": 372,
        "w": 72
      },
      "231 xxx": {
        "x": 1584.5,
        "y": 380.5,
        "w": 87
      },
      "Frank Osterfeld, KDEPIM, KDAB": {
        "x": 1551,
        "y": 335,
        "w": 84
      },
      "Jan Hambrecht (jaham), Calligra": {
        "x": 1460,
        "y": 334,
        "w": 84
      },
      "Thorsten Zachmann (zagge), Calligra": {
        "x": 1397.5,
        "y": 339.5,
        "w": 69
      },
      "Pierre Ducroquet (pinaraf), Calligra": {
        "x": 1457,
        "y": 279,
        "w": 78
      },
      "Andreas Jäger, openSUSE": {
        "x": 1307.5,
        "y": 319.5,
        "w": 81
      },
      "Pascal Mages": {
        "x": 1249.5,
        "y": 330.5,
        "w": 69
      },
      "Luca Gugelmann": {
        "x": 1173,
        "y": 335,
        "w": 72
      },
      "Łukasz Ślachciak, WebKit EFL/GTK": {
        "x": 1098,
        "y": 340,
        "w": 72
      },
      "Tobias Hunger": {
        "x": 811.5,
        "y": 758.5,
        "w": 93
      },
      "Kevin Krammer, KDE PIM": {
        "x": 1003,
        "y": 712,
        "w": 90
      },
      "Sascha Peilicke, openSUSE": {
        "x": 926.5,
        "y": 717.5,
        "w": 81
      },
      "Seif Lotfy, Zeitgeist": {
        "x": 743.5,
        "y": 723.5,
        "w": 93
      },
      "Garrett LeSage (garrett), openSUSE": {
        "x": 596,
        "y": 712,
        "w": 96
      },
      "Adrián Pérez de Castro, Igalia": {
        "x": 324.5,
        "y": 735.5,
        "w": 99
      },
      "246 xxx": {
        "x": 225.5,
        "y": 715.5,
        "w": 87
      },
      "Javier Jardón, GNOME, Codethink": {
        "x": 460.5,
        "y": 690.5,
        "w": 87
      },
      "Ivan Frade, Tracker": {
        "x": 381.5,
        "y": 678.5,
        "w": 87
      },
      "249 xxx": {
        "x": 520.5,
        "y": 678.5,
        "w": 75
      },
      "David Jarvie, KAlarm": {
        "x": 961.5,
        "y": 652.5,
        "w": 93
      },
      "Paul van Tilburg": {
        "x": 840,
        "y": 644,
        "w": 84
      },
      "252 xxx, Debian": {
        "x": 664,
        "y": 653,
        "w": 84
      },
      "253 xxx": {
        "x": 562.5,
        "y": 658.5,
        "w": 87
      },
      "Maddalena Ottonello": {
        "x": 457,
        "y": 625,
        "w": 72
      },
      "Daniele E. Domenichelli (drdanz) KDE Telepathy": {
        "x": 370.5,
        "y": 614.5,
        "w": 87
      },
      "256 xxx": {
        "x": 287,
        "y": 664,
        "w": 78
      },
      "257 xxx": {
        "x": 196,
        "y": 652,
        "w": 84
      },
      "Aditya Bhatt (adityab), Digikam/KDE": {
        "x": 114.5,
        "y": 614.5,
        "w": 87
      },
      "259 Tobias Mueller": {
        "x": 251.5,
        "y": 547.5,
        "w": 63
      },
      "260 xxx": {
        "x": 257,
        "y": 595,
        "w": 72
      },
      "Pablo Vieytes, Openshine": {
        "x": 338,
        "y": 594,
        "w": 66
      },
      "271 xxx": {
        "x": 493,
        "y": 574,
        "w": 72
      },
      "272 xxx": {
        "x": 664,
        "y": 592,
        "w": 84
      },
      "273 xxx": {
        "x": 752,
        "y": 595,
        "w": 84
      },
      "Alberto Salmerón, University of Málaga, Spain": {
        "x": 874.5,
        "y": 598.5,
        "w": 69
      },
      "275 Bram Senders": {
        "x": 1025.5,
        "y": 587.5,
        "w": 93
      },
      "276 xxx": {
        "x": 971,
        "y": 586,
        "w": 78
      },
      "Tobias Koenig (tokoe), KAddressbook, Akonadi, KDAB": {
        "x": 1068,
        "y": 551,
        "w": 60
      },
      "Àlex Fiestas (afiestas), bluedevil, solid": {
        "x": 1017,
        "y": 505,
        "w": 90
      },
      "Christoph Cullmann, Kate": {
        "x": 982.5,
        "y": 530.5,
        "w": 75
      },
      "Frank Karlitschek, KDE eV board": {
        "x": 877.5,
        "y": 537.5,
        "w": 87
      },
      "281 xxx": {
        "x": 940,
        "y": 514,
        "w": 72
      },
      "Siegfried-Angel Gevatter Pujals (RainCT), Zeitgeist": {
        "x": 901.5,
        "y": 497.5,
        "w": 63
      },
      "Nikhil Marathe, Amarok": {
        "x": 831,
        "y": 526,
        "w": 72
      },
      "Dirk Müller, KDE releases": {
        "x": 781.5,
        "y": 536.5,
        "w": 81
      },
      "285 xxx": {
        "x": 734,
        "y": 528,
        "w": 78
      },
      "Mateu Batle, (mbatle) Collabora": {
        "x": 697.5,
        "y": 546.5,
        "w": 69
      },
      "Reynaldo H. Verdejo Pinochet (reynaldo), Collabora": {
        "x": 617,
        "y": 547,
        "w": 72
      },
      "Dario Freddi, (drf) Solid, kdelibs, kde-telepathy, Collabora": {
        "x": 573,
        "y": 577,
        "w": 72
      },
      "289 xxx": {
        "x": 531,
        "y": 538,
        "w": 78
      },
      "Davide Bettio (windowsuninstall)": {
        "x": 455.5,
        "y": 516.5,
        "w": 87
      },
      "291 xxx": {
        "x": 404.5,
        "y": 550.5,
        "w": 75
      },
      "Alberto Ruiz": {
        "x": -43.131,
        "y": 524.5,
        "w": 87
      },
      "Aaron Seigo (aseigo), Plasma": {
        "x": 288,
        "y": 522,
        "w": 72
      },
      "Jeff Mitchell (jefferai), Sysadmin/CWG": {
        "x": 175.5,
        "y": 518.5,
        "w": 87
      },
      "Till Adam (till), KDE PIM": {
        "x": 250,
        "y": 473,
        "w": 72
      },
      "Mirko Böhm, Desktop Summit Chief": {
        "x": 343.5,
        "y": 459.5,
        "w": 81
      },
      "David King (amigadave), Vinagre/Vino": {
        "x": 390.5,
        "y": 482.5,
        "w": 75
      },
      "George Goldberg, KDE Telepathy/Collabora": {
        "x": 445,
        "y": 475,
        "w": 66
      },
      "Mark Kretschmann (markey), Amarok": {
        "x": 510,
        "y": 477,
        "w": 66
      },
      "Kevin Ottens (ervin)": {
        "x": 607,
        "y": 499,
        "w": 60
      },
      "301 xxx": {
        "x": 657.5,
        "y": 497.5,
        "w": 75
      },
      "302 xxx": {
        "x": 704,
        "y": 482,
        "w": 54
      },
      "Jan-Christoph Borchardt (jancborchardt) ownCloud &amp; usability": {
        "x": 756.5,
        "y": 472.5,
        "w": 69
      },
      "Miquel Sabaté Solà (mssola), KDevelop Ruby": {
        "x": 793.5,
        "y": 465.5,
        "w": 63
      },
      "Jakob Sack (jakobsack) ownCloud": {
        "x": 842.5,
        "y": 474.5,
        "w": 63
      },
      "Víctor Blázquez (vblazquez)": {
        "x": 895.5,
        "y": 448.5,
        "w": 69
      },
      "Marc Mauri": {
        "x": 974,
        "y": 483,
        "w": 60
      },
      "Philip Muškovac (yofel), Kubuntu": {
        "x": 1037,
        "y": 453,
        "w": 78
      },
      "Jose Millan Soto (fid_jose) Accessibility": {
        "x": 1032.5,
        "y": 392.5,
        "w": 75
      },
      "Pedro Jurado Maqueda": {
        "x": 998.5,
        "y": 427.5,
        "w": 69
      },
      "Aleix Pol Gonzalez (apol), kdevelop, kalgebra": {
        "x": 925,
        "y": 422,
        "w": 66
      },
      "312 xxx": {
        "x": 888.5,
        "y": 395.5,
        "w": 75
      },
      "Peter Grasch, Simon": {
        "x": 829.5,
        "y": 414.5,
        "w": 63
      },
      "Álvaro Villalba Navarro": {
        "x": 719,
        "y": 426,
        "w": 72
      },
      "315 xxx": {
        "x": 675,
        "y": 423,
        "w": 66
      },
      "Simon Edwards (Sime), PyKDE": {
        "x": 621.5,
        "y": 432.5,
        "w": 75
      },
      "317 xxx": {
        "x": 554,
        "y": 420,
        "w": 72
      },
      "David Edmundson": {
        "x": 517.5,
        "y": 443.5,
        "w": 57
      },
      "Lasse Liehu": {
        "x": 469.5,
        "y": 438.5,
        "w": 69
      },
      "320 xxx": {
        "x": 388.5,
        "y": 423.5,
        "w": 69
      },
      "Jarosław Staniek, Kexi": {
        "x": 1001,
        "y": 349,
        "w": 66
      },
      "322 xxx": {
        "x": 925,
        "y": 368,
        "w": 66
      },
      "323 xxx": {
        "x": 844,
        "y": 358,
        "w": 66
      },
      "Felix Rohrbach (fxrh), Gluon": {
        "x": 784,
        "y": 395,
        "w": 66
      },
      "325 xxx": {
        "x": 747.5,
        "y": 377.5,
        "w": 57
      },
      "Marc-André Lureau (elmarco), Spice": {
        "x": 667,
        "y": 376,
        "w": 60
      },
      "Mario Bensi ([Nef])": {
        "x": 607.5,
        "y": 386.5,
        "w": 69
      },
      "328 xxx": {
        "x": 584,
        "y": 349,
        "w": 66
      },
      "329 xxx": {
        "x": 526,
        "y": 403,
        "w": 54
      },
      "330 xxx": {
        "x": 511.5,
        "y": 382.5,
        "w": 45
      },
      "331 xxx": {
        "x": 439,
        "y": 390,
        "w": 72
      },
      "Ilkka Lehtinen, COSS, Akademy 2010": {
        "x": 373,
        "y": 357,
        "w": 78
      },
      "Len Miles, Collabora": {
        "x": 314,
        "y": 412,
        "w": 72
      },
      "Sirko Kemter (gnokii) Fedora project": {
        "x": 271,
        "y": 376,
        "w": 84
      },
      "Kenny Duffus (seaLne), Akademy": {
        "x": 2730,
        "y": 356,
        "w": 66
      },
      "336 xxx": {
        "x": 2683.5,
        "y": 274.5,
        "w": 69
      },
      "337 xxx, Debian": {
        "x": 2577,
        "y": 257,
        "w": 78
      },
      "Markus Goetz, Qt, woboq": {
        "x": 2548.5,
        "y": 188.5,
        "w": 69
      },
      "339 xxx": {
        "x": 2469,
        "y": 208,
        "w": 66
      },
      "Xan López, Igalia": {
        "x": 2387,
        "y": 215,
        "w": 72
      },
      "Lukas Appelhans, (lappelhans) KGet, Chakra": {
        "x": 2327,
        "y": 197,
        "w": 66
      },
      "Matthias Fuchs, (mat69) KGet": {
        "x": 2224.5,
        "y": 207.5,
        "w": 63
      },
      "Joone Hur, Collabora": {
        "x": 2116,
        "y": 196,
        "w": 84
      },
      "Karen Sandler, GNOME Foundation": {
        "x": 2038,
        "y": 208,
        "w": 72
      },
      "Camila Ayres (camillasan), Umbrello, KDE Brasil": {
        "x": 1954.5,
        "y": 218.5,
        "w": 69
      },
      "Eva Brucherseifer (eva), Basyskom": {
        "x": 1876,
        "y": 178,
        "w": 66
      },
      "Shinjo Park, KDE Korean translations": {
        "x": 1838.5,
        "y": 161.5,
        "w": 57
      },
      "Matthias Welwarsky": {
        "x": 1807,
        "y": 186,
        "w": 54
      },
      "Peter Goetz": {
        "x": 1753.5,
        "y": 193.5,
        "w": 69
      },
      "Laszlo Papp (djszapi), Gluon, KDE mobile": {
        "x": 1736.5,
        "y": 163.5,
        "w": 57
      },
      "Sivan Green (sivang), Ubuntu": {
        "x": 1706.5,
        "y": 193.5,
        "w": 57
      },
      "Ruben Vermeersch, F-Spot": {
        "x": 1665.5,
        "y": 195.5,
        "w": 63
      },
      "Gustavo Barbieri, ProFusion": {
        "x": 1569,
        "y": 170,
        "w": 78
      },
      "Artur Duque de Souza, INdT": {
        "x": 1528.5,
        "y": 180.5,
        "w": 57
      },
      "355 xxx": {
        "x": 1373.5,
        "y": 178.5,
        "w": 63
      },
      "Andrea Diamantini, rekonq": {
        "x": 1258.5,
        "y": 192.5,
        "w": 63
      },
      "Vinicius Depizzol, GNOME": {
        "x": 1156,
        "y": 161,
        "w": 66
      },
      "Stormy Peters": {
        "x": 1075,
        "y": 174,
        "w": 60
      },
      "Stéphane Maniaci, GNOME": {
        "x": 1056.5,
        "y": 212.5,
        "w": 63
      },
      "Chani Armitage (Chani), Plasma, KO": {
        "x": 935.5,
        "y": 227.5,
        "w": 63
      },
      "Andreas Nilsson, Icons": {
        "x": 832,
        "y": 212,
        "w": 72
      },
      "Julita Inca": {
        "x": 768,
        "y": 235,
        "w": 66
      },
      "363 xxx": {
        "x": 745,
        "y": 160,
        "w": 66
      },
      "Tamara Atanasoska": {
        "x": 697,
        "y": 208,
        "w": 66
      },
      "365 xxx": {
        "x": 646,
        "y": 165,
        "w": 72
      },
      "Marta Rybczyńska, KDE Commit-Digest, Polish KDE Translations": {
        "x": 616,
        "y": 206,
        "w": 66
      },
      "367 xxx": {
        "x": 573,
        "y": 166,
        "w": 66
      },
      "Gaurav Chaturvedi (tazz), Kubuntu": {
        "x": 512.5,
        "y": 209.5,
        "w": 69
      },
      "369 xxx": {
        "x": 436.5,
        "y": 204.5,
        "w": 63
      },
      "Matti Saastamoinen (smoinen), COSS, Akademy 2010": {
        "x": 480.5,
        "y": 172.5,
        "w": 63
      },
      "Jonathan Riddell (Riddell), Kubuntu/Canonical": {
        "x": 452.5,
        "y": 168.5,
        "w": 51
      },
      "Daniel Glassy, SIL, text layout": {
        "x": 422.5,
        "y": 158.5,
        "w": 51
      },
      "Brian Cameron, GNOME, Oracle": {
        "x": 385,
        "y": 178,
        "w": 72
      },
      "Heinz Wiesinger (pprkut), Slackware": {
        "x": 349.5,
        "y": 174.5,
        "w": 63
      },
      "Emily Chen, Oracle, GNOME": {
        "x": 318.5,
        "y": 208.5,
        "w": 63
      },
      "Anne Wilson, KDE Community Working Group": {
        "x": 2267,
        "y": 197,
        "w": 60
      },
      "377 xxx": {
        "x": 2508.5,
        "y": 172.5,
        "w": 57
      },
      "Fernando Herrera, GNOME/Igalia": {
        "x": 2396,
        "y": 171,
        "w": 60
      },
      "379 xxx": {
        "x": 2303,
        "y": 173,
        "w": 54
      },
      "Vishesh Handa (vhanda), Nepomuk/KDE": {
        "x": 2249.5,
        "y": 149.5,
        "w": 63
      },
      "Carl-Philipp Wackernagel, TSB": {
        "x": 2190,
        "y": 155,
        "w": 60
      },
      "Gunnar Schmidt": {
        "x": 2153,
        "y": 146,
        "w": 60
      },
      "Daniel Molkentin (danimo), Qt": {
        "x": 2087.5,
        "y": 145.5,
        "w": 63
      },
      "Laura Dragan, Nepomuk": {
        "x": 2006,
        "y": 162,
        "w": 66
      },
      "Danny Bennet, basyskom": {
        "x": 1948,
        "y": 157,
        "w": 60
      },
      "Stephan Binner": {
        "x": 1910,
        "y": 139,
        "w": 54
      },
      "Matthias Kretz (Vir), Phonon": {
        "x": 1831,
        "y": 115,
        "w": 54
      },
      "Louis-Francis Ratté-Boulianne, Collabora": {
        "x": 1750.5,
        "y": 117.5,
        "w": 63
      },
      "Robert Riemann": {
        "x": 1703,
        "y": 119,
        "w": 60
      },
      "Henri Bergius (bergie), Midguard": {
        "x": 1661,
        "y": 97,
        "w": 66
      },
      "Alexander Neundorf, CMake, KDE Buildsystem": {
        "x": 1644.5,
        "y": 155.5,
        "w": 57
      },
      "Siddharth Sharma, xxx": {
        "x": 1610.5,
        "y": 145.5,
        "w": 57
      },
      "Daniel G. Siegel": {
        "x": 1579.5,
        "y": 113.5,
        "w": 51
      },
      "Alex Merry (randomguy), random things": {
        "x": 1535.5,
        "y": 127.5,
        "w": 51
      },
      "395 xxx": {
        "x": 1452.5,
        "y": 129.5,
        "w": 63
      },
      "Thiago Macieira (thiago), Qt, Intel OTC": {
        "x": 1411.5,
        "y": 120.5,
        "w": 69
      },
      "Jonas (beer), KDE user xxx": {
        "x": 1357,
        "y": 120,
        "w": 66
      },
      "Alexander E. Patrakov": {
        "x": 1291.5,
        "y": 132.5,
        "w": 63
      },
      "401 xxx": {
        "x": 1223.5,
        "y": 133.5,
        "w": 63
      },
      "Aurélien Gâteau (agateau), Gwenview, KDE, Canonical": {
        "x": 1200,
        "y": 98,
        "w": 60
      },
      "Daniel Marth, Marble": {
        "x": 1177.5,
        "y": 108.5,
        "w": 57
      },
      "Lucas Rocha, The Board, GNOME, Mozilla": {
        "x": 1123.5,
        "y": 128.5,
        "w": 63
      },
      "Alexander Dymo (adymo), KDevelop": {
        "x": 1073.5,
        "y": 105.5,
        "w": 69
      },
      " Bernhard Beschow (shentey), Marble, Kate": {
        "x": 1033.5,
        "y": 102.5,
        "w": 57
      },
      "Natalia Evdokimova, FSFE": {
        "x": 1002,
        "y": 149,
        "w": 60
      },
      "Luciano Montanaro (mikelima), KDE Italian translations": {
        "x": 961.5,
        "y": 111.5,
        "w": 63
      },
      "Inu Kim": {
        "x": 935,
        "y": 165,
        "w": 48
      },
      "Daniel Laidig": {
        "x": 882.5,
        "y": 117.5,
        "w": 57
      },
      "Jos van den Oever (josvandenoever), Strigi, KO": {
        "x": 838,
        "y": 130,
        "w": 60
      },
      "Jonathan Kolberg (bulldog98), Kubuntu": {
        "x": 802,
        "y": 111,
        "w": 60
      },
      "413 xxx": {
        "x": 772,
        "y": 132,
        "w": 54
      },
      "414 xxx": {
        "x": 739,
        "y": 123,
        "w": 48
      },
      "415 xxx": {
        "x": 683,
        "y": 143,
        "w": 72
      },
      "416 xxx": {
        "x": 618.5,
        "y": 136.5,
        "w": 63
      },
      "Friedrich W. H. Kossebau, Okteta": {
        "x": 541.5,
        "y": 126.5,
        "w": 69
      },
      "Robin Appelman (icewind) ownCloud": {
        "x": 515,
        "y": 124,
        "w": 54
      },
      "419 xxx": {
        "x": 2051,
        "y": 126,
        "w": 60
      },
      "Rohan Garg (shadeslayer), KDE/Kubuntu": {
        "x": 2259.5,
        "y": 123.5,
        "w": 39
      },
      "421 xxx": {
        "x": 2488,
        "y": 115,
        "w": 66
      },
      "Olivier Lê Thanh Duong (staz)": {
        "x": 2436.5,
        "y": 151.5,
        "w": 51
      },
      "423 xxx": {
        "x": 2407.5,
        "y": 138.5,
        "w": 45
      },
      "424 xxx": {
        "x": 2397.5,
        "y": 107.5,
        "w": 51
      },
      "425 xxx": {
        "x": 2416,
        "y": 80,
        "w": 48
      },
      "426 xxx": {
        "x": 2375,
        "y": 83,
        "w": 48
      },
      "Juan José Sánchez, Igalia": {
        "x": 2354,
        "y": 121,
        "w": 54
      },
      "428 xxx": {
        "x": 2319,
        "y": 107,
        "w": 54
      },
      "Theo Chatzimichos (tampakrap), Gentoo": {
        "x": 2301,
        "y": 88,
        "w": 54
      },
      "Efstathios Iosifidis (diamond_gr)": {
        "x": 2270,
        "y": 91,
        "w": 54
      },
      "Stella Rouzi (differentreality)": {
        "x": 2250.5,
        "y": 85.5,
        "w": 45
      },
      "Sergio Martins": {
        "x": 2217.5,
        "y": 91.5,
        "w": 51
      },
      "John Layt": {
        "x": 2199.5,
        "y": 120.5,
        "w": 51
      },
      "434 xxx": {
        "x": 2165,
        "y": 95,
        "w": 60
      },
      "Manuel Nickschas (Sput), Quassel": {
        "x": 2108.5,
        "y": 113.5,
        "w": 63
      },
      "436 xxx": {
        "x": 2101,
        "y": 76,
        "w": 54
      },
      "Rob Taylor, CodeThink": {
        "x": 2073,
        "y": 82,
        "w": 48
      },
      "Volker Krause, Akonadi, KDEPIM, KDAB": {
        "x": 2035,
        "y": 99,
        "w": 48
      },
      "439 xxx": {
        "x": 2019,
        "y": 109,
        "w": 36
      },
      "440 xxx": {
        "x": 1982,
        "y": 123,
        "w": 54
      },
      "441 xxx": {
        "x": 1955,
        "y": 112,
        "w": 48
      },
      "Allan Sandfeld Jensen (carewolf), KDE": {
        "x": 1950,
        "y": 82,
        "w": 42
      },
      "Bertjan Broeksema": {
        "x": 1899,
        "y": 92,
        "w": 60
      },
      "444 xxx": {
        "x": 1868.5,
        "y": 95.5,
        "w": 57
      },
      "445 xxx": {
        "x": 1849.5,
        "y": 83.5,
        "w": 51
      },
      "446 xxx": {
        "x": 1799.5,
        "y": 87.5,
        "w": 57
      },
      "447 xxx": {
        "x": 1761.5,
        "y": 79.5,
        "w": 51
      },
      "Dave Neary": {
        "x": 1689,
        "y": 66,
        "w": 60
      },
      "Richard Dale (rdale), SMOKE,  Ruby Qt Bindings": {
        "x": 1629.5,
        "y": 67.5,
        "w": 63
      },
      "Reinout van Schouwen, Gnome NL team": {
        "x": 1605.5,
        "y": 75.5,
        "w": 51
      },
      "451 xxx": {
        "x": 1562.5,
        "y": 62.5,
        "w": 57
      },
      "Renato Chencarek, INdT": {
        "x": 1507.5,
        "y": 84.5,
        "w": 57
      },
      "Thomas Murach": {
        "x": 1478,
        "y": 62,
        "w": 60
      },
      "454 xxx": {
        "x": 1449,
        "y": 64,
        "w": 54
      },
      "Pierre Rossi, Qt": {
        "x": 1399.5,
        "y": 83.5,
        "w": 51
      },
      "456 xxx": {
        "x": 1378,
        "y": 59,
        "w": 54
      },
      "Allison Randal (wendar) Canonical/Parrot": {
        "x": 1355,
        "y": 90,
        "w": 42
      },
      "Kåre Särs, Skanlite, KDE": {
        "x": 1283.5,
        "y": 86.5,
        "w": 51
      },
      "Richard Hughes, GNOME": {
        "x": 1258.5,
        "y": 66.5,
        "w": 51
      },
      "460 xxx": {
        "x": 1210.5,
        "y": 59.5,
        "w": 57
      },
      "461 xxx": {
        "x": 1174.5,
        "y": 71.5,
        "w": 51
      },
      "Guillaume Emont (guijemont), Grilo/GStreamer, Igalia": {
        "x": 1119,
        "y": 83,
        "w": 48
      },
      "Sune Vuorela (svuorela), Debian, KDE": {
        "x": 1053,
        "y": 54,
        "w": 66
      },
      "Luigi Toscano (tosky), KDE Italian translations": {
        "x": 992.5,
        "y": 100.5,
        "w": 51
      },
      "465 Travis Reitter, Collabora": {
        "x": 969,
        "y": 62,
        "w": 60
      },
      "466 xxx": {
        "x": 922,
        "y": 91,
        "w": 54
      },
      "Chris Lord, Mozilla": {
        "x": 892,
        "y": 81,
        "w": 48
      },
      "Andre Heinecke, Intevation, KDE-Windows": {
        "x": 841.5,
        "y": 82.5,
        "w": 57
      },
      "469 xxx": {
        "x": 760,
        "y": 81,
        "w": 54
      },
      "470 xxx": {
        "x": 735.5,
        "y": 90.5,
        "w": 45
      },
      "471 xxx": {
        "x": 691.5,
        "y": 86.5,
        "w": 63
      },
      "Francesco Nwokeka (nwoki), KDE Telepathy": {
        "x": 630.5,
        "y": 57.5,
        "w": 63
      },
      "473 xxx": {
        "x": 590,
        "y": 98,
        "w": 60
      },
      "474 xxx": {
        "x": 519.5,
        "y": 85.5,
        "w": 57
      },
      "Eckhart Wörner, KDE Promo": {
        "x": 462,
        "y": 72,
        "w": 72
      },
      "Lionel Dricot (ploum), Lanedo, Getting Things GNOME!": {
        "x": 413,
        "y": 109,
        "w": 60
      },
      "Richard Schwarting": {
        "x": 317,
        "y": 119,
        "w": 72
      },
      "Patrick Ohly, Intel OTC, MeeGo": {
        "x": 2338,
        "y": 42,
        "w": 60
      },
      "Stéphane MANKOWSKI (miraks), Skrooge": {
        "x": 2207,
        "y": 43,
        "w": 66
      },
      "480 xxx": {
        "x": 2178,
        "y": 52,
        "w": 36
      },
      "481 xxx": {
        "x": 2139.5,
        "y": 45.5,
        "w": 51
      },
      "Olivier Trichet": {
        "x": 2089.5,
        "y": 23.5,
        "w": 63
      },
      "483 xxx": {
        "x": 2037,
        "y": 65,
        "w": 42
      },
      "Will J. Thompson (wjt), Collabora": {
        "x": 1997,
        "y": 65,
        "w": 54
      },
      "485 xxx": {
        "x": 1979.5,
        "y": 59.5,
        "w": 39
      },
      "Nick Richards, Intel": {
        "x": 1939.5,
        "y": 45.5,
        "w": 51
      },
      "Felix Kaser (kaserf), GNOME": {
        "x": 1869,
        "y": 27,
        "w": 66
      },
      "488 xxx (tectu)": {
        "x": 1797,
        "y": 19,
        "w": 66
      },
      "Mario Fux (unormal)": {
        "x": 1722.5,
        "y": 20.5,
        "w": 57
      },
      "Bernhard Wiedemann": {
        "x": 1629.5,
        "y": 7.5,
        "w": 63
      },
      "491 xxx": {
        "x": 1524,
        "y": 24,
        "w": 60
      },
      "Dirk Hohndel (dirkhh), Intel OTC": {
        "x": 1429.5,
        "y": 17.5,
        "w": 63
      },
      "Torsten Rahn (tackat), Marble": {
        "x": 1335.5,
        "y": 35.5,
        "w": 63
      },
      "Boudewĳn Rempt, krita": {
        "x": 1255,
        "y": 25,
        "w": 66
      },
      "495 xxx": {
        "x": 1179,
        "y": 17,
        "w": 66
      },
      "496 xxx": {
        "x": 1104.5,
        "y": 24.5,
        "w": 69
      },
      "André Wöbbeking": {
        "x": 1032,
        "y": 12,
        "w": 60
      },
      "Hylke Bons, RedHat/SparkleShare": {
        "x": 974.5,
        "y": 30.5,
        "w": 57
      },
      "Marco Barisione, Collabora": {
        "x": 911.5,
        "y": 51.5,
        "w": 51
      },
      "Hector Blanco, Warp Networks": {
        "x": 860,
        "y": 24,
        "w": 60
      },
      "501 xxx": {
        "x": 825,
        "y": 49,
        "w": 42
      },
      "Paul Eggleton, Intel": {
        "x": 777,
        "y": 27,
        "w": 66
      },
      "Rob Bradford, Intel": {
        "x": 699.5,
        "y": 43.5,
        "w": 51
      },
      "Michael Meeks, LibreOffice": {
        "x": 611.5,
        "y": 39.5,
        "w": 51
      },
      "Bin Li, openSUSE": {
        "x": 547,
        "y": 37,
        "w": 66
      },
      "Alexander Naumov, openSUSE": {
        "x": 264.5,
        "y": 626.5,
        "w": 93
      }
    },
    "photo": "2011/groupphoto/desktop-summit-akademy-guadec-group-photo-2011.jpg"
  },
  "2012": {
    "people": {
      "Marijn Kruisselbrink (Mek)": {
        "x": 2739,
        "y": 470,
        "w": 66
      },
      "Daniel Gutiérrez Porset": {
        "x": 2721,
        "y": 542,
        "w": 96
      },
      "Saúl Ibarra": {
        "x": 2613,
        "y": 503,
        "w": 90
      },
      "Jean-Paul Saman": {
        "x": 2541,
        "y": 530,
        "w": 90
      },
      "5 XXX": {
        "x": 2498,
        "y": 591,
        "w": 72
      },
      "6 XXX": {
        "x": 2395,
        "y": 639,
        "w": 78
      },
      "7 XXX": {
        "x": 2294,
        "y": 619,
        "w": 72
      },
      "Jean-Baptiste Kempf": {
        "x": 2245,
        "y": 651,
        "w": 78
      },
      "9 XXX": {
        "x": 2123,
        "y": 616,
        "w": 90
      },
      "10 XXX": {
        "x": 2161.5,
        "y": 675.5,
        "w": 87
      },
      "11 XXX": {
        "x": 2063.5,
        "y": 676.5,
        "w": 99
      },
      "Daniel E. Moctezuma": {
        "x": 2095,
        "y": 764,
        "w": 84
      },
      "Casian Andrei": {
        "x": 1973,
        "y": 698,
        "w": 84
      },
      "Victor Blázquez (vblazquez)": {
        "x": 1991,
        "y": 782,
        "w": 90
      },
      "Albert Astals Cid (tsdgeos)": {
        "x": 1931.5,
        "y": 808.5,
        "w": 87
      },
      "16 XXX": {
        "x": 1873.5,
        "y": 839.5,
        "w": 87
      },
      "17 XXX": {
        "x": 1806.5,
        "y": 859.5,
        "w": 99
      },
      "18 XXX": {
        "x": 1714.5,
        "y": 854.5,
        "w": 81
      },
      "19 XXX": {
        "x": 1661,
        "y": 903,
        "w": 108
      },
      "Myriam Schweingruber (mamarok)": {
        "x": 2722.5,
        "y": 1149.5,
        "w": 123
      },
      "Trever Fischer": {
        "x": 2756,
        "y": 989,
        "w": 114
      },
      "Sune Vuorela": {
        "x": 2643,
        "y": 930,
        "w": 102
      },
      "Martin Sandsmark": {
        "x": 2623,
        "y": 1028,
        "w": 108
      },
      "Cornelius Weiss": {
        "x": 2498,
        "y": 954,
        "w": 96
      },
      "Benjamin Port (ben2367)": {
        "x": 2509,
        "y": 1043,
        "w": 90
      },
      "26 XXX": {
        "x": 2345.5,
        "y": 920.5,
        "w": 75
      },
      "Paul Adams (padams)": {
        "x": 2374.5,
        "y": 976.5,
        "w": 75
      },
      "Kevin Ottens (ervin)": {
        "x": 2402.5,
        "y": 1046.5,
        "w": 93
      },
      "Sebastien Renard": {
        "x": 2290.5,
        "y": 1006.5,
        "w": 99
      },
      "Aurélien Gâteau (agateau)": {
        "x": 2213,
        "y": 1032,
        "w": 96
      },
      "Laurent Montel (montel)": {
        "x": 2144,
        "y": 1071,
        "w": 90
      },
      "Sven Petai (hadara)": {
        "x": 2090.5,
        "y": 1007.5,
        "w": 93
      },
      "Lindsay Roberts": {
        "x": 2052,
        "y": 1063,
        "w": 102
      },
      "Quim Gil (qgil)": {
        "x": 1960.5,
        "y": 1014.5,
        "w": 111
      },
      "Casper van Donderen (Pregnant Ghostlike)": {
        "x": 1938,
        "y": 1089,
        "w": 96
      },
      "Thiago Lacerda": {
        "x": 1861,
        "y": 1048,
        "w": 96
      },
      "Volker Krause": {
        "x": 1789.5,
        "y": 1073.5,
        "w": 99
      },
      "Daker Fernandes (dakerfp)": {
        "x": 1723,
        "y": 1056,
        "w": 96
      },
      "Christof Donat": {
        "x": 1610.5,
        "y": 990.5,
        "w": 87
      },
      "40 XXX": {
        "x": 1555.5,
        "y": 968.5,
        "w": 81
      },
      "41 XXX": {
        "x": 1511.5,
        "y": 891.5,
        "w": 111
      },
      "Jaanus Ojangu": {
        "x": 1442,
        "y": 973,
        "w": 102
      },
      "Sander Lepik": {
        "x": 1384,
        "y": 1025,
        "w": 84
      },
      "Alexander Dymo (adymo)": {
        "x": 1440.5,
        "y": 1075.5,
        "w": 87
      },
      "Stephen Kelly (steveire)": {
        "x": 1672,
        "y": 1135,
        "w": 96
      },
      "Olena Dymo": {
        "x": 1503,
        "y": 1098,
        "w": 90
      },
      "Richard Dale (rdale)": {
        "x": 1412,
        "y": 1144,
        "w": 90
      },
      "Frederik Gladhorn (fregl)": {
        "x": 1551.5,
        "y": 1158.5,
        "w": 111
      },
      "Claus Christensen": {
        "x": 1474,
        "y": 1210,
        "w": 102
      },
      "Boudewĳn Rempt": {
        "x": 1348,
        "y": 1167,
        "w": 96
      },
      "Kai-Uwe Behrmann": {
        "x": 1263,
        "y": 1109,
        "w": 96
      },
      "Camilla Boemann (boemann)": {
        "x": 1229.5,
        "y": 1203.5,
        "w": 99
      },
      "David Jarvie": {
        "x": 1361,
        "y": 1261,
        "w": 108
      },
      "Sebastian Kügler (sebas)": {
        "x": 1241.5,
        "y": 1275.5,
        "w": 87
      },
      "Andras Mantia": {
        "x": 1255,
        "y": 1350,
        "w": 96
      },
      "Kevin Krammer (krake)": {
        "x": 1187.5,
        "y": 1407.5,
        "w": 81
      },
      "Ingo Klöcker (mahoutsukai)": {
        "x": 1128.5,
        "y": 1308.5,
        "w": 99
      },
      "Laszlo Papp (djzsapi)": {
        "x": 1064,
        "y": 1260,
        "w": 102
      },
      "Stefan Majewsky": {
        "x": 962,
        "y": 1272,
        "w": 120
      },
      "Allan Sandfeld Jensen (carewolf)": {
        "x": 1041.5,
        "y": 1361.5,
        "w": 105
      },
      "Christian Mollekopf": {
        "x": 1110.5,
        "y": 1410.5,
        "w": 105
      },
      "Klaas Freitag (dragotin)": {
        "x": 968,
        "y": 1423,
        "w": 114
      },
      "Rivo Laks (rivo)": {
        "x": 896.5,
        "y": 1339.5,
        "w": 111
      },
      "Peter Grasch (bedahr)": {
        "x": 806,
        "y": 1420,
        "w": 102
      },
      "Cornelius Schumacher": {
        "x": 902,
        "y": 1507,
        "w": 102
      },
      "Christoph Cullmann": {
        "x": 725.5,
        "y": 1513.5,
        "w": 99
      },
      "67 XXX": {
        "x": 650.5,
        "y": 1461.5,
        "w": 117
      },
      "Christian Reiner (arkascha)": {
        "x": 930,
        "y": 1603,
        "w": 114
      },
      "Eva Brucherseifer (eva)": {
        "x": 792.5,
        "y": 1588.5,
        "w": 99
      },
      "70 XXX": {
        "x": 860.5,
        "y": 1683.5,
        "w": 105
      },
      "71 XXX": {
        "x": 783.5,
        "y": 1706.5,
        "w": 111
      },
      "72 XXX": {
        "x": 697.5,
        "y": 1745.5,
        "w": 105
      },
      "Dominik Haumann": {
        "x": 650,
        "y": 1574,
        "w": 114
      },
      "74 XXX": {
        "x": 409,
        "y": 1497,
        "w": 108
      },
      "75 XXX": {
        "x": 519.5,
        "y": 1604.5,
        "w": 99
      },
      "76 XXX": {
        "x": 485.5,
        "y": 1726.5,
        "w": 93
      },
      "77 XXX": {
        "x": 363.5,
        "y": 1605.5,
        "w": 81
      },
      "78 XXX": {
        "x": 366,
        "y": 1661,
        "w": 90
      },
      "79 XXX": {
        "x": 244.5,
        "y": 1603.5,
        "w": 117
      },
      "80 XXX": {
        "x": 182.5,
        "y": 1675.5,
        "w": 111
      },
      "81 XXX": {
        "x": 327.5,
        "y": 1717.5,
        "w": 99
      },
      "82 XXX": {
        "x": 418,
        "y": 1779,
        "w": 102
      },
      "Merike Sell": {
        "x": 144.5,
        "y": 1772.5,
        "w": 111
      },
      "Shinjo Park (peremen)": {
        "x": 195,
        "y": 1837,
        "w": 96
      },
      "Eduardo Robles Elvira (Edulix)": {
        "x": 440,
        "y": 1857,
        "w": 96
      },
      "Dani Garcia": {
        "x": 504,
        "y": 1827,
        "w": 96
      },
      "Jure Repinc": {
        "x": 604,
        "y": 1848,
        "w": 114
      },
      "Alejandro Castaño": {
        "x": 716.5,
        "y": 1881.5,
        "w": 105
      },
      "Jiyun Kang": {
        "x": 83,
        "y": 1924,
        "w": 108
      },
      "Adrian Lopez": {
        "x": 338.5,
        "y": 1912.5,
        "w": 111
      },
      "Neja Repinc": {
        "x": 510,
        "y": 1976,
        "w": 114
      },
      "92 XXX": {
        "x": 408.5,
        "y": 2012.5,
        "w": 117
      },
      "Kristjan Kullerkupp (Pelmzh)": {
        "x": 2739,
        "y": 1895,
        "w": 120
      },
      "Rishab Arora (spacetime)": {
        "x": 2675.5,
        "y": 1746.5,
        "w": 99
      },
      "Karl Kadalipp": {
        "x": 2654.5,
        "y": 1651.5,
        "w": 105
      },
      "Martin Klapetek (mck182)": {
        "x": 2562.5,
        "y": 1599.5,
        "w": 117
      },
      "97 XXX": {
        "x": 2518.5,
        "y": 1722.5,
        "w": 123
      },
      "Kenny Duffus (seaLne)": {
        "x": 2545,
        "y": 1893,
        "w": 120
      },
      "Giulia Dani (JiveDive)": {
        "x": 2363.5,
        "y": 1800.5,
        "w": 117
      },
      "Valdur Kana": {
        "x": 2243,
        "y": 1851,
        "w": 120
      },
      "101 XXX": {
        "x": 2355,
        "y": 1685,
        "w": 120
      },
      "Kristjan Kütaru": {
        "x": 2426.5,
        "y": 1611.5,
        "w": 105
      },
      "Lee Marion Lepik": {
        "x": 2336,
        "y": 1570,
        "w": 126
      },
      "Andres Toomsalu (opennode)": {
        "x": 2268,
        "y": 1639,
        "w": 114
      },
      "Heiki Ojasild (Repentinus)": {
        "x": 2168,
        "y": 1780,
        "w": 108
      },
      "Pradeepto Bhattacharya (pradeepto)": {
        "x": 2070.5,
        "y": 1697.5,
        "w": 111
      },
      "Valorie Zimmerman (valorie)": {
        "x": 2125,
        "y": 1623,
        "w": 102
      },
      "Carl Symons (kallecarl)": {
        "x": 2179.5,
        "y": 1521.5,
        "w": 117
      },
      "Kenny Coyle (automatical)": {
        "x": 2017,
        "y": 1528,
        "w": 126
      },
      "Claudia Rauch (claudiar)": {
        "x": 1935.5,
        "y": 1672.5,
        "w": 111
      },
      "Sivan Green (sivang)": {
        "x": 1931,
        "y": 1805,
        "w": 108
      },
      "Laur Mõtus (vprints)": {
        "x": 1788.5,
        "y": 1675.5,
        "w": 111
      },
      "Jos Poortvliet (josportvliet)": {
        "x": 4222.5,
        "y": 2646.5,
        "w": 117
      },
      "Jeroeen Meuween (jmeuween)": {
        "x": 4129,
        "y": 2676,
        "w": 102
      },
      "Lasse Liehu (lliehu)": {
        "x": 3994,
        "y": 2242,
        "w": 102
      },
      "Niklas Laxström (Nikerabbit)": {
        "x": 3948.5,
        "y": 2196.5,
        "w": 93
      },
      "117 XXX": {
        "x": 4394,
        "y": 2087,
        "w": 114
      },
      "Deb Nicholson": {
        "x": 4215,
        "y": 2074,
        "w": 114
      },
      "119 XXX": {
        "x": 4157.5,
        "y": 1966.5,
        "w": 123
      },
      "Knut Yrvin": {
        "x": 3814.5,
        "y": 2066.5,
        "w": 105
      },
      "George Kiagiadakis (gkiagia)": {
        "x": 3950,
        "y": 2029,
        "w": 96
      },
      "Gaurav Chaturvedi (tazz)": {
        "x": 3892,
        "y": 1968,
        "w": 90
      },
      "Thiago Macieira (thiago)": {
        "x": 3796.5,
        "y": 2003.5,
        "w": 87
      },
      "Lydia Pintscher (Nightrose)": {
        "x": 3611,
        "y": 1997,
        "w": 102
      },
      "Heinz Wiesinger (pprkut)": {
        "x": 3739,
        "y": 1943,
        "w": 90
      },
      "Jake Edge": {
        "x": 3856,
        "y": 1875,
        "w": 102
      },
      "127 XXX": {
        "x": 3915.5,
        "y": 1827.5,
        "w": 117
      },
      "Rafal Kulaga (rkulaga)": {
        "x": 3788,
        "y": 1749,
        "w": 108
      },
      "Siim Adamson": {
        "x": 4004.5,
        "y": 1699.5,
        "w": 99
      },
      "Marko Pusaar": {
        "x": 3904,
        "y": 1635,
        "w": 102
      },
      "Silver Püvi": {
        "x": 3843,
        "y": 1515,
        "w": 90
      },
      "Andras Becsi (bbandix)": {
        "x": 3708,
        "y": 1561,
        "w": 90
      },
      "Riccardo Iaconelli (ruphy)": {
        "x": 3618,
        "y": 1780,
        "w": 96
      },
      "Marco Martin (notmart)": {
        "x": 3663,
        "y": 1691,
        "w": 96
      },
      "Luciano Montanaro (mikelima)": {
        "x": 3601.5,
        "y": 1656.5,
        "w": 87
      },
      "Davide Bettio": {
        "x": 3518.5,
        "y": 1578.5,
        "w": 99
      },
      "137 XXX": {
        "x": 3573,
        "y": 1486,
        "w": 78
      },
      "Jędrzej Nowacki (nierob)": {
        "x": 3645,
        "y": 1502,
        "w": 102
      },
      "Rui Dias": {
        "x": 3753.5,
        "y": 1458.5,
        "w": 75
      },
      "140 XXX": {
        "x": 3731,
        "y": 1411,
        "w": 72
      },
      "141 XXX": {
        "x": 3390,
        "y": 1499,
        "w": 90
      },
      "Runa Bhattacharjee": {
        "x": 3295,
        "y": 1397,
        "w": 90
      },
      "Inge Wallin (ingwa)": {
        "x": 2849,
        "y": 1066,
        "w": 90
      },
      "144 XXX": {
        "x": 2976,
        "y": 1225,
        "w": 0
      },
      "Sinny Kumari (sinny)": {
        "x": 2907,
        "y": 1166,
        "w": 84
      },
      "Stuart Dickson (stuartmd)": {
        "x": 2972.5,
        "y": 1075.5,
        "w": 75
      },
      "Shreya Pandit (shreya)": {
        "x": 2994.5,
        "y": 1155.5,
        "w": 81
      },
      "Margus Ernits (magavdraakon)": {
        "x": 3531,
        "y": 1374,
        "w": 84
      },
      "Marek Laane (Qilaq)": {
        "x": 3429,
        "y": 1335,
        "w": 126
      },
      "Will Schroeder": {
        "x": 3616.5,
        "y": 1314.5,
        "w": 93
      },
      "Akarsh Simha (kstar)": {
        "x": 3566.5,
        "y": 1263.5,
        "w": 93
      },
      "152 XXX": {
        "x": 3524,
        "y": 1190,
        "w": 84
      },
      "Edmund Laugasson (zeroconf)": {
        "x": 3311,
        "y": 1267,
        "w": 84
      },
      "Alexander Neundorf": {
        "x": 3294.5,
        "y": 1207.5,
        "w": 69
      },
      "Antje Neundorf": {
        "x": 3237,
        "y": 1208,
        "w": 60
      },
      "Àlex Fiestas (afiestas)": {
        "x": 3179.5,
        "y": 1163.5,
        "w": 81
      },
      "Vishesh Handa (vHanda)": {
        "x": 3080,
        "y": 1139,
        "w": 78
      },
      "董思远 (siyuandong)": {
        "x": 3122,
        "y": 1097,
        "w": 78
      },
      "David Edmundson (d_ed)": {
        "x": 3213.5,
        "y": 1102.5,
        "w": 81
      },
      "Aleix Pol Gonzalez (apol)": {
        "x": 3293,
        "y": 1148,
        "w": 78
      },
      "Uwe Geuder (u1106)": {
        "x": 3420.5,
        "y": 1118.5,
        "w": 81
      },
      "162 XXX": {
        "x": 3547,
        "y": 1110,
        "w": 96
      },
      "Shantanu Tushar (shaan7)": {
        "x": 3543,
        "y": 1038,
        "w": 78
      },
      "Daniel Vrátil (dvratil)": {
        "x": 3268.5,
        "y": 1069.5,
        "w": 75
      },
      "Rohan Garg (shadeslayer)": {
        "x": 3265,
        "y": 991,
        "w": 90
      },
      "Clemens Toennies": {
        "x": 3360,
        "y": 987,
        "w": 96
      },
      "167 XXX": {
        "x": 3536.5,
        "y": 872.5,
        "w": 81
      },
      "168 XXX": {
        "x": 3449.5,
        "y": 806.5,
        "w": 75
      },
      "169 XXX": {
        "x": 3544,
        "y": 739,
        "w": 72
      },
      "Agustín Benito Bethencourt": {
        "x": 3545.5,
        "y": 694.5,
        "w": 63
      },
      "Arjen Hiemstra": {
        "x": 3508.5,
        "y": 632.5,
        "w": 75
      },
      "Nuno Pinheiro": {
        "x": 3420.5,
        "y": 626.5,
        "w": 63
      },
      "Felix Rohrbach": {
        "x": 3426.5,
        "y": 543.5,
        "w": 81
      },
      "Martin Gräßlin": {
        "x": 3369.5,
        "y": 525.5,
        "w": 69
      },
      "Janek Sarjas": {
        "x": 2786,
        "y": 457,
        "w": 60
      },
      "Lauri Võsandi (lauri)": {
        "x": 2813.5,
        "y": 422.5,
        "w": 69
      },
      "Allan Vein": {
        "x": 2873.5,
        "y": 409.5,
        "w": 69
      },
      "David Faure (dfaure)": {
        "x": 2934.5,
        "y": 337.5,
        "w": 75
      },
      "Antonio Larrosa Jimenez (antlarr)": {
        "x": 3013.5,
        "y": 356.5,
        "w": 57
      },
      "180 XXX": {
        "x": 3028.5,
        "y": 310.5,
        "w": 69
      },
      "Johannes Dahl": {
        "x": 3124,
        "y": 291,
        "w": 66
      },
      "Matti Saastamoinen": {
        "x": 3170,
        "y": 281,
        "w": 60
      },
      "Dan Leinir Turthra Jensen": {
        "x": 3315.5,
        "y": 468.5,
        "w": 63
      },
      "Daniele E. Domenichelli (drdanz)": {
        "x": 3379,
        "y": 430,
        "w": 60
      },
      "Kåre Särs": {
        "x": 3273.5,
        "y": 379.5,
        "w": 51
      },
      "Patrick Spendrin": {
        "x": 3346.5,
        "y": 358.5,
        "w": 63
      },
      "187 XXX": {
        "x": 3388.5,
        "y": 312.5,
        "w": 63
      },
      "188 XXX": {
        "x": 3355.5,
        "y": 311.5,
        "w": 51
      },
      "189 XXX": {
        "x": 3318.5,
        "y": 260.5,
        "w": 69
      },
      "Gunnar Schmidt": {
        "x": 3271,
        "y": 254,
        "w": 72
      },
      "Ilkka Lehtinen": {
        "x": 3186,
        "y": 230,
        "w": 78
      },
      "Jonathan Riddell (Riddell)": {
        "x": 3316,
        "y": 173,
        "w": 84
      }
    },
    "photo": "2012/groupphoto/ak2012-group.jpg"
  },
  "2013": {
    "people": {
      "Antonio Larrosa Jimenez (antlarr)": {
        "x": 4086,
        "y": 853,
        "w": 114
      },
      "Jerome Leclanche (Adys/jleclanche)": {
        "x": 4273,
        "y": 809,
        "w": 150
      },
      "Victor Blázquez (vblazquez)": {
        "x": 4115,
        "y": 784,
        "w": 102
      },
      "David Gil Oliva (dgilo)": {
        "x": 4304.5,
        "y": 524.5,
        "w": 111
      },
      "Daniele E. Domenichelli (drdanz)": {
        "x": 4224,
        "y": 566,
        "w": 120
      },
      "Clemens Toennies (starbuck)": {
        "x": 4132,
        "y": 525,
        "w": 114
      },
      "Shantanu Tushar (Shann7)": {
        "x": 4043,
        "y": 589,
        "w": 108
      },
      "Dominik Haumann (dhaumann)": {
        "x": 4023,
        "y": 592,
        "w": 48
      },
      "Milian Wolff (milian)": {
        "x": 3942.5,
        "y": 710.5,
        "w": 117
      },
      "Aleix Pol Gonzalez (apol)": {
        "x": 3956.5,
        "y": 600.5,
        "w": 93
      },
      "Philip Muškovac (yofel)": {
        "x": 3921,
        "y": 550,
        "w": 84
      },
      "Juan Ases (Ases)": {
        "x": 3869.5,
        "y": 591.5,
        "w": 99
      },
      "Björn Balazs (Alcapond)": {
        "x": 3817.5,
        "y": 637.5,
        "w": 87
      },
      "Torsten Rahn (tackat)": {
        "x": 3734.5,
        "y": 620.5,
        "w": 99
      },
      "Allan Sandfeld Jensen (carewolf)": {
        "x": 3805.5,
        "y": 619.5,
        "w": 45
      },
      "Eduardo Robles Elvira (edulix)": {
        "x": 4017.5,
        "y": 815.5,
        "w": 105
      },
      "Jose Millan Soto (fid_jose)": {
        "x": 3994.5,
        "y": 906.5,
        "w": 123
      },
      "Christoph Cullmann (cullmann)": {
        "x": 3859.5,
        "y": 825.5,
        "w": 105
      },
      "Joseph Wenninger (jowenn)": {
        "x": 3833.5,
        "y": 895.5,
        "w": 111
      },
      "Mailson Menezes (mailson)": {
        "x": 3766.5,
        "y": 826.5,
        "w": 111
      },
      "Luigi Toscano (tosky)": {
        "x": 3726.5,
        "y": 898.5,
        "w": 93
      },
      "Sinny Kumari (ksinny)": {
        "x": 3688.5,
        "y": 981.5,
        "w": 111
      },
      "Kevin Krammer (krake)": {
        "x": 3673,
        "y": 918,
        "w": 78
      },
      "Fabio D'Urso (fabiod)": {
        "x": 3595,
        "y": 860,
        "w": 114
      },
      "Luca Novello": {
        "x": 3573,
        "y": 964,
        "w": 90
      },
      "Heiko Tietze": {
        "x": 3677.5,
        "y": 657.5,
        "w": 75
      },
      "Alexander Dymo (adymo)": {
        "x": 3583,
        "y": 667,
        "w": 78
      },
      "Nuno Pinheiro (pinheiro)": {
        "x": 3638.5,
        "y": 689.5,
        "w": 69
      },
      "Olena Dymo (olenadymo)": {
        "x": 3522,
        "y": 729,
        "w": 90
      },
      "Volker Krause (vkrause)": {
        "x": 3527,
        "y": 645,
        "w": 78
      },
      "31 XXX": {
        "x": 3488.5,
        "y": 684.5,
        "w": 81
      },
      "32 XXX": {
        "x": 3421.5,
        "y": 675.5,
        "w": 93
      },
      "33 XXX": {
        "x": 3383,
        "y": 730,
        "w": 72
      },
      "Roland Krause": {
        "x": 3324,
        "y": 694,
        "w": 84
      },
      "35 XXX": {
        "x": 3292,
        "y": 741,
        "w": 60
      },
      "37 XXX": {
        "x": 3242,
        "y": 684,
        "w": 84
      },
      "Dennis Nienhüser (Earthwings)": {
        "x": 3219.5,
        "y": 739.5,
        "w": 99
      },
      "Axel Davy": {
        "x": 3166,
        "y": 754,
        "w": 78
      },
      "Leo Savernik": {
        "x": 3117,
        "y": 709,
        "w": 78
      },
      "Oksana Kovalevska": {
        "x": 3085,
        "y": 757,
        "w": 102
      },
      "Anant Kamath (flak37)": {
        "x": 3025.5,
        "y": 748.5,
        "w": 81
      },
      "Lamarque Souza (lamarque)": {
        "x": 2993,
        "y": 803,
        "w": 84
      },
      "Kenny Coyle (automatical)": {
        "x": 2930,
        "y": 786,
        "w": 90
      },
      "Anton Kreuzkamp (akreuzkamp)": {
        "x": 2889.5,
        "y": 754.5,
        "w": 75
      },
      "46 XXX": {
        "x": 2839.5,
        "y": 787.5,
        "w": 75
      },
      "Ignaz Forster (ifo)": {
        "x": 2786,
        "y": 753,
        "w": 78
      },
      "Shinjo Park (peremen)": {
        "x": 2796,
        "y": 819,
        "w": 72
      },
      "49 XXX": {
        "x": 2750.5,
        "y": 793.5,
        "w": 63
      },
      "Martin Gonzalez (kote)": {
        "x": 3452.5,
        "y": 883.5,
        "w": 93
      },
      "David Greaves (lbt)": {
        "x": 3497,
        "y": 924,
        "w": 108
      },
      "Silvia Montanaro": {
        "x": 3386,
        "y": 975,
        "w": 108
      },
      "Adrian Chaves Fernandez (Gallaecio)": {
        "x": 3316,
        "y": 836,
        "w": 102
      },
      "Rishab Arora (spacetime)": {
        "x": 3354.5,
        "y": 910.5,
        "w": 87
      },
      "Laszlo Papp (lpapp)": {
        "x": 3271,
        "y": 901,
        "w": 90
      },
      "Luciano Montanaro (mikelima)": {
        "x": 3288,
        "y": 964,
        "w": 96
      },
      "Neja Repinc (SmrtSkoso)": {
        "x": 3216.5,
        "y": 988.5,
        "w": 105
      },
      "Heinz Wiesinger (pprkut)": {
        "x": 3171.5,
        "y": 913.5,
        "w": 99
      },
      "Albert Astals Cid (tsdgeos)": {
        "x": 3093.5,
        "y": 861.5,
        "w": 117
      },
      "Jure Repinc (JLP)": {
        "x": 3091,
        "y": 962,
        "w": 132
      },
      "Samikshan Bairagya (samikshan)": {
        "x": 2958.5,
        "y": 1005.5,
        "w": 111
      },
      "Jonathan Riddell (Riddell)": {
        "x": 2902,
        "y": 1085,
        "w": 126
      },
      "Shivam Makkar (amourphious) ???": {
        "x": 2713,
        "y": 778,
        "w": 54
      },
      "Andreas Cord-Landwehr (CoLa)": {
        "x": 2678.5,
        "y": 840.5,
        "w": 57
      },
      "Lasse Liehu (lliehu)": {
        "x": 2722,
        "y": 831,
        "w": 60
      },
      "Thomas McGuire (tmcguire)": {
        "x": 2642,
        "y": 816,
        "w": 60
      },
      "Sébastien Renard (renards)": {
        "x": 2581.5,
        "y": 786.5,
        "w": 75
      },
      "Kevin Ottens (ervin)": {
        "x": 2587,
        "y": 846,
        "w": 90
      },
      "Devaja Shah (devaja)": {
        "x": 2873,
        "y": 1001,
        "w": 96
      },
      "Yash Shah (yashshah)": {
        "x": 2810,
        "y": 1041,
        "w": 108
      },
      "Matti Saastamoinen (smoinen)": {
        "x": 2730.5,
        "y": 1019.5,
        "w": 99
      },
      "Smit Mehta (alloy)": {
        "x": 2623,
        "y": 916,
        "w": 114
      },
      "Iker Salmon (shaola)": {
        "x": 2721.5,
        "y": 1090.5,
        "w": 129
      },
      "Lydia Pintscher (Nightrose)": {
        "x": 2623.5,
        "y": 1052.5,
        "w": 135
      },
      "Vishesh Handa (vHanda)": {
        "x": 2549,
        "y": 975,
        "w": 132
      },
      "Valério Valério (VDVsx)": {
        "x": 2388,
        "y": 1092,
        "w": 114
      },
      "Rouald v Schlegell": {
        "x": 2542,
        "y": 828,
        "w": 72
      },
      "78 XXX": {
        "x": 2503,
        "y": 815,
        "w": 60
      },
      "Martin Gräßlin (mgraesslin)": {
        "x": 2468.5,
        "y": 861.5,
        "w": 75
      },
      "David Edmundson (d_ed)": {
        "x": 2407.5,
        "y": 851.5,
        "w": 81
      },
      "Dario Freddi (drf__)": {
        "x": 2441,
        "y": 805,
        "w": 78
      },
      "Vesa-Matti Hartikainen (veskuh)": {
        "x": 2341,
        "y": 844,
        "w": 90
      },
      "David Faure (dfaure)": {
        "x": 2310.5,
        "y": 825.5,
        "w": 63
      },
      "John Layt (jlayt)": {
        "x": 2296,
        "y": 873,
        "w": 72
      },
      "Peter Grasch (bedahr)": {
        "x": 2269,
        "y": 843,
        "w": 60
      },
      "Martin Klapetek (mck182)": {
        "x": 2211.5,
        "y": 863.5,
        "w": 87
      },
      "Jonas Vejlin (Beer)": {
        "x": 2123,
        "y": 834,
        "w": 90
      },
      "Stefan Derkits (HorusHorrendus)": {
        "x": 2182.5,
        "y": 881.5,
        "w": 57
      },
      "Stephen Kelly (steveire)": {
        "x": 2169.5,
        "y": 1075.5,
        "w": 141
      },
      "Gunnar Schmidt": {
        "x": 2070,
        "y": 1059,
        "w": 120
      },
      "Teo Mrnjavac (teo-)": {
        "x": 2018.5,
        "y": 891.5,
        "w": 81
      },
      "Daniel Vrátil (dvratil)": {
        "x": 2075.5,
        "y": 865.5,
        "w": 81
      },
      "Patrick Spendrin (SaroEngels)": {
        "x": 1973.5,
        "y": 851.5,
        "w": 81
      },
      "Audrey Prevost (GiGi)": {
        "x": 1966,
        "y": 927,
        "w": 90
      },
      "Rui Dias": {
        "x": 1885.5,
        "y": 880.5,
        "w": 81
      },
      "Jan Grulich (jgrulich)": {
        "x": 1947,
        "y": 912,
        "w": 48
      },
      "97 XXX": {
        "x": 1871,
        "y": 941,
        "w": 102
      },
      "Holger Schröder": {
        "x": 1925,
        "y": 1061,
        "w": 132
      },
      "Will Stephenson (Bille)": {
        "x": 1944,
        "y": 1252,
        "w": 120
      },
      "Sayantan Datta (kenzo450D)": {
        "x": 1894,
        "y": 1169,
        "w": 126
      },
      "Olivia Kiok": {
        "x": 1831.5,
        "y": 1051.5,
        "w": 117
      },
      "Baltasar Ortega (baltolkien)": {
        "x": 1809.5,
        "y": 929.5,
        "w": 87
      },
      "Philipp Schmidt (hefeweiz3n)": {
        "x": 1774.5,
        "y": 956.5,
        "w": 57
      },
      "Jean-Baptiste Kempf (j-b)": {
        "x": 1707,
        "y": 904,
        "w": 96
      },
      "Valorie Zimmerman (valorie)": {
        "x": 1669,
        "y": 1184,
        "w": 132
      },
      "Bo Thorsen": {
        "x": 1650,
        "y": 1095,
        "w": 120
      },
      "Marta Rybczyńska": {
        "x": 1507,
        "y": 1128,
        "w": 120
      },
      "Oihane Kamara (oihanek)": {
        "x": 1492,
        "y": 1275,
        "w": 102
      },
      "Felix Paul Kühne (feepk)": {
        "x": 1615,
        "y": 915,
        "w": 96
      },
      "110 XXX": {
        "x": 1685,
        "y": 960,
        "w": 60
      },
      "Agustín Benito Bethencourt (toscalix)": {
        "x": 1532.5,
        "y": 942.5,
        "w": 105
      },
      "Ludovic (etix)": {
        "x": 1475.5,
        "y": 900.5,
        "w": 99
      },
      "113 XXX": {
        "x": 1414.5,
        "y": 948.5,
        "w": 105
      },
      "Matěj Laitl (strohel)": {
        "x": 1342,
        "y": 967,
        "w": 96
      },
      "115 XXX": {
        "x": 1292.5,
        "y": 936.5,
        "w": 87
      },
      "Cornelius Schumacher (cornelius)": {
        "x": 1173.5,
        "y": 937.5,
        "w": 123
      },
      "Patrick von Reth (TheOneRing)": {
        "x": 1265,
        "y": 988,
        "w": 72
      },
      "118 XXX": {
        "x": 1092.5,
        "y": 955.5,
        "w": 111
      },
      "Till Adam (till)": {
        "x": 973,
        "y": 982,
        "w": 102
      },
      "Kai Koehne": {
        "x": 1030,
        "y": 942,
        "w": 96
      },
      "Aitor Pazos Ibarzabal (aitorpazos)": {
        "x": 912.5,
        "y": 966.5,
        "w": 75
      },
      "Claus Christensen (Claus_chr)": {
        "x": 878,
        "y": 1011,
        "w": 90
      },
      "123 XXX": {
        "x": 738,
        "y": 977,
        "w": 108
      },
      "124 XXX": {
        "x": 816.5,
        "y": 1020.5,
        "w": 87
      },
      "Cruz Enrique Borges Hernández (cruzki)": {
        "x": 1418.5,
        "y": 1224.5,
        "w": 117
      },
      "Dani Gutiérrez (danitxu)": {
        "x": 1380.5,
        "y": 1122.5,
        "w": 111
      },
      "127 XXX": {
        "x": 1323,
        "y": 1083,
        "w": 102
      },
      "Gorka Sorrosal (gsorrosal)": {
        "x": 1197.5,
        "y": 1111.5,
        "w": 129
      },
      "Davide Bettio": {
        "x": 1163.5,
        "y": 1274.5,
        "w": 129
      },
      "Ander Pijoan (ander)": {
        "x": 1085,
        "y": 1190,
        "w": 138
      },
      "Miquel Sabaté Solà (mssola)": {
        "x": 993.5,
        "y": 1176.5,
        "w": 117
      },
      "Albert Vaca Cintora (elvaka)": {
        "x": 992,
        "y": 1271,
        "w": 126
      },
      "Ivan Čukić": {
        "x": 714.5,
        "y": 1135.5,
        "w": 111
      },
      "Friedrich W. H. Kossebau": {
        "x": 590.5,
        "y": 999.5,
        "w": 105
      },
      "Richard Dale (rdale)": {
        "x": 553.5,
        "y": 1062.5,
        "w": 87
      },
      "Camila Ayres": {
        "x": 503.5,
        "y": 1106.5,
        "w": 99
      },
      "137 XXX": {
        "x": 419,
        "y": 1068,
        "w": 108
      },
      "David E. Narváez (dMaggot)": {
        "x": 290,
        "y": 1065,
        "w": 90
      },
      "Kenny Duffus (seaLne)": {
        "x": 342,
        "y": 1087,
        "w": 114
      },
      "Kåre Särs": {
        "x": 201,
        "y": 1050,
        "w": 120
      },
      "Jos van den Oever (vandenoever)": {
        "x": 103,
        "y": 1046,
        "w": 126
      },
      "Nicolás Alvarez (PovAddict)": {
        "x": 865.5,
        "y": 1191.5,
        "w": 123
      },
      "Sven Brauch (scummos)": {
        "x": 813.5,
        "y": 1286.5,
        "w": 129
      },
      "Ran Lu (rlu144)": {
        "x": 786,
        "y": 1199,
        "w": 114
      },
      "Franck Arrecot (Knarfy)": {
        "x": 628.5,
        "y": 1222.5,
        "w": 135
      },
      "Helio Castro (heliocastro)": {
        "x": 647,
        "y": 1326,
        "w": 126
      },
      "Marco Martin (notmart)": {
        "x": 518.5,
        "y": 1219.5,
        "w": 123
      },
      "Àlex Fiestas (afiestas)": {
        "x": 499.5,
        "y": 1311.5,
        "w": 129
      },
      "Angel Abad (angelabad)": {
        "x": 450,
        "y": 1262,
        "w": 96
      },
      "Sebastian Kügler (sebas)": {
        "x": 361.5,
        "y": 1272.5,
        "w": 117
      },
      "Thiago Macieira (thiago)": {
        "x": 228,
        "y": 1256,
        "w": 126
      },
      "Ignacio Serantes (iserantes)": {
        "x": 313.5,
        "y": 1343.5,
        "w": 135
      },
      "Jos Poortvliet (jospoortvliet)": {
        "x": 1738.5,
        "y": 1381.5,
        "w": 141
      }
    },
    "photo": "2013/groupphoto/ak2013-group-photo.jpg"
  },
  "2014": {
    "people": {
      "1 ()": {
        "x": 4354.5,
        "y": 405.5,
        "w": 99
      },
      "2 ()": {
        "x": 4259.5,
        "y": 333.5,
        "w": 117
      },
      "Martin Kollman": {
        "x": 4207,
        "y": 285,
        "w": 102
      },
      "4 ()": {
        "x": 4162.5,
        "y": 351.5,
        "w": 93
      },
      "David Kolibac (davkol)": {
        "x": 4256,
        "y": 484,
        "w": 102
      },
      "6 ()": {
        "x": 4214.5,
        "y": 412.5,
        "w": 99
      },
      "Shantanu Tushar (Shaan7)": {
        "x": 4066.5,
        "y": 516.5,
        "w": 123
      },
      "Jan Kundrat (jkt)": {
        "x": 4037.5,
        "y": 424.5,
        "w": 123
      },
      "Martin Kyral": {
        "x": 4007.5,
        "y": 309.5,
        "w": 123
      },
      "Richard Dale (rdale)": {
        "x": 3926,
        "y": 346,
        "w": 108
      },
      "Rishab Arora (spacetime)": {
        "x": 3933.5,
        "y": 475.5,
        "w": 129
      },
      "Scarlett Clark (sgclark)": {
        "x": 4179,
        "y": 776,
        "w": 132
      },
      "Josef Kufner": {
        "x": 3557.5,
        "y": 560.5,
        "w": 123
      },
      "Simon Wächter (swaechter)": {
        "x": 3785.5,
        "y": 362.5,
        "w": 81
      },
      "Kenny Coyle (autonomical)": {
        "x": 3740.5,
        "y": 349.5,
        "w": 69
      },
      "Sven Brauch (scummos)": {
        "x": 3660,
        "y": 353,
        "w": 102
      },
      "17 ()": {
        "x": 3600.5,
        "y": 355.5,
        "w": 81
      },
      "Dominik Haumann (dhaumann)": {
        "x": 3507,
        "y": 359,
        "w": 120
      },
      "Allan Sandfeld Jensen (careworld)": {
        "x": 3535.5,
        "y": 445.5,
        "w": 123
      },
      "Luigi Toscano (tosky)": {
        "x": 3806,
        "y": 538,
        "w": 150
      },
      "Bruno Friedmann (tigerfoot)": {
        "x": 3961,
        "y": 769,
        "w": 114
      },
      "Kenny Duffus (seaLne)": {
        "x": 3965,
        "y": 677,
        "w": 114
      },
      "Francoise Wybrecht ()": {
        "x": 3842.5,
        "y": 780.5,
        "w": 123
      },
      "Jan Grulich (jgrulich)": {
        "x": 3708.5,
        "y": 737.5,
        "w": 123
      },
      "Thomas Posch ()": {
        "x": 3644.5,
        "y": 669.5,
        "w": 123
      },
      "26 ()": {
        "x": 3530,
        "y": 728,
        "w": 132
      },
      "Ingo Klöcker": {
        "x": 3408.5,
        "y": 666.5,
        "w": 135
      },
      "Yash Shah(yashshah)": {
        "x": 3286.5,
        "y": 818.5,
        "w": 141
      },
      "Sinny Kumari (ksinny)": {
        "x": 3187.5,
        "y": 847.5,
        "w": 147
      },
      "Peter Grasch (bedahr)": {
        "x": 3220,
        "y": 664,
        "w": 126
      },
      "Akshay Ratan (akshay_r)": {
        "x": 3086,
        "y": 787,
        "w": 138
      },
      "Sujith Haridasan (sujith_h)": {
        "x": 3022.5,
        "y": 661.5,
        "w": 141
      },
      "Joseph Wenninger ()": {
        "x": 2970.5,
        "y": 784.5,
        "w": 141
      },
      "Helio Castro (heliocastro)": {
        "x": 2786,
        "y": 817,
        "w": 144
      },
      "35 ()": {
        "x": 2716.5,
        "y": 875.5,
        "w": 111
      },
      "Lydia Pintscher (Nightrose)": {
        "x": 2590.5,
        "y": 905.5,
        "w": 117
      },
      "Aracele Torres (araceletorres)": {
        "x": 2518,
        "y": 821,
        "w": 132
      },
      "Claus Christensen ()": {
        "x": 2664,
        "y": 737,
        "w": 144
      },
      "Ivan Čukić (ivan|home)": {
        "x": 2819,
        "y": 655,
        "w": 138
      },
      "Frederik Gladhorn (fregl)": {
        "x": 3453.5,
        "y": 522.5,
        "w": 117
      },
      "Marco Martin (notmart)": {
        "x": 3326,
        "y": 557,
        "w": 150
      },
      "Bruno Coudoin (bdoin)": {
        "x": 3174.5,
        "y": 569.5,
        "w": 129
      },
      "Christoph Cullmann (cullmann)": {
        "x": 3398.5,
        "y": 365.5,
        "w": 99
      },
      "44 ()": {
        "x": 3373,
        "y": 430,
        "w": 132
      },
      "Philip Muškovac (yofel)": {
        "x": 3255,
        "y": 440,
        "w": 126
      },
      "Patrick Spendrin (sengels)": {
        "x": 3295,
        "y": 294,
        "w": 132
      },
      "Leo Savernik": {
        "x": 3182.5,
        "y": 316.5,
        "w": 111
      },
      "Jonathan Riddell (Riddell)": {
        "x": 3096.5,
        "y": 440.5,
        "w": 117
      },
      "Albert Astals Cid (tsdgeos)": {
        "x": 2933,
        "y": 459,
        "w": 114
      },
      "Tejo  ()": {
        "x": 3059,
        "y": 336,
        "w": 96
      },
      "Friedrich W. H. Kossebau (frinring)": {
        "x": 2968.5,
        "y": 332.5,
        "w": 117
      },
      "Jaroslav Kames": {
        "x": 2887,
        "y": 368,
        "w": 126
      },
      "Volker Krause (volker)": {
        "x": 2798.5,
        "y": 340.5,
        "w": 105
      },
      "54 ()": {
        "x": 2726,
        "y": 362,
        "w": 108
      },
      "Michael Bohlender (mbohlender)": {
        "x": 2706.5,
        "y": 436.5,
        "w": 117
      },
      "56 ()": {
        "x": 2595.5,
        "y": 370.5,
        "w": 105
      },
      "57 ()": {
        "x": 2515,
        "y": 382,
        "w": 102
      },
      "Franklin Weng (franklin)": {
        "x": 2530.5,
        "y": 465.5,
        "w": 81
      },
      "59 ()": {
        "x": 2459.5,
        "y": 439.5,
        "w": 99
      },
      "60 ()": {
        "x": 2375,
        "y": 381,
        "w": 78
      },
      "Jarosław Staniek (jstaniek)": {
        "x": 2331,
        "y": 427,
        "w": 84
      },
      "62 ()": {
        "x": 2277,
        "y": 416,
        "w": 78
      },
      "Frank Karlitschek ()": {
        "x": 2214,
        "y": 398,
        "w": 90
      },
      "Andreas Cord-Landwehr (CoLa)": {
        "x": 2345,
        "y": 571,
        "w": 132
      },
      "Heiko Tietze (htietze)": {
        "x": 2485,
        "y": 716,
        "w": 126
      },
      "Lamarque Souza (lamarque)": {
        "x": 2366,
        "y": 818,
        "w": 120
      },
      "Filipe Saraiva (filipesaraiva)": {
        "x": 2293,
        "y": 712,
        "w": 132
      },
      "Victor Blázquez (vblazquez)": {
        "x": 2217.5,
        "y": 816.5,
        "w": 135
      },
      "Lukas Tinkl  (ltinkl)": {
        "x": 2069,
        "y": 791,
        "w": 150
      },
      "Martin Gräßlin (mgraesslin)": {
        "x": 2185.5,
        "y": 538.5,
        "w": 117
      },
      "Wojciech Kosowicz (wojak)": {
        "x": 2167.5,
        "y": 453.5,
        "w": 81
      },
      "72 ()": {
        "x": 2125,
        "y": 441,
        "w": 60
      },
      "Clemens Toennies (starbuck)": {
        "x": 2069,
        "y": 393,
        "w": 84
      },
      "Daniel Izquierdo": {
        "x": 1997,
        "y": 419,
        "w": 114
      },
      "Kai Uwe Broulik (kbroulik)": {
        "x": 1997,
        "y": 558,
        "w": 126
      },
      "76 ()": {
        "x": 1993,
        "y": 679,
        "w": 120
      },
      "77 ()": {
        "x": 1825,
        "y": 668,
        "w": 138
      },
      "Rohan Garg (shadeslayer)": {
        "x": 1808.5,
        "y": 554.5,
        "w": 129
      },
      "Jose Millan Soto (fid_jose)": {
        "x": 1859,
        "y": 472,
        "w": 108
      },
      "Rui Dias (TheInvisible)": {
        "x": 1931.5,
        "y": 378.5,
        "w": 99
      },
      "Àlex Fiestas (afiestas)": {
        "x": 1920.5,
        "y": 818.5,
        "w": 135
      },
      "Vishesh Handa (vHanda)": {
        "x": 1756,
        "y": 833,
        "w": 120
      },
      "Timothée Giet": {
        "x": 1644,
        "y": 799,
        "w": 126
      },
      "Jos Poortvliet (jospoortvliet:)": {
        "x": 1682.5,
        "y": 698.5,
        "w": 135
      },
      "Luciano Montanaro (mikelima)": {
        "x": 1664,
        "y": 598,
        "w": 126
      },
      "Vladimir Peric ()": {
        "x": 1695.5,
        "y": 485.5,
        "w": 111
      },
      "Marta Rybczyńska ()": {
        "x": 1776,
        "y": 418,
        "w": 102
      },
      "Jaroslav Řezník (jreznik)": {
        "x": 1697.5,
        "y": 404.5,
        "w": 111
      },
      "Camila Ayres (camilasan)": {
        "x": 1589,
        "y": 706,
        "w": 114
      },
      "Remi Benoit": {
        "x": 1523.5,
        "y": 773.5,
        "w": 117
      },
      "Franck Arrecot (Knarf)": {
        "x": 1393.5,
        "y": 834.5,
        "w": 111
      },
      "Milian Wolff (milian)": {
        "x": 1442.5,
        "y": 709.5,
        "w": 123
      },
      "Shinjo Park (pereman)": {
        "x": 1505,
        "y": 507,
        "w": 132
      },
      "Simeon Kuran": {
        "x": 1605,
        "y": 394,
        "w": 102
      },
      "Ladislav Hagara ()": {
        "x": 1553,
        "y": 432,
        "w": 90
      },
      "Gunnar Schmidt": {
        "x": 1475.5,
        "y": 432.5,
        "w": 105
      },
      "97 ()": {
        "x": 1417,
        "y": 433,
        "w": 72
      },
      "98 ()": {
        "x": 1342.5,
        "y": 400.5,
        "w": 99
      },
      "David Faure (dfaure)": {
        "x": 1349,
        "y": 467,
        "w": 126
      },
      "Heinz Wiesinger (pprkut)": {
        "x": 1338,
        "y": 642,
        "w": 114
      },
      "Martin Konold ()": {
        "x": 1244.5,
        "y": 750.5,
        "w": 141
      },
      "Kevin Kofler (Kevin_Kofler)": {
        "x": 1084,
        "y": 814,
        "w": 126
      },
      "Aleix Pol Gonzalez (apol)": {
        "x": 1122,
        "y": 695,
        "w": 126
      },
      "Andreas Kainz (andreas_k)": {
        "x": 1175.5,
        "y": 613.5,
        "w": 117
      },
      "Adriaan de Groot ([ade])": {
        "x": 1229,
        "y": 512,
        "w": 114
      },
      "Sune Vuorela (svuorela)": {
        "x": 1214.5,
        "y": 442.5,
        "w": 99
      },
      "Stefan Derkits (HorusHorrendus)": {
        "x": 1174.5,
        "y": 420.5,
        "w": 75
      },
      "Paul Adams (padams)": {
        "x": 1086.5,
        "y": 500.5,
        "w": 99
      },
      "Kevin Ottens (ervin)": {
        "x": 1070,
        "y": 452,
        "w": 72
      },
      "Ignaz Forster (fos)": {
        "x": 978,
        "y": 593,
        "w": 144
      },
      "Stephanie Lerotic ()": {
        "x": 866.5,
        "y": 590.5,
        "w": 123
      },
      "Cornelius Schumacher": {
        "x": 879,
        "y": 801,
        "w": 144
      },
      "Valentin Rusu (valir)": {
        "x": 691.5,
        "y": 818.5,
        "w": 129
      },
      "Sebastian Kügler (sebas)": {
        "x": 686,
        "y": 741,
        "w": 114
      },
      "Björn Balazs ()": {
        "x": 531,
        "y": 844,
        "w": 126
      },
      "Andrew Lake ()": {
        "x": 482,
        "y": 756,
        "w": 126
      },
      "Harald Sitter (apachelogger)": {
        "x": 320.5,
        "y": 847.5,
        "w": 129
      },
      "Bhushan Shah (bshah)": {
        "x": 161.5,
        "y": 878.5,
        "w": 117
      },
      "119 ()": {
        "x": 301,
        "y": 805,
        "w": 78
      },
      "Thomas Pfeiffer ()": {
        "x": 352,
        "y": 723,
        "w": 120
      },
      "Valorie Zimmerman (valorie)": {
        "x": 558.5,
        "y": 697.5,
        "w": 117
      },
      "Teo Mrnjavac ()": {
        "x": 751.5,
        "y": 599.5,
        "w": 135
      },
      "Patrick von Reth (TheOneRing)": {
        "x": 596.5,
        "y": 555.5,
        "w": 123
      },
      "124 ()": {
        "x": 1014,
        "y": 496,
        "w": 96
      },
      "Sebastien Renard (renards)": {
        "x": 1004,
        "y": 395,
        "w": 108
      },
      "Benjamin Port (ben2367)": {
        "x": 946,
        "y": 422,
        "w": 66
      },
      "Mikey Ariel  (thatdocslady)": {
        "x": 923,
        "y": 462,
        "w": 96
      },
      "David Edmundson (d_ed)": {
        "x": 766.5,
        "y": 537.5,
        "w": 93
      },
      "Dominik Schmidt (domme)": {
        "x": 860,
        "y": 449,
        "w": 72
      },
      "Daniel Vrátil (dvratil)": {
        "x": 816.5,
        "y": 427.5,
        "w": 69
      },
      "Jiri Konecny (DragonLich)": {
        "x": 766.5,
        "y": 453.5,
        "w": 81
      },
      "Kevin Krammer (krake)": {
        "x": 687.5,
        "y": 518.5,
        "w": 93
      },
      "133 ()": {
        "x": 687,
        "y": 449,
        "w": 96
      },
      "Fabio D'Urso (fabiod)": {
        "x": 606.5,
        "y": 447.5,
        "w": 111
      },
      "135 ()": {
        "x": 507.5,
        "y": 508.5,
        "w": 111
      },
      "Kevin Funk (kfunk)": {
        "x": 437.5,
        "y": 564.5,
        "w": 123
      },
      "Martin Klapetek (mck182)": {
        "x": 287.5,
        "y": 609.5,
        "w": 129
      },
      "Georgia Young (geographic)": {
        "x": 297,
        "y": 532,
        "w": 114
      },
      "Dong Siyuan (song)": {
        "x": 359.5,
        "y": 470.5,
        "w": 111
      },
      "Krzysztof Staniorowski (ksx4system)": {
        "x": 506.5,
        "y": 408.5,
        "w": 105
      },
      "141 ()": {
        "x": 4421.5,
        "y": 380.5,
        "w": 75
      }
    },
    "photo": "2014/groupphoto/akademy2014_group_photo.png"
  },
  "2015": {
    "people": {
      "Andrew Lake": {
        "x": 355,
        "y": 1451,
        "w": 240
      },
      "Thomas Pfeiffer (colomar)": {
        "x": 731,
        "y": 1453,
        "w": 222
      },
      "Jens Reuterberg ()": {
        "x": 1020.5,
        "y": 1436.5,
        "w": 267
      },
      "Peter Bouda (pbouda)": {
        "x": 1342,
        "y": 1368,
        "w": 228
      },
      "Alex Merry (alexmerry)": {
        "x": 1588.5,
        "y": 1451.5,
        "w": 255
      },
      "Scarlett Clark (sgclark)": {
        "x": 1857,
        "y": 1480,
        "w": 228
      },
      "Jonathan Riddell (Riddell)": {
        "x": 2093.5,
        "y": 1386.5,
        "w": 213
      },
      "8 ()": {
        "x": 2354,
        "y": 1404,
        "w": 222
      },
      "Sanjiban Bairagya (fewcha)": {
        "x": 2589.5,
        "y": 1362.5,
        "w": 249
      },
      "Vishesh Handa (vHanda)": {
        "x": 3361,
        "y": 1333,
        "w": 102
      },
      "Riccardo Iaconelli (ruphy)": {
        "x": 3218.5,
        "y": 1337.5,
        "w": 117
      },
      "Frederik Gladhorn (fregl)": {
        "x": 3110.5,
        "y": 1326.5,
        "w": 111
      },
      "Jigar Rainsinghani (jigar)": {
        "x": 2924,
        "y": 1367,
        "w": 174
      },
      "David Faure (dfaure)": {
        "x": 3010.5,
        "y": 1329.5,
        "w": 87
      },
      "Yash Shah ()": {
        "x": 3380.5,
        "y": 1165.5,
        "w": 93
      },
      "Lydia Pintscher (Nightrose)": {
        "x": 3272.5,
        "y": 1179.5,
        "w": 93
      },
      "Marco Martin (notmart)": {
        "x": 3162,
        "y": 1166,
        "w": 102
      },
      "Kenny Duffus (seaLne)": {
        "x": 3163,
        "y": 1091,
        "w": 90
      },
      "Davide Bettio ()": {
        "x": 3022,
        "y": 1158,
        "w": 108
      },
      "Silvia Montanaro ()": {
        "x": 3029.5,
        "y": 1089.5,
        "w": 81
      },
      "Luca Novello ()": {
        "x": 2931,
        "y": 1076,
        "w": 90
      },
      "Sebastien Renard (renards)": {
        "x": 2889.5,
        "y": 1158.5,
        "w": 93
      },
      "Franck Arrecot (knarf)": {
        "x": 2784.5,
        "y": 1164.5,
        "w": 99
      },
      "Luciano Montanaro (mikelima)": {
        "x": 2748,
        "y": 1077,
        "w": 96
      },
      "Benjamin Port (ben2367)": {
        "x": 2665,
        "y": 1170,
        "w": 90
      },
      "Thomas Maltri-Wenninger": {
        "x": 2677,
        "y": 1081,
        "w": 54
      },
      "Joseph Wenninger  (jowenn)": {
        "x": 2617.5,
        "y": 1012.5,
        "w": 81
      },
      "Stefan Derkits (HorusHorrendus)": {
        "x": 2583,
        "y": 1069,
        "w": 90
      },
      "Cornelius Schumacher ()": {
        "x": 2495,
        "y": 1010,
        "w": 90
      },
      "30 ()": {
        "x": 2482.5,
        "y": 917.5,
        "w": 75
      },
      "31 ()": {
        "x": 2408,
        "y": 926,
        "w": 60
      },
      "Clemens Toennies ()": {
        "x": 2302.5,
        "y": 927.5,
        "w": 51
      },
      "33 ()": {
        "x": 2339.5,
        "y": 927.5,
        "w": 57
      },
      "Valentin Rusu (valir)": {
        "x": 2501,
        "y": 1167,
        "w": 96
      },
      "Leo Savernik (leo)": {
        "x": 2439,
        "y": 1052,
        "w": 84
      },
      "Aleix Pol Gonzalez (apol)": {
        "x": 2358,
        "y": 1159,
        "w": 96
      },
      "Kenny Coyle (automatical)": {
        "x": 2383.5,
        "y": 1031.5,
        "w": 75
      },
      "Claus Christensen ()": {
        "x": 2326.5,
        "y": 1021.5,
        "w": 81
      },
      "Franklin Weng": {
        "x": 2273,
        "y": 1069,
        "w": 84
      },
      "Jan Grulich ()": {
        "x": 2207.5,
        "y": 1017.5,
        "w": 81
      },
      "Emile de Weerd (mederel)": {
        "x": 2162,
        "y": 1098,
        "w": 78
      },
      "Jose Millan Soto (fid_jose)": {
        "x": 2229.5,
        "y": 1169.5,
        "w": 87
      },
      "Lamarque Souza (lamarque)": {
        "x": 2072,
        "y": 1251,
        "w": 90
      },
      "Marcos Chavarria Teijeiro (chavaone)": {
        "x": 2072,
        "y": 1175,
        "w": 78
      },
      "Ali Moreira": {
        "x": 1993.5,
        "y": 1172.5,
        "w": 87
      },
      "Sofia Prosper": {
        "x": 2045.5,
        "y": 1137.5,
        "w": 69
      },
      "47 ()": {
        "x": 2107,
        "y": 1074,
        "w": 66
      },
      "Kai Uwe Broulik ()": {
        "x": 2157.5,
        "y": 1079.5,
        "w": 39
      },
      "Jure Repinc (JLP)": {
        "x": 2174,
        "y": 927,
        "w": 66
      },
      "Jean-Baptiste Mardelle (jbm)": {
        "x": 2138,
        "y": 945,
        "w": 54
      },
      "Irina Rempt (irina)": {
        "x": 2099,
        "y": 966,
        "w": 60
      },
      "Andreas Cord-Landwehr  (CoLa)": {
        "x": 2048.5,
        "y": 1055.5,
        "w": 75
      },
      "Shinjo Park (peremen)": {
        "x": 1986,
        "y": 1080,
        "w": 78
      },
      "Douglas DeMaio (ddemaio)": {
        "x": 1819.5,
        "y": 1219.5,
        "w": 111
      },
      "Nuno Pinheiro ()": {
        "x": 1875.5,
        "y": 1133.5,
        "w": 87
      },
      "Marcos Lopez (feirlane)": {
        "x": 1797.5,
        "y": 1152.5,
        "w": 75
      },
      "Helio Castro (heliocastro)": {
        "x": 1800,
        "y": 1110,
        "w": 54
      },
      "Sebastian Kügler (sebas)": {
        "x": 1847,
        "y": 1066,
        "w": 78
      },
      "Boudewĳn Rempt (boud)": {
        "x": 1993.5,
        "y": 928.5,
        "w": 75
      },
      "Volker Krause (vkrause)": {
        "x": 1914,
        "y": 908,
        "w": 72
      },
      "Raoul Bogdan": {
        "x": 1937,
        "y": 1033,
        "w": 66
      },
      "Bongsang Kim": {
        "x": 1895,
        "y": 1007,
        "w": 60
      },
      "Kevin Ottens (ervin)": {
        "x": 1849.5,
        "y": 955.5,
        "w": 63
      },
      "Harald Sitter (sitter)": {
        "x": 1851.5,
        "y": 1009.5,
        "w": 57
      },
      "65 ()": {
        "x": 1813,
        "y": 1004,
        "w": 54
      },
      "Ovidiu-Florin Bogdan (ovidiu-florin)": {
        "x": 1768,
        "y": 1001,
        "w": 54
      },
      "Matthias Kirschner": {
        "x": 1158.5,
        "y": 896.5,
        "w": 69
      },
      "Sandro Andrade": {
        "x": 1694.5,
        "y": 1040.5,
        "w": 99
      },
      "Santiago Saavedra Lopez (ssice)": {
        "x": 1695,
        "y": 1143,
        "w": 102
      },
      "Eva Romero (lilaindefinida)": {
        "x": 1644,
        "y": 960,
        "w": 72
      },
      "Antonio Larrosa Jimenez (antlarr)": {
        "x": 1595.5,
        "y": 940.5,
        "w": 69
      },
      "Jos van den Oever (vandenoever)": {
        "x": 1561,
        "y": 1044,
        "w": 78
      },
      "Matthias Klumpp (ximion)": {
        "x": 1443.5,
        "y": 1064.5,
        "w": 81
      },
      "Sebastian Gruener": {
        "x": 1498.5,
        "y": 939.5,
        "w": 69
      },
      "75 ()": {
        "x": 1442.5,
        "y": 964.5,
        "w": 57
      },
      "Christian Mollekopf (cmollekopf)": {
        "x": 1392,
        "y": 965,
        "w": 66
      },
      "Martin Gräßlin (mgraesslin)": {
        "x": 1346,
        "y": 1061,
        "w": 78
      },
      "Jan Kundrat (jkt)": {
        "x": 1305,
        "y": 904,
        "w": 72
      },
      "Minh Ngo (nlminhtl)": {
        "x": 1232,
        "y": 892,
        "w": 66
      },
      "Matija Suklje (silver_hook)": {
        "x": 1162,
        "y": 897,
        "w": 66
      },
      "Christoph Cullmann": {
        "x": 1209.5,
        "y": 934.5,
        "w": 81
      },
      "Dominik Haumann (dhaumann)": {
        "x": 1149.5,
        "y": 955.5,
        "w": 75
      },
      "Matej Laitl (strohel)": {
        "x": 1114.5,
        "y": 872.5,
        "w": 63
      },
      "Pablo Castro Valiño (castrinho8)": {
        "x": 1626,
        "y": 1183,
        "w": 84
      },
      "Adrián Pereira Guarra": {
        "x": 1562,
        "y": 1284,
        "w": 108
      },
      "Pedro Costal (Pedrety)": {
        "x": 1557,
        "y": 1152,
        "w": 96
      },
      "Chema Casanova": {
        "x": 1472.5,
        "y": 1130.5,
        "w": 99
      },
      "Ignacio Serantes (iserantes)": {
        "x": 1426,
        "y": 1184,
        "w": 108
      },
      "Baltasar Ortega (baltolkien)": {
        "x": 1388,
        "y": 1263,
        "w": 96
      },
      "Juan Carlos": {
        "x": 1256.5,
        "y": 1265.5,
        "w": 105
      },
      "Vadim Trochinsky": {
        "x": 1302,
        "y": 1145,
        "w": 90
      },
      "Àlex Fiestas (afiestas)": {
        "x": 1189,
        "y": 1184,
        "w": 114
      },
      "Daniel Vrátil (dvratil)": {
        "x": 1108,
        "y": 1226,
        "w": 114
      },
      "Albert Astals Cid (tsdgeos)": {
        "x": 1073.5,
        "y": 1178.5,
        "w": 93
      },
      "Ashish Bansal(mrphantom)": {
        "x": 1105.5,
        "y": 1090.5,
        "w": 93
      },
      "Devaja Shah (devaja)": {
        "x": 1012,
        "y": 1149,
        "w": 96
      },
      "Luigi Toscano (tosky)": {
        "x": 927.5,
        "y": 1221.5,
        "w": 99
      },
      "99 ()": {
        "x": 863.5,
        "y": 1180.5,
        "w": 105
      },
      "Rohan Garg (shadeslayer)": {
        "x": 755,
        "y": 1209,
        "w": 114
      },
      "Jose Manuel Santamaria (santa)": {
        "x": 587.5,
        "y": 1172.5,
        "w": 87
      },
      "Gunnar Schmidt": {
        "x": 1025.5,
        "y": 875.5,
        "w": 87
      },
      "Ingo Klöcker": {
        "x": 945,
        "y": 856,
        "w": 78
      },
      "Rajeesh K Nambiar (knambiar)": {
        "x": 860.5,
        "y": 853.5,
        "w": 81
      },
      "Milian Wolff ()": {
        "x": 915,
        "y": 997,
        "w": 90
      },
      "Allan Sandfeld Jensen (carewolf)": {
        "x": 888,
        "y": 1088,
        "w": 114
      },
      "Max Harmathy ()": {
        "x": 809.5,
        "y": 1076.5,
        "w": 99
      },
      "Tobias Fischbach ()": {
        "x": 707.5,
        "y": 1086.5,
        "w": 81
      },
      "Sandro Knauß (hefee)": {
        "x": 763.5,
        "y": 1010.5,
        "w": 87
      },
      "Bruno Coudoin (bdoin)": {
        "x": 666,
        "y": 1038,
        "w": 72
      },
      "Zohra Coudoin (zohra)": {
        "x": 607.5,
        "y": 1057.5,
        "w": 87
      },
      "Valorie Zimmerman (valorie)": {
        "x": 517,
        "y": 1124,
        "w": 102
      },
      "Aaron Honeycutt (ahoneybun)": {
        "x": 433,
        "y": 1091,
        "w": 96
      },
      "Claudio Desideri (snitzzo)": {
        "x": 259.5,
        "y": 1112.5,
        "w": 111
      },
      "Francesco Wofford": {
        "x": 152.5,
        "y": 1176.5,
        "w": 99
      },
      "Rouald v Schlegell": {
        "x": 675,
        "y": 880,
        "w": 78
      },
      "Rui Dias (TheInvisible)": {
        "x": 557.5,
        "y": 892.5,
        "w": 81
      },
      "Timothée Giet (Animtim)": {
        "x": 352,
        "y": 966,
        "w": 60
      },
      "Dan Leinir Turthra Jensen (leinir)": {
        "x": 200,
        "y": 976,
        "w": 66
      },
      "Victor Blázquez (vblazquez)": {
        "x": 300,
        "y": 899,
        "w": 60
      },
      "David Edmundson (d_ed)": {
        "x": 171,
        "y": 891,
        "w": 60
      }
    },
    "photo": "2015/groupphoto/akademy2015.jpg"
  },
  "2016": {
    "people": {
      "XXX 8": {
        "x": 5835.5,
        "y": 478.5,
        "w": 87
      },
      "Alessandro Portale": {
        "x": 5551,
        "y": 346,
        "w": 144
      },
      "XXX 10": {
        "x": 5587.5,
        "y": 498.5,
        "w": 117
      },
      "Oswald Buddenhagen": {
        "x": 5438,
        "y": 519,
        "w": 114
      },
      "Petra Gillert": {
        "x": 5390,
        "y": 584,
        "w": 168
      },
      "Devaja Shah": {
        "x": 5283,
        "y": 581,
        "w": 114
      },
      "Bhushan Shah (bshah)": {
        "x": 5188.5,
        "y": 585.5,
        "w": 117
      },
      "Sandro Andrade": {
        "x": 5143,
        "y": 520,
        "w": 120
      },
      "XXX 16": {
        "x": 5301,
        "y": 477,
        "w": 120
      },
      "XXX 17": {
        "x": 5730,
        "y": 297,
        "w": 132
      },
      "XXX 18": {
        "x": 5626,
        "y": 277,
        "w": 102
      },
      "XXX 19": {
        "x": 5502,
        "y": 293,
        "w": 108
      },
      "XXX 20": {
        "x": 5564,
        "y": 62,
        "w": 120
      },
      "Frank Karlitschek": {
        "x": 5484.5,
        "y": 44.5,
        "w": 105
      },
      "XXX 22": {
        "x": 5459,
        "y": 267,
        "w": 96
      },
      "XXX 23": {
        "x": 5377,
        "y": 320,
        "w": 114
      },
      "Michael Brühning": {
        "x": 5365,
        "y": 260,
        "w": 96
      },
      "Lamarque Souza": {
        "x": 5268,
        "y": 358,
        "w": 96
      },
      "Filipe Saraiva": {
        "x": 5190,
        "y": 366,
        "w": 96
      },
      "Simon Peter": {
        "x": 5255,
        "y": 268,
        "w": 108
      },
      "Scarlett Clark": {
        "x": 5178.5,
        "y": 213.5,
        "w": 75
      },
      "XXX 29": {
        "x": 5334,
        "y": 171,
        "w": 78
      },
      "Fredrik de Vibe": {
        "x": 5391,
        "y": 153,
        "w": 90
      },
      "XXX 31": {
        "x": 5392,
        "y": 68,
        "w": 102
      },
      "Max Mehl": {
        "x": 5310.5,
        "y": 33.5,
        "w": 93
      },
      "XXX 33": {
        "x": 5248,
        "y": 58,
        "w": 102
      },
      "XXX 34": {
        "x": 5221,
        "y": 133,
        "w": 96
      },
      "XXX 35": {
        "x": 5221,
        "y": 75,
        "w": 72
      },
      "XXX 36": {
        "x": 5187,
        "y": 111,
        "w": 66
      },
      "XXX 37": {
        "x": 5181,
        "y": 57,
        "w": 72
      },
      "XXX 38": {
        "x": 5139.5,
        "y": 69.5,
        "w": 93
      },
      "XXX 39": {
        "x": 5085.5,
        "y": 29.5,
        "w": 87
      },
      "XXX 40": {
        "x": 5085.5,
        "y": 314.5,
        "w": 75
      },
      "Aracele Torres": {
        "x": 5077.5,
        "y": 368.5,
        "w": 105
      },
      "Kåre Särs": {
        "x": 5060.5,
        "y": 560.5,
        "w": 111
      },
      "Dominik Haumann": {
        "x": 4946,
        "y": 547,
        "w": 114
      },
      "XXX 44": {
        "x": 4903.5,
        "y": 509.5,
        "w": 87
      },
      "XXX 45": {
        "x": 4966.5,
        "y": 305.5,
        "w": 87
      },
      "XXX 46": {
        "x": 5058,
        "y": 137,
        "w": 78
      },
      "XXX 47": {
        "x": 5032,
        "y": 100,
        "w": 78
      },
      "XXX 48": {
        "x": 5024,
        "y": 50,
        "w": 72
      },
      "Tero Kojo": {
        "x": 4959.5,
        "y": 46.5,
        "w": 93
      },
      "XXX 50": {
        "x": 4889.5,
        "y": 31.5,
        "w": 87
      },
      "Michael Bohlender": {
        "x": 4928.5,
        "y": 43.5,
        "w": 75
      },
      "Rohan Garg (shadeslayer)": {
        "x": 4919,
        "y": 106,
        "w": 84
      },
      "Luigi Toscano (tosky)": {
        "x": 4872.5,
        "y": 107.5,
        "w": 69
      },
      "XXX 54": {
        "x": 4855,
        "y": 77,
        "w": 66
      },
      "Ivan Čukić": {
        "x": 4810.5,
        "y": 41.5,
        "w": 93
      },
      "Irene Cortinovis (pipsini)": {
        "x": 4853.5,
        "y": 332.5,
        "w": 93
      },
      "Dan Leinir Turthra Jensen (leiner)": {
        "x": 4830,
        "y": 259,
        "w": 84
      },
      "Harald Sitter (sitter)": {
        "x": 4797,
        "y": 102,
        "w": 78
      },
      "Hannah von Reth (TheOneRing)": {
        "x": 4767.5,
        "y": 83.5,
        "w": 69
      },
      "XXX 60": {
        "x": 4763,
        "y": 48,
        "w": 42
      },
      "Cornelius Schumacher": {
        "x": 4739,
        "y": 78,
        "w": 54
      },
      "XXX 62": {
        "x": 4695,
        "y": 81,
        "w": 72
      },
      "Kenny Duffus (seaLne)": {
        "x": 4737.5,
        "y": 123.5,
        "w": 87
      },
      "Patricia Oniga": {
        "x": 4687.5,
        "y": 170.5,
        "w": 75
      },
      "XXX 65": {
        "x": 4785.5,
        "y": 341.5,
        "w": 75
      },
      "Bernhard Beschow": {
        "x": 4854,
        "y": 505,
        "w": 78
      },
      "Christoph Cullmann": {
        "x": 4806.5,
        "y": 574.5,
        "w": 87
      },
      "XXX 68": {
        "x": 4747,
        "y": 498,
        "w": 84
      },
      "XXX 69": {
        "x": 4671.5,
        "y": 325.5,
        "w": 105
      },
      "Jan Murawski": {
        "x": 4670,
        "y": 573,
        "w": 84
      },
      "XXX 71": {
        "x": 4598,
        "y": 570,
        "w": 96
      },
      "XXX 72": {
        "x": 4601,
        "y": 345,
        "w": 66
      },
      "Mario Koppensteiner": {
        "x": 4630,
        "y": 230,
        "w": 84
      },
      "XXX 74": {
        "x": 4607,
        "y": 128,
        "w": 72
      },
      "Stefan Derkits (HorusHorrendus)": {
        "x": 4652,
        "y": 84,
        "w": 72
      },
      "XXX 76": {
        "x": 4620,
        "y": 70,
        "w": 72
      },
      "Mario Fux (unormal)": {
        "x": 4584,
        "y": 47,
        "w": 72
      },
      "XXX 78": {
        "x": 4504.5,
        "y": 80.5,
        "w": 45
      },
      "XXX 79": {
        "x": 4517,
        "y": 78,
        "w": 60
      },
      "XXX 80": {
        "x": 4544,
        "y": 81,
        "w": 72
      },
      "Cristian Baldi (crisbal)": {
        "x": 4514,
        "y": 124,
        "w": 60
      },
      "XXX 82": {
        "x": 4539.5,
        "y": 165.5,
        "w": 75
      },
      "Simon Hornbacher": {
        "x": 4534,
        "y": 219,
        "w": 72
      },
      "XXX 84": {
        "x": 4500.5,
        "y": 592.5,
        "w": 87
      },
      "Jean-Baptiste Mardelle": {
        "x": 4499,
        "y": 340,
        "w": 90
      },
      "XXX 86": {
        "x": 4443.5,
        "y": 212.5,
        "w": 93
      },
      "Shinjo Park (peremen)": {
        "x": 4459.5,
        "y": 132.5,
        "w": 75
      },
      "Jos van den Oever (vandenoever)": {
        "x": 4465,
        "y": 78,
        "w": 66
      },
      "XXX 89": {
        "x": 4421,
        "y": 76,
        "w": 66
      },
      "XXX 90": {
        "x": 4379.5,
        "y": 66.5,
        "w": 63
      },
      "XXX 91": {
        "x": 4408.5,
        "y": 103.5,
        "w": 51
      },
      "Ingo Klöcker (mahoutsukai)": {
        "x": 4402.5,
        "y": 140.5,
        "w": 57
      },
      "Johannes Zarl-Zierl": {
        "x": 4379.5,
        "y": 187.5,
        "w": 93
      },
      "XXX 94": {
        "x": 4364,
        "y": 277,
        "w": 72
      },
      "Ovidiu-Florin Bogdan (ovidiuflorin)": {
        "x": 4430.5,
        "y": 407.5,
        "w": 87
      },
      "Lars Schmertmann": {
        "x": 4372,
        "y": 587,
        "w": 90
      },
      "Helio Castro": {
        "x": 4201,
        "y": 564,
        "w": 96
      },
      "Sandro Knauss (hefee)": {
        "x": 4243.5,
        "y": 399.5,
        "w": 99
      },
      "Riccardo Iaconelli (ruphy)": {
        "x": 4335.5,
        "y": 345.5,
        "w": 81
      },
      "Albert Astals Cid (tsdgeos)": {
        "x": 4293,
        "y": 314,
        "w": 78
      },
      "XXX 101": {
        "x": 4233.5,
        "y": 344.5,
        "w": 81
      },
      "XXX 102": {
        "x": 4172,
        "y": 343,
        "w": 78
      },
      "Matthias Kirschner": {
        "x": 4208,
        "y": 236,
        "w": 84
      },
      "XXX 104": {
        "x": 4142,
        "y": 229,
        "w": 84
      },
      "XXX 105": {
        "x": 4296,
        "y": 163,
        "w": 84
      },
      "Nicolas Dietrich (nidi)": {
        "x": 4302,
        "y": 116,
        "w": 60
      },
      "Zoltan Padrah": {
        "x": 4349.5,
        "y": 97.5,
        "w": 45
      },
      "XXX 108": {
        "x": 4296.5,
        "y": 57.5,
        "w": 75
      },
      "XXX 109": {
        "x": 4271.5,
        "y": 91.5,
        "w": 45
      },
      "XXX 110": {
        "x": 4246,
        "y": 125,
        "w": 72
      },
      "XXX 111": {
        "x": 4214.5,
        "y": 95.5,
        "w": 75
      },
      "XXX 112": {
        "x": 4190,
        "y": 138,
        "w": 54
      },
      "XXX 113": {
        "x": 4177.5,
        "y": 83.5,
        "w": 69
      },
      "XXX 114": {
        "x": 4134.5,
        "y": 100.5,
        "w": 69
      },
      "XXX 115": {
        "x": 4092,
        "y": 110,
        "w": 66
      },
      "XXX 116": {
        "x": 4107.5,
        "y": 62.5,
        "w": 63
      },
      "XXX 117": {
        "x": 4068.5,
        "y": 55.5,
        "w": 63
      },
      "XXX 118": {
        "x": 4065,
        "y": 109,
        "w": 66
      },
      "XXX 119": {
        "x": 4020,
        "y": 112,
        "w": 60
      },
      "Thomas Doczkal": {
        "x": 4035.5,
        "y": 93.5,
        "w": 57
      },
      "Frederik Gladhorn (fregl)": {
        "x": 4068,
        "y": 246,
        "w": 78
      },
      "XXX 122": {
        "x": 4011,
        "y": 281,
        "w": 84
      },
      "XXX 123": {
        "x": 4117.5,
        "y": 365.5,
        "w": 75
      },
      "Myriam Schweingruber (Mamarok)": {
        "x": 4061.5,
        "y": 445.5,
        "w": 111
      },
      "Tuukka Turunen": {
        "x": 4034.5,
        "y": 554.5,
        "w": 111
      },
      "XXX 126": {
        "x": 3933.5,
        "y": 605.5,
        "w": 87
      },
      "Italo Vignoli": {
        "x": 3929.5,
        "y": 374.5,
        "w": 111
      },
      "Max Harmathy": {
        "x": 3949,
        "y": 296,
        "w": 84
      },
      "Michael Weghorn": {
        "x": 3865,
        "y": 296,
        "w": 84
      },
      "XXX 130": {
        "x": 3965,
        "y": 161,
        "w": 72
      },
      "Daniel Vrátil (dvratil)l": {
        "x": 3976.5,
        "y": 98.5,
        "w": 57
      },
      "David Faure (dfaure)": {
        "x": 3925,
        "y": 70,
        "w": 72
      },
      "XXX 133": {
        "x": 3912,
        "y": 123,
        "w": 72
      },
      "Robert Lönning": {
        "x": 3885,
        "y": 66,
        "w": 72
      },
      "Volker Krause": {
        "x": 3861,
        "y": 70,
        "w": 48
      },
      "XXX 136": {
        "x": 3844.5,
        "y": 98.5,
        "w": 39
      },
      "XXX 137": {
        "x": 3834,
        "y": 122,
        "w": 36
      },
      "XXX 138": {
        "x": 3818,
        "y": 117,
        "w": 36
      },
      "Andy Nichols": {
        "x": 3824.5,
        "y": 138.5,
        "w": 81
      },
      "XXX 140": {
        "x": 3799,
        "y": 337,
        "w": 96
      },
      "Simon Hausmann": {
        "x": 3758.5,
        "y": 573.5,
        "w": 81
      },
      "XXX 142": {
        "x": 3664.5,
        "y": 368.5,
        "w": 99
      },
      "Jędrzej Nowacki": {
        "x": 3728,
        "y": 318,
        "w": 78
      },
      "Dennis Knorr": {
        "x": 3766,
        "y": 307,
        "w": 66
      },
      "Gunnar Schmidt": {
        "x": 3765,
        "y": 252,
        "w": 66
      },
      "Mirko Böhm (mirko)": {
        "x": 3773,
        "y": 115,
        "w": 66
      },
      "XXX 147": {
        "x": 3751,
        "y": 94,
        "w": 54
      },
      "XXX 148": {
        "x": 3727,
        "y": 85,
        "w": 42
      },
      "XXX 149": {
        "x": 3703,
        "y": 105,
        "w": 42
      },
      "XXX 150": {
        "x": 3649.5,
        "y": 84.5,
        "w": 75
      },
      "XXX 151": {
        "x": 3614,
        "y": 84,
        "w": 60
      },
      "Allan Sandfeld Jensen (carewolf)": {
        "x": 3579.5,
        "y": 112.5,
        "w": 63
      },
      "XXX 153": {
        "x": 3692.5,
        "y": 142.5,
        "w": 69
      },
      "XXX 154": {
        "x": 3614.5,
        "y": 246.5,
        "w": 93
      },
      "Tobias Fischbach": {
        "x": 3579,
        "y": 296,
        "w": 84
      },
      "Pradeepto Bhattacharya": {
        "x": 3573,
        "y": 474,
        "w": 78
      },
      "Boudhayan Gupta (BaloneyGeek)": {
        "x": 3619.5,
        "y": 648.5,
        "w": 93
      },
      "XXX 158": {
        "x": 3453,
        "y": 621,
        "w": 102
      },
      "XXX 159": {
        "x": 3501.5,
        "y": 397.5,
        "w": 51
      },
      "Alexander Giss": {
        "x": 3446.5,
        "y": 382.5,
        "w": 81
      },
      "XXX 161": {
        "x": 3471.5,
        "y": 246.5,
        "w": 93
      },
      "Shawn Rutledge": {
        "x": 3423.5,
        "y": 302.5,
        "w": 81
      },
      "XXX 163": {
        "x": 3563,
        "y": 140,
        "w": 36
      },
      "XXX 164": {
        "x": 3547,
        "y": 104,
        "w": 42
      },
      "Laszlo Agocs": {
        "x": 3507.5,
        "y": 116.5,
        "w": 57
      },
      "XXX 166": {
        "x": 3485,
        "y": 93,
        "w": 48
      },
      "XXX 167": {
        "x": 3445,
        "y": 120,
        "w": 84
      },
      "Friedemann Kleint": {
        "x": 3423.5,
        "y": 117.5,
        "w": 57
      },
      "XXX 169": {
        "x": 3390.5,
        "y": 96.5,
        "w": 63
      },
      "XXX 170": {
        "x": 3383,
        "y": 140,
        "w": 54
      },
      "XXX 171": {
        "x": 3327,
        "y": 668,
        "w": 96
      },
      "Alexander Anokhin": {
        "x": 3326,
        "y": 383,
        "w": 84
      },
      "Richard Moore (richmoore)": {
        "x": 3273.5,
        "y": 363.5,
        "w": 87
      },
      "XXX 174": {
        "x": 3309.5,
        "y": 243.5,
        "w": 87
      },
      "XXX 175": {
        "x": 3235,
        "y": 303,
        "w": 66
      },
      "XXX 176": {
        "x": 3344.5,
        "y": 85.5,
        "w": 75
      },
      "XXX 177": {
        "x": 3330.5,
        "y": 123.5,
        "w": 75
      },
      "XXX 178": {
        "x": 3297,
        "y": 134,
        "w": 60
      },
      "Gilbert Assaf": {
        "x": 3256.5,
        "y": 124.5,
        "w": 69
      },
      "Antonio Larrosa Jimenez": {
        "x": 3241.5,
        "y": 73.5,
        "w": 69
      },
      "XXX 181": {
        "x": 3240,
        "y": 122,
        "w": 48
      },
      "XXX 182": {
        "x": 3187,
        "y": 134,
        "w": 78
      },
      "Douglas DeMaio": {
        "x": 3158,
        "y": 128,
        "w": 72
      },
      "XXX 184": {
        "x": 3165.5,
        "y": 91.5,
        "w": 45
      },
      "XXX 185": {
        "x": 3110,
        "y": 117,
        "w": 42
      },
      "XXX 186": {
        "x": 3133,
        "y": 128,
        "w": 42
      },
      "XXX 187": {
        "x": 3223,
        "y": 655,
        "w": 84
      },
      "Erik Albers": {
        "x": 3125,
        "y": 676,
        "w": 114
      },
      "Cellini Bedi": {
        "x": 3085,
        "y": 622,
        "w": 90
      },
      "Boris Moiseev": {
        "x": 3185.5,
        "y": 380.5,
        "w": 105
      },
      "XXX 191": {
        "x": 3134,
        "y": 334,
        "w": 96
      },
      "XXX 192": {
        "x": 3130,
        "y": 233,
        "w": 84
      },
      "Daniel Molkentin": {
        "x": 3077.5,
        "y": 119.5,
        "w": 63
      },
      "XXX 194": {
        "x": 3006.5,
        "y": 97.5,
        "w": 69
      },
      "XXX 195": {
        "x": 2969.5,
        "y": 94.5,
        "w": 57
      },
      "XXX 196": {
        "x": 2956.5,
        "y": 145.5,
        "w": 75
      },
      "XXX 197": {
        "x": 3014.5,
        "y": 119.5,
        "w": 87
      },
      "XXX 198": {
        "x": 3077.5,
        "y": 306.5,
        "w": 81
      },
      "XXX 199": {
        "x": 3063,
        "y": 374,
        "w": 90
      },
      "XXX 200": {
        "x": 2933,
        "y": 99,
        "w": 60
      },
      "Sagar Chand Agarwal": {
        "x": 3004.5,
        "y": 311.5,
        "w": 93
      },
      "XXX 202": {
        "x": 2963,
        "y": 682,
        "w": 96
      },
      "XXX 203": {
        "x": 2938,
        "y": 361,
        "w": 108
      },
      "XXX 204": {
        "x": 2932.5,
        "y": 245.5,
        "w": 99
      },
      "XXX 205": {
        "x": 2905,
        "y": 293,
        "w": 78
      },
      "Jing Bai": {
        "x": 2856,
        "y": 338,
        "w": 90
      },
      "XXX 207": {
        "x": 2906,
        "y": 136,
        "w": 66
      },
      "XXX 208": {
        "x": 2827.5,
        "y": 89.5,
        "w": 99
      },
      "XXX 209": {
        "x": 2831.5,
        "y": 147.5,
        "w": 45
      },
      "XXX 210": {
        "x": 2783,
        "y": 145,
        "w": 84
      },
      "XXX 211": {
        "x": 2786,
        "y": 95,
        "w": 72
      },
      "XXX 212": {
        "x": 2759,
        "y": 150,
        "w": 48
      },
      "XXX 213": {
        "x": 2764.5,
        "y": 115.5,
        "w": 33
      },
      "XXX 214": {
        "x": 2698.5,
        "y": 90.5,
        "w": 45
      },
      "XXX 215": {
        "x": 2701,
        "y": 117,
        "w": 36
      },
      "XXX 216": {
        "x": 2716,
        "y": 131,
        "w": 60
      },
      "Claus Christensen": {
        "x": 2734,
        "y": 250,
        "w": 126
      },
      "Markus Goetz": {
        "x": 2744,
        "y": 343,
        "w": 84
      },
      "XXX 219": {
        "x": 2785.5,
        "y": 395.5,
        "w": 99
      },
      "XXX 220": {
        "x": 2795.5,
        "y": 625.5,
        "w": 129
      },
      "XXX 221": {
        "x": 2642,
        "y": 398,
        "w": 96
      },
      "XXX 222": {
        "x": 2667.5,
        "y": 140.5,
        "w": 81
      },
      "Jonathan Riddell (Riddell)": {
        "x": 2621,
        "y": 156,
        "w": 78
      },
      "XXX 224": {
        "x": 2633,
        "y": 83,
        "w": 72
      },
      "XXX 225": {
        "x": 2578,
        "y": 94,
        "w": 72
      },
      "XXX 226": {
        "x": 2598,
        "y": 140,
        "w": 54
      },
      "XXX 227": {
        "x": 2563,
        "y": 152,
        "w": 60
      },
      "XXX 228": {
        "x": 2609.5,
        "y": 302.5,
        "w": 75
      },
      "Jocelyn Turcotte": {
        "x": 2605.5,
        "y": 367.5,
        "w": 81
      },
      "XXX 230": {
        "x": 2646.5,
        "y": 632.5,
        "w": 99
      },
      "XXX 231": {
        "x": 2521,
        "y": 395,
        "w": 102
      },
      "Christoph Miebach (no_se)": {
        "x": 2523.5,
        "y": 255.5,
        "w": 99
      },
      "XXX 233": {
        "x": 2521.5,
        "y": 135.5,
        "w": 75
      },
      "Hugo Beauzée-Luyssen (chouquette)": {
        "x": 2446,
        "y": 256,
        "w": 108
      },
      "Olivier Goffart": {
        "x": 2457,
        "y": 342,
        "w": 102
      },
      "XXX 236": {
        "x": 2492.5,
        "y": 674.5,
        "w": 105
      },
      "XXX 237": {
        "x": 2421,
        "y": 439,
        "w": 78
      },
      "Jesús Fernández": {
        "x": 2374,
        "y": 659,
        "w": 96
      },
      "XXX 239": {
        "x": 2287.5,
        "y": 426.5,
        "w": 93
      },
      "Konstantin Pavlov (thresh)": {
        "x": 2348,
        "y": 328,
        "w": 78
      },
      "XXX 241": {
        "x": 2471.5,
        "y": 135.5,
        "w": 69
      },
      "XXX 242": {
        "x": 2449,
        "y": 137,
        "w": 42
      },
      "XXX 243": {
        "x": 2428.5,
        "y": 164.5,
        "w": 69
      },
      "XXX 244": {
        "x": 2405,
        "y": 118,
        "w": 60
      },
      "XXX 245": {
        "x": 2365,
        "y": 120,
        "w": 66
      },
      "Valorie Zimmerman (valorie)": {
        "x": 2356,
        "y": 166,
        "w": 90
      },
      "Jean-Paul Saman (jpsaman)": {
        "x": 2312.5,
        "y": 110.5,
        "w": 87
      },
      "XXX 248": {
        "x": 2270.5,
        "y": 130.5,
        "w": 69
      },
      "XXX 249": {
        "x": 2205.5,
        "y": 111.5,
        "w": 51
      },
      "XXX 250": {
        "x": 2191,
        "y": 155,
        "w": 60
      },
      "XXX 251": {
        "x": 2227,
        "y": 172,
        "w": 78
      },
      "XXX 252": {
        "x": 2205.5,
        "y": 359.5,
        "w": 87
      },
      "Luca Barabato (lu_zero)": {
        "x": 2240.5,
        "y": 684.5,
        "w": 93
      },
      "XXX 254": {
        "x": 2125,
        "y": 704,
        "w": 90
      },
      "Felix Paul Kühne (feepk)": {
        "x": 2153.5,
        "y": 387.5,
        "w": 99
      },
      "Kenny Coyle": {
        "x": 2132,
        "y": 122,
        "w": 96
      },
      "XXX 257": {
        "x": 2134.5,
        "y": 102.5,
        "w": 57
      },
      "Rémi Denis-Courmont (courmisch)": {
        "x": 2094.5,
        "y": 154.5,
        "w": 69
      },
      "XXX 259": {
        "x": 2064,
        "y": 140,
        "w": 48
      },
      "Carola Nitz": {
        "x": 2081,
        "y": 337,
        "w": 84
      },
      "XXX 261": {
        "x": 2028.5,
        "y": 168.5,
        "w": 75
      },
      "Filip Roséen": {
        "x": 2034.5,
        "y": 126.5,
        "w": 51
      },
      "Jean-Baptiste Kempf (j-b)": {
        "x": 2024,
        "y": 405,
        "w": 84
      },
      "XXX 264": {
        "x": 1968,
        "y": 134,
        "w": 78
      },
      "XXX 265": {
        "x": 1954.5,
        "y": 251.5,
        "w": 93
      },
      "XXX 266": {
        "x": 1932.5,
        "y": 157.5,
        "w": 57
      },
      "XXX 267": {
        "x": 2004.5,
        "y": 704.5,
        "w": 105
      },
      "Simon Wächter": {
        "x": 1944,
        "y": 634,
        "w": 108
      },
      "XXX 269": {
        "x": 1892.5,
        "y": 137.5,
        "w": 69
      },
      "XXX 270": {
        "x": 1902.5,
        "y": 187.5,
        "w": 69
      },
      "XXX 271": {
        "x": 1845.5,
        "y": 139.5,
        "w": 69
      },
      "XXX 272": {
        "x": 1792.5,
        "y": 156.5,
        "w": 45
      },
      "XXX 273": {
        "x": 1807,
        "y": 181,
        "w": 72
      },
      "Thorsten Behrens": {
        "x": 1885,
        "y": 346,
        "w": 84
      },
      "Lydia Pintscher": {
        "x": 1878,
        "y": 410,
        "w": 102
      },
      "XXX 276": {
        "x": 1828.5,
        "y": 723.5,
        "w": 75
      },
      "Kevin Ottens": {
        "x": 1794.5,
        "y": 597.5,
        "w": 105
      },
      "XXX 278": {
        "x": 1772,
        "y": 297,
        "w": 102
      },
      "XXX 279": {
        "x": 1748.5,
        "y": 156.5,
        "w": 69
      },
      "XXX 280": {
        "x": 1708.5,
        "y": 136.5,
        "w": 51
      },
      "XXX 281": {
        "x": 1709.5,
        "y": 184.5,
        "w": 75
      },
      "Àlex Fiestas (afiestas)": {
        "x": 1717,
        "y": 402,
        "w": 102
      },
      "Adriaan de Groot ([ade])": {
        "x": 1680,
        "y": 569,
        "w": 102
      },
      "Johan Thelin": {
        "x": 1656.5,
        "y": 675.5,
        "w": 87
      },
      "Ludovic Fauvet (etix)": {
        "x": 1626,
        "y": 343,
        "w": 102
      },
      "Thomas Guillem (tguillem)": {
        "x": 1635,
        "y": 141,
        "w": 78
      },
      "XXX 287": {
        "x": 1619,
        "y": 179,
        "w": 54
      },
      "XXX 288": {
        "x": 1590,
        "y": 109,
        "w": 66
      },
      "XXX 289": {
        "x": 1559.5,
        "y": 176.5,
        "w": 87
      },
      "XXX 290": {
        "x": 1544.5,
        "y": 126.5,
        "w": 57
      },
      "Vishesh Handa (vhanda)": {
        "x": 1566,
        "y": 406,
        "w": 90
      },
      "Audrey Prevost": {
        "x": 1515.5,
        "y": 362.5,
        "w": 81
      },
      "XXX 293": {
        "x": 1498.5,
        "y": 147.5,
        "w": 75
      },
      "XXX 294": {
        "x": 1491.5,
        "y": 187.5,
        "w": 39
      },
      "Gilles Fernandez": {
        "x": 1508.5,
        "y": 706.5,
        "w": 87
      },
      "Sean Harmer": {
        "x": 1501,
        "y": 602,
        "w": 102
      },
      "Rafael Carré (funman)": {
        "x": 1431,
        "y": 393,
        "w": 108
      },
      "XXX 298": {
        "x": 1445,
        "y": 149,
        "w": 72
      },
      "XXX 299": {
        "x": 1347.5,
        "y": 608.5,
        "w": 99
      },
      "XXX 300": {
        "x": 1342,
        "y": 408,
        "w": 102
      },
      "François Cartégnie (inthewings)": {
        "x": 1385.5,
        "y": 331.5,
        "w": 87
      },
      "XXX 302": {
        "x": 1400.5,
        "y": 160.5,
        "w": 87
      },
      "XXX 303": {
        "x": 1380,
        "y": 145,
        "w": 66
      },
      "XXX 304": {
        "x": 1390,
        "y": 124,
        "w": 42
      },
      "XXX 305": {
        "x": 1343.5,
        "y": 191.5,
        "w": 81
      },
      "XXX 306": {
        "x": 1273.5,
        "y": 688.5,
        "w": 105
      },
      "XXX 307": {
        "x": 1195.5,
        "y": 683.5,
        "w": 111
      },
      "Jose Millan Soto": {
        "x": 1240.5,
        "y": 577.5,
        "w": 117
      },
      "XXX 309": {
        "x": 1223.5,
        "y": 412.5,
        "w": 99
      },
      "XXX 310": {
        "x": 1264.5,
        "y": 320.5,
        "w": 117
      },
      "XXX 311": {
        "x": 1290.5,
        "y": 150.5,
        "w": 87
      },
      "XXX 312": {
        "x": 1261.5,
        "y": 122.5,
        "w": 63
      },
      "XXX 313": {
        "x": 1219,
        "y": 143,
        "w": 90
      },
      "XXX 314": {
        "x": 1157,
        "y": 144,
        "w": 90
      },
      "XXX 315": {
        "x": 1126,
        "y": 175,
        "w": 72
      },
      "XXX 316": {
        "x": 1102.5,
        "y": 136.5,
        "w": 63
      },
      "XXX 317": {
        "x": 1107.5,
        "y": 631.5,
        "w": 99
      },
      "XXX 318": {
        "x": 1032,
        "y": 684,
        "w": 108
      },
      "Tobias Hunger": {
        "x": 1028.5,
        "y": 370.5,
        "w": 87
      },
      "XXX 320": {
        "x": 1077,
        "y": 173,
        "w": 72
      },
      "XXX 321": {
        "x": 1007.5,
        "y": 91.5,
        "w": 117
      },
      "XXX 322": {
        "x": 978,
        "y": 127,
        "w": 54
      },
      "Albert Vaca Cintora": {
        "x": 946,
        "y": 156,
        "w": 90
      },
      "XXX 324": {
        "x": 900.5,
        "y": 129.5,
        "w": 99
      },
      "XXX 325": {
        "x": 890,
        "y": 155,
        "w": 36
      },
      "Manuel Nikschas (Sput)": {
        "x": 854.5,
        "y": 160.5,
        "w": 93
      },
      "XXX 327": {
        "x": 805,
        "y": 128,
        "w": 102
      },
      "Gabriel de Dietrich": {
        "x": 910,
        "y": 433,
        "w": 102
      },
      "XXX 329": {
        "x": 882.5,
        "y": 696.5,
        "w": 123
      },
      "XXX 330": {
        "x": 796,
        "y": 461,
        "w": 144
      },
      "Louai Al-Khanji": {
        "x": 733,
        "y": 644,
        "w": 126
      },
      "XXX 332": {
        "x": 645.5,
        "y": 717.5,
        "w": 123
      },
      "XXX 333": {
        "x": 621.5,
        "y": 512.5,
        "w": 111
      },
      "XXX 334": {
        "x": 729.5,
        "y": 404.5,
        "w": 123
      },
      "Andrew Knight": {
        "x": 745.5,
        "y": 149.5,
        "w": 99
      },
      "XXX 336": {
        "x": 704.5,
        "y": 105.5,
        "w": 99
      },
      "XXX 337": {
        "x": 669.5,
        "y": 144.5,
        "w": 99
      },
      "XXX 338": {
        "x": 664,
        "y": 112,
        "w": 72
      },
      "XXX 339": {
        "x": 625.5,
        "y": 132.5,
        "w": 69
      },
      "Jake Petroules": {
        "x": 594.5,
        "y": 144.5,
        "w": 93
      },
      "XXX 341": {
        "x": 532,
        "y": 649,
        "w": 114
      },
      "XXX 342": {
        "x": 504,
        "y": 406,
        "w": 108
      },
      "XXX 343": {
        "x": 538.5,
        "y": 160.5,
        "w": 105
      },
      "Johan Helsing": {
        "x": 472,
        "y": 90,
        "w": 114
      },
      "XXX 345": {
        "x": 379,
        "y": 89,
        "w": 120
      },
      "XXX 346": {
        "x": 425,
        "y": 178,
        "w": 114
      },
      "XXX 347": {
        "x": 411.5,
        "y": 409.5,
        "w": 111
      },
      "XXX 348": {
        "x": 253.5,
        "y": 421.5,
        "w": 111
      },
      "Paul Brown": {
        "x": 48.5,
        "y": 423.5,
        "w": 129
      },
      "Sebastian Kügler": {
        "x": 186.5,
        "y": 330.5,
        "w": 111
      },
      "XXX 351": {
        "x": 288,
        "y": 178,
        "w": 120
      },
      "XXX 352": {
        "x": 261,
        "y": 133,
        "w": 108
      },
      "Clemens Toennies": {
        "x": 77,
        "y": 89,
        "w": 150
      }
    },
    "photo": "2016/groupphoto/qtcon.jpg"
  },
  "2017": {
    "people": {
      "Emma Gospodinova (xstyle)": {
        "x": 3911.5,
        "y": 1424.5,
        "w": 75
      },
      "Dora Olah": {
        "x": 3864,
        "y": 1384,
        "w": 66
      },
      "Eliakin Costa": {
        "x": 3782,
        "y": 1454,
        "w": 66
      },
      "John Samuel": {
        "x": 3794.5,
        "y": 1534.5,
        "w": 87
      },
      "Camilo Higuita": {
        "x": 3693,
        "y": 1562,
        "w": 78
      },
      "Vishesh Handa (vHanda)": {
        "x": 3596.5,
        "y": 1511.5,
        "w": 81
      },
      "Jean-Baptiste Mardelle": {
        "x": 3664.5,
        "y": 1484.5,
        "w": 75
      },
      "Douglas DeMaio": {
        "x": 3695,
        "y": 1369,
        "w": 84
      },
      "Albert Vaca Cintora": {
        "x": 3621,
        "y": 1295,
        "w": 78
      },
      "Luigi Toscano (tosky)": {
        "x": 3609,
        "y": 1418,
        "w": 78
      },
      "Eva Romero": {
        "x": 3559.5,
        "y": 1384.5,
        "w": 69
      },
      "Antonio Larrosa Jimenez (antlarr)": {
        "x": 3480,
        "y": 1345,
        "w": 72
      },
      "Àlex Fiestas": {
        "x": 3482.5,
        "y": 1282.5,
        "w": 75
      },
      "Frederik Gladhorn": {
        "x": 3395.5,
        "y": 1311.5,
        "w": 69
      },
      "Cristóbal Saraiba Torres": {
        "x": 3596.5,
        "y": 1589.5,
        "w": 69
      },
      "Shinjo Park (pereman)": {
        "x": 3520.5,
        "y": 1542.5,
        "w": 75
      },
      "Tobias Fischbach": {
        "x": 3414.5,
        "y": 1381.5,
        "w": 69
      },
      "Richard Dale (rdale)": {
        "x": 3475.5,
        "y": 1462.5,
        "w": 69
      },
      "Allan Sandfeld Jensen (careworld)": {
        "x": 3411.5,
        "y": 1538.5,
        "w": 81
      },
      "Mirko Böhm (miroslav)": {
        "x": 3538.5,
        "y": 1660.5,
        "w": 75
      },
      "Jure Repinc": {
        "x": 3387.5,
        "y": 1619.5,
        "w": 63
      },
      "Gunnar Schmidt": {
        "x": 3303,
        "y": 1586,
        "w": 78
      },
      "Gerry Boland": {
        "x": 3382.5,
        "y": 1685.5,
        "w": 81
      },
      "Lukas Hetzenecker": {
        "x": 3418,
        "y": 1782,
        "w": 84
      },
      "David Edmundson (d_ed)": {
        "x": 3266.5,
        "y": 1751.5,
        "w": 69
      },
      "Cesar Expósito": {
        "x": 3234,
        "y": 1681,
        "w": 60
      },
      "Christoph Cullmann": {
        "x": 3217,
        "y": 1610,
        "w": 60
      },
      "David Faure (dfaure)": {
        "x": 3229,
        "y": 1486,
        "w": 78
      },
      "Jos van den Oever (vandenoever)": {
        "x": 3353.5,
        "y": 1440.5,
        "w": 63
      },
      "Dennis Knorr": {
        "x": 3371,
        "y": 1404,
        "w": 30
      },
      "Agustín Benito Bethencourt (toscalix)": {
        "x": 3292,
        "y": 1333,
        "w": 60
      },
      "Andres Betts (Andy)": {
        "x": 3196.5,
        "y": 1400.5,
        "w": 51
      },
      "José María Martínez (Itúbal)": {
        "x": 3132,
        "y": 1302,
        "w": 42
      },
      "Adrian Chaves Fernandez (Gallaecio)": {
        "x": 3130.5,
        "y": 1352.5,
        "w": 45
      },
      "Thomas Pfeiffer": {
        "x": 3114.5,
        "y": 1431.5,
        "w": 57
      },
      "Marco Martin": {
        "x": 3133.5,
        "y": 1718.5,
        "w": 63
      },
      "Alejandro López": {
        "x": 3109,
        "y": 1660,
        "w": 60
      },
      "Ignacio Serantes": {
        "x": 2990,
        "y": 1761,
        "w": 102
      },
      "Franklin Weng": {
        "x": 2753,
        "y": 1768,
        "w": 96
      },
      "Paul Brown MKII": {
        "x": 2861,
        "y": 1720,
        "w": 90
      },
      "Valorie Zimmerman (valorie)": {
        "x": 2963,
        "y": 1894,
        "w": 84
      },
      "Adriaan de Groot ([ade])": {
        "x": 2975,
        "y": 1289,
        "w": 54
      },
      "Jens Reuterberg": {
        "x": 3002,
        "y": 1355,
        "w": 90
      },
      "Francis Herne (FLHerne)": {
        "x": 3072.5,
        "y": 1503.5,
        "w": 69
      },
      "Ivan Čukić (ivan)": {
        "x": 3054,
        "y": 1560,
        "w": 78
      },
      "Paul Brown": {
        "x": 2983.5,
        "y": 1681.5,
        "w": 75
      },
      "Aleix Pol Gonzalez (apol)": {
        "x": 2949.5,
        "y": 1624.5,
        "w": 63
      },
      "Dominik Haumann": {
        "x": 2932,
        "y": 1557,
        "w": 54
      },
      "Andreas Cord-Landwehr": {
        "x": 2819.5,
        "y": 1563.5,
        "w": 63
      },
      "José Millán": {
        "x": 3116,
        "y": 1838,
        "w": 102
      },
      "Dmitri Popov": {
        "x": 3210.5,
        "y": 2029.5,
        "w": 111
      },
      "Anu Mittal": {
        "x": 3082,
        "y": 1986,
        "w": 84
      },
      "Jeff Huang": {
        "x": 2761,
        "y": 1887,
        "w": 108
      },
      "Vasudha Mathur": {
        "x": 2839,
        "y": 2004,
        "w": 90
      },
      "Kenny Duffus (seaLne)": {
        "x": 2971.5,
        "y": 2065.5,
        "w": 93
      },
      "Sven Brauch (scummos)": {
        "x": 2760.5,
        "y": 1507.5,
        "w": 75
      },
      "Sandro Andrade": {
        "x": 2790.5,
        "y": 1375.5,
        "w": 75
      },
      "XXX 58": {
        "x": 2683.5,
        "y": 1338.5,
        "w": 87
      },
      "Gabriel Ochoa": {
        "x": 2699.5,
        "y": 1454.5,
        "w": 75
      },
      "Volker Krause (vkrause)": {
        "x": 2600,
        "y": 1467,
        "w": 78
      },
      "Manoli Herrero": {
        "x": 2536,
        "y": 1419,
        "w": 72
      },
      "Paco Estrada": {
        "x": 2449.5,
        "y": 1396.5,
        "w": 75
      },
      "Kevin Ottens (ervin)": {
        "x": 2560.5,
        "y": 1615.5,
        "w": 93
      },
      "Jesús Fernández": {
        "x": 2398,
        "y": 1533,
        "w": 78
      },
      "Gabrielle Ponzo": {
        "x": 2529.5,
        "y": 1781.5,
        "w": 81
      },
      "Arnav Dhamija (shortstheory)": {
        "x": 2659,
        "y": 1962,
        "w": 90
      },
      "Bhushan Shah (bshah)": {
        "x": 2675,
        "y": 2060,
        "w": 108
      },
      "Boudhayan Gupta (BaloneyGeek)": {
        "x": 2481,
        "y": 1971,
        "w": 120
      },
      "Xisco Fauli (x1sc0)": {
        "x": 2407,
        "y": 1656,
        "w": 90
      },
      "Teodor Mircea Ionita (shinnok)": {
        "x": 2393.5,
        "y": 1803.5,
        "w": 99
      },
      "Jorge Maldonado Ventura": {
        "x": 2256.5,
        "y": 1732.5,
        "w": 93
      },
      "Sebastian Kügler (sebas)": {
        "x": 2365.5,
        "y": 1911.5,
        "w": 99
      },
      "Eike Hein (Sho_)": {
        "x": 2353.5,
        "y": 2055.5,
        "w": 105
      },
      "Kenny Coyle": {
        "x": 2155.5,
        "y": 1809.5,
        "w": 75
      },
      "Davide Bettio": {
        "x": 2138.5,
        "y": 1917.5,
        "w": 99
      },
      "Lydia Pintscher (nightrose)": {
        "x": 2151.5,
        "y": 2113.5,
        "w": 99
      },
      "Harald Sitter": {
        "x": 1977,
        "y": 2009,
        "w": 102
      },
      "Albert Astals Cid (tsdgeos)": {
        "x": 1860.5,
        "y": 1919.5,
        "w": 105
      },
      "Scarlett Clark (sgclark)": {
        "x": 1746.5,
        "y": 2095.5,
        "w": 99
      },
      "Aditya Mehra": {
        "x": 1642.5,
        "y": 1906.5,
        "w": 87
      },
      "Timothée Giet": {
        "x": 1470,
        "y": 1842,
        "w": 90
      },
      "Tomaz Canabrava": {
        "x": 1359.5,
        "y": 1991.5,
        "w": 93
      },
      "Ingo Klöcker (mahoutsukai)": {
        "x": 1260,
        "y": 1807,
        "w": 108
      },
      "Luciano Montanaro (mikelima)": {
        "x": 1080.5,
        "y": 1974.5,
        "w": 129
      },
      "Dan Leinir Turthra Jensen": {
        "x": 1117.5,
        "y": 1717.5,
        "w": 75
      },
      "Lia Silva": {
        "x": 995,
        "y": 1986,
        "w": 72
      },
      "Rahul Yadav": {
        "x": 962,
        "y": 1718,
        "w": 84
      },
      "Jonathan Riddell": {
        "x": 781.5,
        "y": 1672.5,
        "w": 87
      },
      "Petra Gillert (pgillert)": {
        "x": 672,
        "y": 1651,
        "w": 90
      },
      "Claus Christensen": {
        "x": 537.5,
        "y": 1587.5,
        "w": 99
      },
      "Ismael Olea  (@olea)": {
        "x": 383,
        "y": 1644,
        "w": 96
      },
      "Helio Castro": {
        "x": 925,
        "y": 1896,
        "w": 90
      },
      "Remedios Fernandez": {
        "x": 783,
        "y": 1895,
        "w": 84
      },
      "Baltasar Ortega": {
        "x": 611.5,
        "y": 1866.5,
        "w": 99
      },
      "Rubén Gómez Antolí (rgomezantoli)": {
        "x": 462,
        "y": 1918,
        "w": 114
      },
      "Juanjo Salvador (jsalvador)": {
        "x": 319.5,
        "y": 1871.5,
        "w": 105
      },
      "Rafa Aybar": {
        "x": 196,
        "y": 1787,
        "w": 120
      }
    },
    "photo": "2017/groupphoto/akademy2017-groupphoto.jpg"
  }
}