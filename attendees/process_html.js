const fs = require('fs')
const path = require('path')

const years = {}

fs.readdirSync('..').map((dir) => {
    if (!dir.startsWith('20')) {
        return
    }
    const html = fs.readFileSync(path.join('..', dir, 'groupphoto', 'index.html'), { encoding: 'utf8' })
    let photo = null
    const people = {}
    html.split('\n').map((line) => {
        const m1 = /coords="([^"]*)"/.exec(line)
        const m2 = /title="([^"]*)"/.exec(line)
        const m3 = /<img src="([^"]*)"/.exec(line)
        if (m1 && m2) {
            const c = m1[1].split(',')
            const w = 3 * c[2]
            const x = c[0] - 1.5 * c[2]
            const y = c[1] - 1.5 * c[2]
            people[m2[1]] = { x, y, w }
        }
        if (m3) {
            photo = path.join(dir, 'groupphoto', m3[1])
        }
    })
    years[dir] = { people, photo }
})

fs.writeFileSync('people.js', 'const years = ' + JSON.stringify(years, null, '  ').normalize(), 'utf8')
