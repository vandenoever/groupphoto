"use strict"
Element.prototype.documentOffsetTop = function () {
    return this.offsetTop + ( this.offsetParent ? this.offsetParent.documentOffsetTop() : 0 )
}
Element.prototype.documentOffsetLeft = function () {
    return this.offsetLeft + ( this.offsetParent ? this.offsetParent.documentOffsetLeft() : 0 )
}
let currentArea
const ensureVisible = () => {
    const top = currentArea.documentOffsetTop() - ( window.innerHeight / 2 )
    const left = currentArea.documentOffsetLeft() - ( window.innerWidth / 2 )
    window.scrollTo(left, top)
}
const toNext = () => {
    if (!currentArea) {
        return
    }
    const parent = currentArea.parentNode
    let next = currentArea.nextSibling
    while (next !== currentArea) {
        if (next && next.nodeType === 1 && next.getAttribute('class') === 'border') {
            currentArea = next
            ensureVisible()
            return
        }
        if (!next) {
            next = parent.firstChild
        } else {
            next = next.nextSibling
        }
    }
}
const search = (i) => {
    const query = i.value
    const overlay = document.getElementById('overlay')
    const q = new RegExp(query, "i")
    let area = overlay.firstChild
    let hits = 0
    let title
    const first = true
    currentArea = null
    while (area) {
        if (area.setAttribute) {
            if (query.length && area.textContent.match(q)) {
                title = area.textContent
                area.setAttribute('class', 'border')
                hits += 1
                if (!currentArea) {
                    currentArea = area
                    ensureVisible()
                }
            } else {
                area.setAttribute('class', '')
            }
        }
        area = area.nextSibling
    }
    const count = document.getElementById('hitcount')
    if (hits == 1) {
        count.innerHTML = 'Found ' + title + '!'
    } else if (hits == 0 && query.length) {
        count.innerHTML = 'No hits found.'
    } else {
        count.innerHTML = hits + " people"
    }
}
const searchAnchor = (input) => {
    const i = document.location.href.indexOf('#')
    if (i === -1) {
        return
    }
    const anchor = decodeURI(document.location.href.substr(i + 1))
    input.value = anchor
    search(input)
}
const addArea = (area) => {
    const c = area.coords.split(',')
    const w = 3 * c[2]
    const x = c[0] - 1.5 * c[2]
    const y = c[1] - 1.5 * c[2]
    addBorder(x, y, w, w, area.title)
}
const addBorder = (x, y, w, h, text) => {
    const overlay = document.getElementById('overlay')
    const div = document.createElement('div')
    overlay.appendChild(div)
    div.style.left = x + 'px'
    div.style.top = y + 'px'
    div.style.height = h + 'px'
    div.style.width = w + 'px'
    const t = document.createElement('div')
    div.appendChild(t)
    t.style.top = h + 'px'
    const s = document.createElement('span')
    s.innerHTML = text
    t.appendChild(s)
}
const setStatus = (text) => {
    window.status = text
}
const init = () => {
    const input = document.getElementById('input')
    input.addEventListener('keydown', (evt) => {
        if (evt.keyCode === 9) {
            toNext()
            if (evt.preventDefault) {
                evt.preventDefault()
            }
            return false
        }
    })
    input.addEventListener('keyup', (evt) => {
        if (evt.keyCode === 9) {
            if (evt.preventDefault) {
                evt.preventDefault()
            }
            return
        }
        search(evt.target)
    })
    input.focus()
    const map = document.getElementById('unnamed')
    let area = map.firstChild
    let title
    while (area) {
        if (area.tagName == 'AREA' || area.tagName == 'area') {
            title = area.title
            addArea(area)
        }
        area = area.nextSibling
    }
    window.scrollTo(document.body.offsetWidth / 2, document.body.offsetHeight / 2)
    searchAnchor(input)
}
window.addEventListener('load', init)
